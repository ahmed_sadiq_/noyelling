package au.com.saybravo.noyelling.lessons.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StudentPostClaimsBody
{
    @SerializedName("claims")
    @Expose
    public Claim[] claims;

    public StudentPostClaimsBody(Claim[] claims)
    {
        this.claims = claims;
    }
}


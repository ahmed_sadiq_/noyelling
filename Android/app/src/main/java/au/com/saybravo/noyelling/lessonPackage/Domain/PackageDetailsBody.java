package au.com.saybravo.noyelling.lessonPackage.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ahmed.sadiq on 31/05/16.
 */
public class PackageDetailsBody {

    @SerializedName("package_details")
    @Expose
    private List<ProductItem> packageDetails;


    public void setPackageDetails(List<ProductItem> details){
        packageDetails = details;
    }

}

package au.com.saybravo.noyelling.student.Recycler;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.student.Domain.Student;

/**
 * Created by ahmed.sadiq on 15/04/16.
 */
public abstract class BaseStudentRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected  List<Student> students;
    protected  final int VIEW_STUDENT = 1;
    protected  final int VIEW_PROG = 0;

    OnItemClickListener onItemClickListener;


    public interface OnItemClickListener{

        public void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener){

        this.onItemClickListener = mItemClickListener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if(viewType == VIEW_STUDENT) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_students_recyclerview_item, parent, false);
            StudentViewHolder vh = new StudentViewHolder(v, onItemClickListener);
            return vh;
        }else if(viewType == VIEW_PROG){

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_students_recyclerview_item_loading, parent, false);
            ProgressViewHolder vh = new ProgressViewHolder(v);
            return vh;
        }
        return null;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if(holder instanceof StudentViewHolder) {

            StudentViewHolder studentHolder = (StudentViewHolder)holder;
            studentHolder.student = students.get(position);

            StringBuilder sb = new StringBuilder(studentHolder.student.GetEmail());
            if(studentHolder.student.Address.Suburb != null){

                sb.append(", "+studentHolder.student.Address.Suburb);
            }

            studentHolder.emailSuburb.setText(sb.toString());
            studentHolder.name.setText(studentHolder.student.FirstName + " " + studentHolder.student.LastName);

        }else if(holder instanceof ProgressViewHolder){

            ProgressViewHolder progressHolder = (ProgressViewHolder)holder;
            progressHolder.progressBar.setIndeterminate(true);

        }
    }

    @Override
    public int getItemCount() {
        if(students == null){
            return 0;
        }
        return students.size();
    }

    @Override
    public int getItemViewType(int position){
        return students.get(position) != null ? VIEW_STUDENT : VIEW_PROG;
    }

}

package au.com.saybravo.noyelling.student;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.app.BaseActivity;
import au.com.saybravo.noyelling.app.DelayAutoCompleteTextView;
import au.com.saybravo.noyelling.app.Helpers.ValidationHelper;
import au.com.saybravo.noyelling.app.ProgressButton.ProgressButton;
import au.com.saybravo.noyelling.app.ProgressButton.ProgressButtonCallback;
import au.com.saybravo.noyelling.app.Services.JWTRefreshService;
import au.com.saybravo.noyelling.app.Services.ServiceGenerator;
import au.com.saybravo.noyelling.student.Domain.AddressAttributes;
import au.com.saybravo.noyelling.student.Domain.AddressAutocompleteItem;
import au.com.saybravo.noyelling.student.Domain.CreateStudent.CustomerAttributes;
import au.com.saybravo.noyelling.student.Domain.CreateStudent.Student;
import au.com.saybravo.noyelling.student.Domain.CreateStudent.StudentCreateBody;
import au.com.saybravo.noyelling.student.Domain.StudentBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ahmed.sadiq on 27/05/16.
 */
public class AddStudentActivity extends BaseActivity {

    DelayAutoCompleteTextView addressDelayAutoComplete;
    private String newAddressPlaceId = null;
    private ProgressButton saveButton;

    private
    Activity activity;

    public AddStudentActivity() {
        super(R.layout.activity_student_add,  R.string.StudentDetails_HeaderText);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = this;
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        saveButton = new ProgressButton(this, R.id.SaveButton, R.string.StudentAdd_SaveButton_Text, R.string.StudentDetails_SaveButton_LoadingText, new ProgressButtonCallback() {
            @Override
            public void callback() {
                save();
            }
        });


        setupGooglePlaces();
    }

    private void setupGooglePlaces() {

        String apiKey = "";

        try {
            ApplicationInfo ai = getPackageManager().getApplicationInfo(activity.getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = ai.metaData;
            apiKey = bundle.getString("GooglePlacesApiKey");
        } catch (PackageManager.NameNotFoundException e) {
            logError("Failed to load meta-data, NameNotFound: " + e.getMessage());
        } catch (NullPointerException e) {
            logError("Failed to load meta-data, NullPointer: " + e.getMessage());
        }

        addressDelayAutoComplete = (DelayAutoCompleteTextView) findViewById(R.id.AddressDelayAutoComplete);
        addressDelayAutoComplete.setText("");
        addressDelayAutoComplete.setThreshold(3);
        addressDelayAutoComplete.setAdapter(new AddressAutocompleteAdapter(this, apiKey)); // 'this' is Activity instance
        addressDelayAutoComplete.setLoadingIndicator((android.widget.ProgressBar) findViewById(R.id.AddressDelayAutoCompleteProgressBar));
        addressDelayAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                AddressAutocompleteItem item = (AddressAutocompleteItem) adapterView.getItemAtPosition(position);

                addressDelayAutoComplete.setText(item.Title);
                newAddressPlaceId = item.PlaceId;
            }
        });
    }

    private void save() {

        EditText mobileNumber = (EditText) activity.findViewById(R.id.MobileNumber);

        boolean hasErrors = false;

        if (TextUtils.isEmpty(mobileNumber.getText())) {

            mobileNumber.setError(getString(R.string.Validation_MobileNumberIsRequired));
            hasErrors = true;
        } else if (!ValidationHelper.IsValidMobileNumber(mobileNumber.getText().toString())) {
            mobileNumber.setError(getString(R.string.Validation_MobileNumberIsNotValid));
            hasErrors = true;
        }

        EditText firstName = (EditText) activity.findViewById(R.id.FirstNameEdit);
        if (TextUtils.isEmpty(firstName.getText())) {

            firstName.setError(getString(R.string.Validation_FirstNameIsRequired));
            hasErrors = true;
        }

        EditText lastName = (EditText) activity.findViewById(R.id.LastNameEdit);
        if (TextUtils.isEmpty(lastName.getText())) {

            lastName.setError(getString(R.string.Validation_LastNameIsRequired));
            hasErrors = true;
        }

        EditText email = (EditText) activity.findViewById(R.id.EmailAddressEdit);
        if (TextUtils.isEmpty(email.getText())) {

            email.setError(getString(R.string.Validation_EmailIsRequired));
            hasErrors = true;

        }else if(!ValidationHelper.IsValidEmail(email.getText().toString())){

            email.setError(getString(R.string.Validation_EmailIsNotValid));
            hasErrors = true;
        }

        if(TextUtils.isEmpty(addressDelayAutoComplete.getText())){
            addressDelayAutoComplete.setError(getString(R.string.Validation_AddressIsRequired));
            hasErrors = true;
        }


        boolean manual = ((RadioButton)(activity.findViewById(R.id.TransmissionManual))).isChecked();
        boolean auto = ((RadioButton)(activity.findViewById(R.id.TransmissionAutomatic))).isChecked();

        TextView transmissionLabel = (TextView)activity.findViewById(R.id.TransmissionTitle);

        if(!manual && !auto){
            hasErrors = true;
            transmissionLabel.setError(getString(R.string.Validation_TransmissionTypeIsRequired));
        }else{
            transmissionLabel.setError(null);
        }

        if (hasErrors) {

            resetSaveButtonText(false);

            Toast.makeText(getApplicationContext(), R.string.StudentAdd_ErrorToast, Toast.LENGTH_LONG).show();
            return;
        }

        Student createStudent = new Student();

        createStudent.setFname(firstName.getText().toString());
        createStudent.setLname(lastName.getText().toString());
        createStudent.setMobileNumber(mobileNumber.getText().toString());

        CustomerAttributes customerAttributes = new CustomerAttributes();
        customerAttributes.setEmail(email.getText().toString());
        createStudent.setCustomerAttributes(customerAttributes);
        createStudent.setTransmission(manual == true? "manual" : "automatic");

        AddressAttributes addressAttributes = new AddressAttributes(newAddressPlaceId);
        createStudent.setAddressAttributes(addressAttributes);

        uploadData(createStudent);

    }


    private void uploadData(Student newStudent){


        StudentCreateBody studentCreateBody = new StudentCreateBody();
        studentCreateBody.setStudent(newStudent);

        ServiceGenerator.StudentService studentService = ServiceGenerator.createService(ServiceGenerator.StudentService.class, this);

        Call<au.com.saybravo.noyelling.student.Domain.StudentBody> call = studentService.CreateStudent(GetCurrentInstructorId(), studentCreateBody);

        JWTRefreshService.enqueueSafe(call, this, new Callback<StudentBody>() {
            @Override
            public void onResponse(Call<StudentBody> call, Response<StudentBody> response) {


                if(response.isSuccessful()) {
                    resetSaveButtonText(true);

                    StudentBody studentBody = response.body();
                    if(studentBody != null) {

                        showNextStepDialog(studentBody.student);

                    }
                }else{
                    resetSaveButtonText(false);
                    Toast.makeText(getApplicationContext(), R.string.StudentAddDuplicate_ErrorToast, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<StudentBody> call, Throwable t) {

                resetSaveButtonText(false);
            }
        });

    }


    private void showNextStepDialog(final au.com.saybravo.noyelling.student.Domain.Student student){

        Intent intent = new Intent(getApplicationContext(), StudentActivity.class);
        intent.putExtra(BaseActivity.IntentPayloadIdentifier, student.Id);
        intent.putExtra("student", student);
        intent.putExtra("loadaddpackage", true);

        startActivity(intent);
        finish();
    }

    private void resetSaveButtonText(boolean saved)
    {
        saveButton.hideLoading();
        if(saved) {
            saveButton.setText(getString(R.string.StudentAdd_SaveButton_DoneText));
        }
    }

}

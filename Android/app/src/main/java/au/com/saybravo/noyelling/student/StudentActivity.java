package au.com.saybravo.noyelling.student;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.app.BaseActivity;
import au.com.saybravo.noyelling.app.Services.JWTRefreshService;
import au.com.saybravo.noyelling.app.Services.ServiceGenerator;
import au.com.saybravo.noyelling.app.util.AppConstants;
import au.com.saybravo.noyelling.lessonPackage.LessonCashPackageActivity;
import au.com.saybravo.noyelling.lessonPackage.LessonPackageActivity;
import au.com.saybravo.noyelling.lessons.LessonsActivity;
import au.com.saybravo.noyelling.student.Domain.Student;
import au.com.saybravo.noyelling.student.Domain.StudentBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StudentActivity extends BaseActivity {

    private int studentId;
    Student student;
    private ProgressBar loadingProgressBar;
    private Button viewLessonsButton;
    private Button addPackageButton;
    private boolean isLoading;
    private AlertDialog paymentMethodSelectionDialog;

    public StudentActivity() {
        super(R.layout.activity_student);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        studentId = (int) getIntent().getSerializableExtra(IntentPayloadIdentifier);
        loadingProgressBar = (ProgressBar) findViewById(R.id.progressBarStudent);
        viewLessonsButton  = (Button) findViewById(R.id.ViewLessonsButton);
        addPackageButton = (Button) findViewById(R.id.AddPackage);

        student = (Student)getIntent().getSerializableExtra("student");
        setText(R.id.LessonsRemaining, "");
        setText(R.id.Transmission, "");

        TextView firstName = setText(R.id.StudentFirstName, student.FirstName);
        TextView lastName = setText(R.id.StudentLastName, student.LastName);

        if (student.FirstName.length() > 15 || student.LastName.length() > 15)
        {
            firstName.setTextSize(30);
            lastName.setTextSize(30);
        }

        setText(R.id.CallStudentTextView, "Call " + student.FirstName);
        setText(R.id.TextStudentTextView, "Text " + student.FirstName);
        setText(R.id.EmailStudentTextView, "Email " + student.FirstName);
        if(getIntent().hasExtra("loadaddpackage")){
            Toast.makeText(getApplicationContext(), "Student created", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        startLoading();
        loadStudent(studentId, this);
    }

    @Override
    protected void onUserSettingsClicked() {

        if (isLoading)
        {
            return;
        }

        startActivity(StudentDetailsActivity.class, student);
    }

    private void startLoading()
    {
        isLoading = true;

        viewLessonsButton.setVisibility(View.INVISIBLE);
        addPackageButton.setVisibility(View.INVISIBLE);
        loadingProgressBar.setVisibility(View.VISIBLE);
    }

    private void stopLoading()
    {
        isLoading = false;

        viewLessonsButton.setVisibility(View.VISIBLE);
        addPackageButton.setVisibility(View.VISIBLE);
        loadingProgressBar.setVisibility(View.GONE);
    }

    private void loadStudent(int studentId, Context context) {

        ServiceGenerator.StudentService studentService = ServiceGenerator.createService(ServiceGenerator.StudentService.class, context);

        Call<StudentBody> call = studentService.GetStudent(GetCurrentInstructorId(), studentId);
        JWTRefreshService.enqueueSafe(call, this, new Callback<StudentBody>() {
            @Override
            public void onResponse(Call<StudentBody> call, Response<StudentBody> response) {

                if (response.isSuccessful()) {

                    StudentBody result = response.body();
                    student = result.student;

                    bindStudentToView();
                } else {
                    ShowGenericError();
                }
            }

            @Override
            public void onFailure(Call<StudentBody> call, Throwable t) {
                ShowGenericError();
            }
        });
    }

    private void bindStudentToView() {

        setText(R.id.StudentFirstName, student.FirstName);
        setText(R.id.StudentLastName, student.LastName);

        String lessonsRemaining = String.format("%s lesson%s remaining", student.LessonsRemaining, student.LessonsRemaining != 1 ? "s" : "");
        setText(R.id.LessonsRemaining, lessonsRemaining);

        setText(R.id.Transmission, student.Transmission.equalsIgnoreCase("Automatic") ? "Automatic" : "Manual");

        Button viewLessonsButton = (Button) findViewById(R.id.ViewLessonsButton);
        viewLessonsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(LessonsActivity.class, student);
            }
        });

        addPackageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPaymentMethodSelectionDialog();
            }
        });

        TextView callTextView = setText(R.id.CallStudentTextView, "Call " + student.FirstName);
        callTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callStudent();
            }
        });

        TextView textStudentTextView = setText(R.id.TextStudentTextView, "Text " + student.FirstName);
        textStudentTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textStudent();
            }
        });

        TextView emailButton = setText(R.id.EmailStudentTextView, "Email " + student.FirstName);
        emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailStudent();
            }
        });

        TextView directionsButton = (TextView) findViewById(R.id.DirectionsTextView);
        directionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openMaps();
            }
        });

        stopLoading();
    }

    private void callStudent() {

        if (isLoading) {
            return;
        }

        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + student.MobileNumber));
        startActivity(callIntent);
    }

    private void textStudent() {

        if (isLoading) {
            return;
        }

        Intent textIntent = new Intent(Intent.ACTION_SENDTO);
        textIntent.setData(Uri.parse("sms:" + student.MobileNumber));
        startActivity(textIntent);
    }

    private void emailStudent() {

        if (isLoading) {
            return;
        }

        StartEmailIntent(R.string.Student_EmailStudent_Subject, R.string.Student_EmailStudent_Text, student.GetEmail());
    }

    //region Maps

    private void openMaps() {

        if (isLoading) {
            return;
        }

        Intent mapsIntent = GetMapsIntent();
        mapsIntent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        try {
            startActivity(mapsIntent);
        } catch (ActivityNotFoundException ex) {
            try {
                mapsIntent = GetMapsIntent();

                startActivity(mapsIntent);
            } catch (ActivityNotFoundException innerEx) {
                Toast.makeText(this, "Please install a maps application", Toast.LENGTH_LONG).show();
            }
        }
    }

    private Intent GetMapsIntent() {
        Intent mapsIntent = new Intent(Intent.ACTION_VIEW);

        String url = "https://www.google.com.au/maps/dir/%s/%s"; // /From/To
        mapsIntent.setData(Uri.parse(String.format(url, "", student.GetAddress())));

        return mapsIntent;
    }

/*  TODO SD - After a discussion with Pat, working out the current address is a nice to have feature, for MVP we will just plot the position on the map
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private String GetCurrentAddress() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.ACCESS_FINE_LOCATION }, 1);

            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null;
        }

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        double longitude = location.getLongitude();
        double latitude = location.getLatitude();


        Geocoder geocoder = new Geocoder(getBaseContext());
        List<android.location.Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses != null && addresses.size() > 0) {

            String x = "";
        }

        return null;
    }*/

    //endregion

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    public void showPaymentMethodSelectionDialog() {
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.layout_dialog_payment_method, null);

        paymentMethodSelectionDialog = new AlertDialog.Builder(this)
                .setView(dialogView)
                .create();

        View tapAndGoLayout = dialogView.findViewById(R.id.radioTapAndGo);
        View enterCardNumber = dialogView.findViewById(R.id.radioEnterCardNumber);
        View sendPaymentLink = dialogView.findViewById(R.id.radioSendPaymentLink);
        View cash = dialogView.findViewById(R.id.radioCash);

        tapAndGoLayout.setOnClickListener(paymentMethodClickListener);
        enterCardNumber.setOnClickListener(paymentMethodClickListener);
        sendPaymentLink.setOnClickListener(paymentMethodClickListener);
        cash.setOnClickListener(paymentMethodClickListener);

        paymentMethodSelectionDialog.show();

    }

    View.OnClickListener paymentMethodClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AppConstants.PAYMENT_METHOD paymentMethod = null;
            switch (v.getId()) {
                case R.id.radioTapAndGo:
                    paymentMethod = AppConstants.PAYMENT_METHOD.TAP_AND_GO;
                    break;
                case R.id.radioEnterCardNumber:
                    paymentMethod = AppConstants.PAYMENT_METHOD.CREDIT_CARD;
                    break;
                case R.id.radioSendPaymentLink:
                    paymentMethod = AppConstants.PAYMENT_METHOD.SEND_LINK;
                    break;
                case R.id.radioCash:
                    Intent intent = new Intent(StudentActivity.this, LessonCashPackageActivity.class);
                    intent.putExtra(AppConstants.INTENT_KEY_STUDENT, student);
                    startActivity(intent);
                    if(paymentMethodSelectionDialog != null && paymentMethodSelectionDialog.isShowing())
                        paymentMethodSelectionDialog.dismiss();
                    break;
            }

            if(paymentMethod != null) {
                if(paymentMethodSelectionDialog != null && paymentMethodSelectionDialog.isShowing())
                    paymentMethodSelectionDialog.dismiss();
                Intent intent = new Intent(StudentActivity.this, LessonPackageActivity.class);
                intent.putExtra(AppConstants.INTENT_KEY_PAYMENT_METHOD, paymentMethod.getValue());
                intent.putExtra(AppConstants.INTENT_KEY_STUDENT, student);
                startActivity(intent);
            }
        }
    };
}

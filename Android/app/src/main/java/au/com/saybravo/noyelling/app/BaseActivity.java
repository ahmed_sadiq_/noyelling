package au.com.saybravo.noyelling.app;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bugsnag.android.BeforeNotify;
import com.bugsnag.android.Bugsnag;
import com.bugsnag.android.Error;

import java.io.Serializable;

import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.app.Services.JWTokenService;
import au.com.saybravo.noyelling.instructor.Domain.Instructor;

public abstract class BaseActivity extends AppCompatActivity {

    protected static final String IntentPayloadIdentifier = "Payload";
    protected static final String EmailIntentSender = "Send Mail";

    protected static final int RESULT_CLAIM_DELETED = 1;

    private int layoutResId;
    private int toolbarTitleResId;

    public BaseActivity(@LayoutRes int layoutResId)
    {
        this.layoutResId = layoutResId;
        this.toolbarTitleResId = -1;
    }

    public BaseActivity(@LayoutRes int layoutResId, @StringRes int toolbarTitleResId)
    {
        this.layoutResId = layoutResId;
        this.toolbarTitleResId = toolbarTitleResId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layoutResId);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

        Bugsnag.init(getApplicationContext());
        Instructor instructor = PreferencesManager.getInstance(getApplicationContext()).getInstructor();
        if(instructor != null) {
            String jwt = JWTokenService.getToken(getApplicationContext());
            if(jwt != null) {
                Bugsnag.setUser(jwt, instructor.email, instructor.fname+" "+instructor.lname);
            } else {
                Bugsnag.setUser(instructor.id + "", instructor.email, instructor.fname + " " + instructor.lname);
            }
        }

        initUncaughtExceptionHandler();
        initToolbar();
    }

    protected void initUncaughtExceptionHandler()
    {
        final Thread.UncaughtExceptionHandler existingHandler = Thread.getDefaultUncaughtExceptionHandler();

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable e) {

                Intent intent = new Intent(getApplicationContext(), CrashActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

                if (existingHandler != null)
                {
                    existingHandler.uncaughtException(thread, e);
                }
            }
        });
    }

    //region Toolbar

    protected void initToolbar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);

        if (toolbar == null)
        {
            return;
        }

        boolean showTitle = toolbarTitleResId != -1;

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true); // show or hide the default home button
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout
        actionBar.setDisplayShowTitleEnabled(false); // disable the default title element here (for centered title)

        TextView toolbarTitleText = (TextView) toolbar.findViewById(R.id.toolbar_title);

        if (showTitle) {
            String toolbarTitle = getBaseContext().getResources().getText(toolbarTitleResId).toString();
            toolbarTitleText.setText(toolbarTitle);
            toolbarTitleText.setVisibility(View.VISIBLE);
        }
        else
        {
            toolbarTitleText.setText("");
            toolbarTitleText.setVisibility(View.INVISIBLE);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    protected void setToolbarTitle(String title)
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);

        if (toolbar == null)
        {
            return;
        }

        TextView toolbarTitleText = (TextView) toolbar.findViewById(R.id.toolbar_title);

        toolbarTitleText.setText(title);
        toolbarTitleText.setVisibility(View.VISIBLE);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        //if (id == R.id.action_settings) {
        //    return true;
        //}
        //else
        if(id == R.id.action_usersettings)
        {
            onUserSettingsClicked();
        }
        if(id == R.id.action_addstudent){
            onAddStudentClicked();
        }

        return super.onOptionsItemSelected(item);
    }

    protected void onAddStudentClicked(){
    }

    protected void onUserSettingsClicked()
    {
        // SD - Nought to do
    }

    //endregion

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    protected void startActivity(Class activityClass) {

        startActivity(activityClass, null);
    }

    protected void startActivity(Class activityClass, Serializable payload) {

        Intent intent = new Intent(getApplicationContext(), activityClass);

        if(payload != null)
        {
            intent.putExtra(IntentPayloadIdentifier, payload);
        }

        startActivity(intent);
    }

    protected void logInfo(String msg)
    {
        if(!NoYellingApplication.IsLive)
        {
            Log.i(this.getClass().getSimpleName(), msg);
        }
    }

    protected void logError(String msg)
    {
        Log.e(this.getClass().getSimpleName(), msg);
    }

    protected Instructor GetCurrentInstructor()
    {
        NoYellingApplication application = (NoYellingApplication) getApplication();

        if(application.CurrentInstructor != null){
            return application.CurrentInstructor;
        }
        return PreferencesManager.getInstance(getApplicationContext()).getInstructor();
    }

    protected int GetCurrentInstructorId()
    {
        NoYellingApplication application = (NoYellingApplication) getApplication();

        if(application.CurrentInstructor != null) {
            return application.CurrentInstructor.id;
        }
        Instructor instructor = PreferencesManager.getInstance(getApplicationContext()).getInstructor();
        // if instructor is null this will throw an exception
        // this should not return null
        return instructor.id;
    }

    protected void SetCurrentInstructor(Instructor instructor)
    {
        NoYellingApplication application = (NoYellingApplication) getApplication();

        application.CurrentInstructor = instructor;
        PreferencesManager.getInstance(getApplicationContext()).setInstructor(instructor);
    }

    protected void ShowGenericError()
    {
        ShowGenericError(null);
    }

    protected void ShowGenericError(@Nullable Exception e)
    {
        if (e != null)
        {
            logInfo(e.getMessage());
        }

        Toast.makeText(getApplicationContext(), getString(R.string.Login_ErrorMessage_Text), Toast.LENGTH_LONG).show();
    }

    protected boolean IsNetworkAvailable() {

        ConnectivityManager connectivityManager  = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    protected void ShowNoConnectionNotification(View view, final View.OnClickListener retryListener)
    {
        Snackbar snackbar = Snackbar.make(view, R.string.app_nointernetconnection_text, Snackbar.LENGTH_LONG)
                .setAction("RETRY", retryListener);

        snackbar.setActionTextColor(Color.YELLOW);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);

        snackbar.show();
    }

    protected TextView setText(int id, String value)
    {
        TextView textView = (TextView) this.findViewById(id);

        textView.setText(value);

        return textView;
    }

    protected String GetTextFromResource(int id){

        return getResources().getText(id).toString();
    }

    // TODO SD - This is a bit stupid now, having overloads for everything.. maybe tidy this.
    protected void StartEmailIntent(int subjectResourceId, int textResourceId, int emailToResourceId)
    {
        StartEmailIntent(subjectResourceId, GetTextFromResource(textResourceId), GetTextFromResource(emailToResourceId));
    }

    protected void StartEmailIntent(int subjectResourceId, String text, int emailToResourceId)
    {
        StartEmailIntent(subjectResourceId, text, GetTextFromResource(emailToResourceId));
    }

    protected void StartEmailIntent(int subjectResourceId, int textResourceId, String emailTo)
    {
        StartEmailIntent(subjectResourceId, GetTextFromResource(textResourceId), emailTo);
    }

    protected void StartEmailIntent(int subjectResourceId, String text, String emailTo)
    {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, GetTextFromResource(subjectResourceId));
        emailIntent.putExtra(Intent.EXTRA_TEXT, text);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] { emailTo });

        startActivity(Intent.createChooser(emailIntent, EmailIntentSender));
    }
}
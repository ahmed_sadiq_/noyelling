package au.com.saybravo.noyelling.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;

import au.com.saybravo.noyelling.app.Domain.Auth;
import au.com.saybravo.noyelling.app.Domain.AuthBody;
import au.com.saybravo.noyelling.app.Helpers.ValidationHelper;
import au.com.saybravo.noyelling.app.ProgressButton.ProgressButton;
import au.com.saybravo.noyelling.app.ProgressButton.ProgressButtonCallback;
import au.com.saybravo.noyelling.app.Services.JWTokenService;
import au.com.saybravo.noyelling.app.Services.PasswordService;
import au.com.saybravo.noyelling.app.Services.ServiceGenerator.LoginService;
import au.com.saybravo.noyelling.app.Services.ServiceGenerator;
import au.com.saybravo.noyelling.app.Domain.JWToken;

import android.widget.Toast;
import android.widget.ViewAnimator;

import au.com.saybravo.noyelling.login.Domain.ForgotPasswordBody;
import au.com.saybravo.noyelling.login.Domain.ForgotPasswordResponse;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Callback;

import au.com.saybravo.noyelling.app.BaseActivity;
import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.dashboard.DashboardActivity;

public class LoginActivity extends BaseActivity {

    Activity activity;
    ViewAnimator viewAnimator;
    private ProgressButton signInButton;
    private ProgressButton forgotPasswordSubmitButton;

    public LoginActivity() {
        super(R.layout.activity_login);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        gotoDashboardIfAlreadyLoggedIn();

        activity = this;

        viewAnimator = (ViewAnimator) findViewById(R.id.viewAnimator);

        //region Login

        signInButton = new ProgressButton(this, R.id.SubmitButton, R.string.Login_SignInButton_Text, R.string.Login_SignInButton_LoadingText, new ProgressButtonCallback() {
            @Override
            public void callback() {
                login();
            }
        });

        Button forgotPasswordButton = (Button) findViewById(R.id.ForgotPasswordButton);
        forgotPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                viewAnimator.setInAnimation(AnimationUtils.loadAnimation(activity, R.anim.pull_in_right));
                viewAnimator.setOutAnimation(AnimationUtils.loadAnimation(activity, R.anim.push_out_left));
                viewAnimator.showNext();
            }
        });

        //endregion

        //region Forgot password

        forgotPasswordSubmitButton = new ProgressButton(this, R.id.ForgotPasswordSubmitButton, R.string.ForgotPassword_SubmitButton_Text, R.string.ForgotPassword_SubmitButton_LoadingText, new ProgressButtonCallback() {
            @Override
            public void callback() {
                forgotPasswordSubmit();
            }
        });

        Button iRememberMyPasswordButton = (Button) findViewById(R.id.IRememberMyPasswordButton);
        iRememberMyPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (forgotPasswordSubmitButton.isLoading) {

                    return;
                }

                viewAnimator.setInAnimation(AnimationUtils.loadAnimation(activity, R.anim.pull_in_left));
                viewAnimator.setOutAnimation(AnimationUtils.loadAnimation(activity, R.anim.push_out_right));
                viewAnimator.showPrevious();
            }
        });

        Button backToLoginButton = (Button) findViewById(R.id.BackToLoginButton);
        backToLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                viewAnimator.setInAnimation(AnimationUtils.loadAnimation(activity, R.anim.pull_in_left));
                viewAnimator.setOutAnimation(AnimationUtils.loadAnimation(activity, R.anim.push_out_right));
                viewAnimator.setDisplayedChild(0);
            }
        });

        //endregion
    }

    @Override
    protected void onStart() {
        super.onStart();

        gotoDashboardIfAlreadyLoggedIn();
    }

    private void gotoDashboardIfAlreadyLoggedIn() {
        String jWToken = JWTokenService.getToken(getApplicationContext());
        if (jWToken != null && !jWToken.isEmpty() && !jWToken.equals("null"))
        {
            Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);// clear back stack
            startActivity(intent);
            finish();
        }
    }

    //region Login

    private void login() {

        EditText emailEditText = (EditText) findViewById(R.id.Email);
        EditText passwordEditText = (EditText) findViewById(R.id.Password);

        emailEditText.setError(null);
        passwordEditText.setError(null);

        final String email = emailEditText.getText().toString();
        final String password = passwordEditText.getText().toString();

        boolean hasErrors = false;

        if (TextUtils.isEmpty(email)) {

            emailEditText.setError(getString(R.string.Validation_EmailIsRequired));
            hasErrors = true;
        }

        if (TextUtils.isEmpty(password)) {

            passwordEditText.setError(getString(R.string.Validation_PasswordIsRequired));
            hasErrors = true;
        }

        if (hasErrors) {

            resetSignInButtonText();
            return;
        }

        if (!IsNetworkAvailable())
        {
            resetSignInButtonText();

            ShowNoConnectionNotification(findViewById(R.id.LoginView), new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    login();
                }
            });

            return;
        }


        LoginService loginService = ServiceGenerator.createService(LoginService.class,getApplicationContext(), true);

        AuthBody authBody = new AuthBody();
        authBody.auth = new Auth();
        authBody.auth.email = email;        // "jmboyschau@gmail.com.local";
        authBody.auth.password = password;  // "NoTelling99!";

        Call<JWToken> call = loginService.Basiclogin(authBody);
        call.enqueue(new Callback<JWToken>() {
            @Override
            public void onResponse(Call<JWToken> call, Response<JWToken> response) {
                try {
                    if (response.isSuccessful()) {
                        JWToken result = response.body();
                        JWTokenService.setToken(result.jwt, getApplicationContext());
                        PasswordService.setPassword(password, getApplicationContext());
                        PasswordService.setUserName(email, getApplicationContext());

                        Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                        startActivity(intent);
                    } else {
                        // Handle errors

                        setText(R.id.LoginMessage, getString(R.string.Login_InvalidLoginMessage_Text));

                        resetSignInButtonText();
                    }

                } catch (Exception e) {
                    //e.printStackTrace();

                    resetSignInButtonText();

                    Toast.makeText(getApplicationContext(), getString(R.string.Login_ErrorMessage_Text) + " "  + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<JWToken> call, Throwable t) {

                resetSignInButtonText();

                Toast.makeText(getApplicationContext(), getString(R.string.Login_ErrorMessage_Text)  + " "  + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void resetSignInButtonText()
    {
        signInButton.hideLoading();
    }

    //endregion

    //region Forgot password

    private void forgotPasswordSubmit() {

        final EditText mEmailView = (EditText) findViewById(R.id.ForgotPasswordEmail);

        mEmailView.setError(null);

        String email = mEmailView.getText().toString();

        if (TextUtils.isEmpty(email)) {

            mEmailView.setError(getString(R.string.Validation_EmailIsRequired));
            cancelSubmit(mEmailView);

            return;
        }
        else if (!ValidationHelper.IsValidEmail(email))
        {
            mEmailView.setError(getString(R.string.Validation_EmailIsNotValid));
            cancelSubmit(mEmailView);

            return;
        }

        if (!IsNetworkAvailable())
        {
            ResetSubmitButtonText();

            ShowNoConnectionNotification(findViewById(R.id.LoginView), new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    forgotPasswordSubmit();
                }
            });

            return;
        }

        ServiceGenerator.InstructorService instructorService = ServiceGenerator.createService(ServiceGenerator.InstructorService.class,getApplicationContext(), true);

        Call<ForgotPasswordResponse> call = instructorService.ForgotPassword(email, new ForgotPasswordBody());
        call.enqueue(new Callback<ForgotPasswordResponse>() {
            @Override
            public void onResponse(Call<ForgotPasswordResponse> call, Response<ForgotPasswordResponse> response) {
                try {

                    if (response.isSuccessful()) {

                        viewAnimator.setInAnimation(AnimationUtils.loadAnimation(activity, R.anim.pull_in_right));
                        viewAnimator.setOutAnimation(AnimationUtils.loadAnimation(activity, R.anim.push_out_left));
                        viewAnimator.showNext();

                        forgotPasswordSubmitButton.hideLoading();

                    } else {

                        mEmailView.setError(getString(R.string.ForgotPassword_InvalidEmailValidationMessage));

                        ResetSubmitButtonText();
                    }

                } catch (Exception e) {

                    ShowGenericError();
                    ResetSubmitButtonText();
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordResponse> call, Throwable t) {

                ShowGenericError();
                ResetSubmitButtonText();
            }
        });
    }

    private void ResetSubmitButtonText()
    {
        forgotPasswordSubmitButton.hideLoading();
    }

    private void cancelSubmit(View focusView)
    {
        ResetSubmitButtonText();
        focusView.requestFocus();
    }

    //endregion
}

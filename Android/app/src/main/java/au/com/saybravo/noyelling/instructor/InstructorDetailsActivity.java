package au.com.saybravo.noyelling.instructor;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.app.BaseActivity;
import au.com.saybravo.noyelling.app.Helpers.ValidationHelper;
import au.com.saybravo.noyelling.app.ProgressButton.ProgressButton;
import au.com.saybravo.noyelling.app.ProgressButton.ProgressButtonCallback;
import au.com.saybravo.noyelling.app.Services.JWTRefreshService;
import au.com.saybravo.noyelling.app.Services.ServiceGenerator;
import au.com.saybravo.noyelling.instructor.Domain.Instructor;
import au.com.saybravo.noyelling.instructor.Domain.InstructorBody;
import au.com.saybravo.noyelling.instructor.Domain.InstructorUpdate;
import au.com.saybravo.noyelling.instructor.Domain.InstructorUpdateBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InstructorDetailsActivity extends BaseActivity {

    public InstructorDetailsActivity() {
        super(R.layout.activity_instructor_details, R.string.InstructorDetails_HeaderText);
    }

    Activity activity;
    private Instructor instructor;
    private ProgressButton saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = this;
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        instructor = GetCurrentInstructor();
        if(instructor == null)
        {
            ShowGenericError();
            return;
        }

        bindInstructorDetailsToView();

        LinearLayout scrollLayout = (LinearLayout) findViewById(R.id.ScrollLayout);
        scrollLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hideSoftKeyboard(InstructorDetailsActivity.this);
            }
        });

        saveButton = new ProgressButton(this, R.id.SaveButton, R.string.InstructorDetails_SaveButton_Text, R.string.InstructorDetails_SaveButton_LoadingText, new ProgressButtonCallback() {
            @Override
            public void callback() {
                save();
            }
        });


        // SD - This is a bit hacky, the design has only part of the text in green, but the android solution didnt seem compatible with all phones
        TextView readOnlyEditText1 = (TextView) findViewById(R.id.ReadOnlyEditText1);
        readOnlyEditText1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openEmailToNoYelling();
            }
        });

        TextView readOnlyEditText2 = (TextView) findViewById(R.id.ReadOnlyEditText2);
        readOnlyEditText2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openEmailToNoYelling();
            }
        });
    }

    public void hideSoftKeyboard(Activity activity) {

        ((InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    private void bindInstructorDetailsToView() {

        int selectedGender;
        switch (instructor.gender)
        {
            case "male":
                selectedGender = R.id.GenderMale;
                break;

            case "female":
                selectedGender = R.id.GenderFemale;
                break;

            default:
                selectedGender = R.id.GenderOther;
                break;
        }

        ((RadioButton) activity.findViewById(selectedGender)).setChecked(true);

        setText(R.id.FirstName, instructor.fname);
        setText(R.id.LastName, instructor.lname);
        setText(R.id.MobileNumber, instructor.mobileNumber);
        setText(R.id.EmailAddress, instructor.email);
        setText(R.id.AboutMe, instructor.about);
        setText(R.id.Transmission, instructor.transmission.equalsIgnoreCase("automatic") ? "Automatic" : "Manual");
        setText(R.id.CarDescription, instructor.carInfo);
        setText(R.id.CarRegistration, instructor.carRego);
        setText(R.id.BusinessInfoGSTRegistered, instructor.gstRegistered != null && instructor.gstRegistered ? "Yes" : "No" );
        setText(R.id.BusinessInfoKeysToDrive, instructor.keysToDrive != null && instructor.keysToDrive ? "Yes" : "No");
        setText(R.id.BusinessInfoBSB, instructor.bsb != null ? instructor.bsb : "-");
        setText(R.id.BusinessInfoAccountNumber, instructor.accountNumber != null ? instructor.accountNumber : "-");
    }

    private void save() {

        EditText firstName = (EditText) activity.findViewById(R.id.FirstName);
        EditText lastName = (EditText) activity.findViewById(R.id.LastName);
        EditText mobileNumber = (EditText) activity.findViewById(R.id.MobileNumber);
        EditText email = (EditText) activity.findViewById(R.id.EmailAddress);
        EditText about = (EditText) activity.findViewById(R.id.AboutMe);

        boolean hasErrors = false;

        if (TextUtils.isEmpty(firstName.getText())) {

            firstName.setError(getString(R.string.Validation_FirstNameIsRequired));
            hasErrors = true;
        }

        if (TextUtils.isEmpty(lastName.getText())) {

            lastName.setError(getString(R.string.Validation_LastNameIsRequired));
            hasErrors = true;
        }

        if (TextUtils.isEmpty(mobileNumber.getText())) {

            mobileNumber.setError(getString(R.string.Validation_MobileNumberIsRequired));
            hasErrors = true;
        }
        else if (!ValidationHelper.IsValidMobileNumber(mobileNumber.getText().toString()))
        {
            mobileNumber.setError(getString(R.string.Validation_MobileNumberIsNotValid));
            hasErrors = true;
        }

        if (TextUtils.isEmpty(email.getText())) {

            email.setError(getString(R.string.Validation_EmailIsRequired));
            hasErrors = true;
        }
        else if (!ValidationHelper.IsValidEmail(email.getText().toString()))
        {
            email.setError(getString(R.string.Validation_EmailIsNotValid));
            hasErrors = true;
        }

        if (TextUtils.isEmpty(about.getText())) {

            about.setError(getString(R.string.Validation_Instructor_AboutMeIsRequired));
            hasErrors = true;
        }

        if (hasErrors)
        {
            resetSaveButtonText();

            return;
        }

        if (!IsNetworkAvailable())
        {
            resetSaveButtonText();

            ShowNoConnectionNotification(findViewById(R.id.InstructorDetailsView), new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    save();
                }
            });

            return;
        }


        ServiceGenerator.InstructorService instructorService = ServiceGenerator.createService(ServiceGenerator.InstructorService.class, getApplicationContext());

        final InstructorUpdate instructorUpdate = new InstructorUpdate();

        instructorUpdate.gender = getGenderValue();
        instructorUpdate.fname = firstName.getText().toString();
        instructorUpdate.lname = lastName.getText().toString();
        instructorUpdate.mobileNumber = mobileNumber.getText().toString();
        instructorUpdate.email = email.getText().toString();
        instructorUpdate.about = about.getText().toString();

        InstructorUpdateBody body = new InstructorUpdateBody(instructorUpdate);

        Call<InstructorBody> call = instructorService.Update(body);

        JWTRefreshService.enqueueSafe(call, this, new Callback<InstructorBody>() {
            @Override
            public void onResponse(Call<InstructorBody> call, Response<InstructorBody> response) {

                if (response.isSuccessful()) {

                    instructor = response.body().getInstructor();
                    SetCurrentInstructor(instructor);

                    resetSaveButtonText();
                }
                else
                {
                    ShowGenericError();
                    resetSaveButtonText();
                }
            }

            @Override
            public void onFailure(Call<InstructorBody> call, Throwable t) {

                ShowGenericError();
                resetSaveButtonText();
            }
        });
    }

    private String getGenderValue()
    {
        RadioGroup genderRadioGroup = (RadioGroup) activity.findViewById(R.id.GenderRadioGroup);

        switch (genderRadioGroup.getCheckedRadioButtonId())
        {
            case R.id.GenderMale:
                return "male";

            case R.id.GenderFemale:
                return "female";

            default:
                return "other";
        }
    }

    private void resetSaveButtonText()
    {
        saveButton.hideLoading();
    }

    private void openEmailToNoYelling() {

        StartEmailIntent(R.string.InstructorDetails_ContactNoYelling_Subject, R.string.InstructorDetails_ContactNoYelling_Text, R.string.InstructorDetails_ContactNoYelling_EmailTo);
    }

    @Override
    public void onBackPressed() {

        boolean showConfirmation = false;

        if (!instructor.gender.equals(getGenderValue()))
        {
            showConfirmation = true;
        }

        EditText firstName = (EditText) activity.findViewById(R.id.FirstName);
        if (!instructor.fname.equals(firstName.getText().toString()))
        {
            showConfirmation = true;
        }

        EditText lastName = (EditText) activity.findViewById(R.id.LastName);
        if (!instructor.lname.equals(lastName.getText().toString()))
        {
            showConfirmation = true;
        }

        EditText mobileNumber = (EditText) activity.findViewById(R.id.MobileNumber);
        if (!instructor.mobileNumber.equals(mobileNumber.getText().toString()))
        {
            showConfirmation = true;
        }

        EditText email = (EditText) activity.findViewById(R.id.EmailAddress);
        if (!instructor.email.equals(email.getText().toString()))
        {
            showConfirmation = true;
        }

        EditText about = (EditText) activity.findViewById(R.id.AboutMe);
        if (!instructor.about.equals(about.getText().toString()))
        {
            showConfirmation = true;
        }

        if (showConfirmation)
        {
            new AlertDialog.Builder(this, R.style.AlertDialogCustom).setTitle(GetTextFromResource(R.string.ChangesMadeConfirmation_Title))
                    .setMessage(GetTextFromResource(R.string.ChangesMadeConfirmation_Text))
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int whichButton) {
                            goBack();
                        }
                    }).setNegativeButton("no", null).show();
        }
        else {

            goBack();
        }
    }

    private void goBack()
    {
        super.onBackPressed();
    }
}

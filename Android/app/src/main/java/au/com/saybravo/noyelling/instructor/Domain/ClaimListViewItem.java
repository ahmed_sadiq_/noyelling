package au.com.saybravo.noyelling.instructor.Domain;

import au.com.saybravo.noyelling.student.Domain.Student;

public class ClaimListViewItem {

    public ClaimsListViewItemType Type;
    public String MainText;
    public String SubText;
    public Integer OriginalPosition;
    public Integer PackageDetailId;
    public Integer ClaimId;
    public Student Student;
    public Double TotalTimeInHours;
    public String ClaimType;

    public ClaimListViewItem(ClaimsListViewItemType type, String mainText, String subText, Integer originalPosition, Integer claimId, Integer packageDetailId, Student student, String claimType)
    {
        Type = type;
        MainText = mainText;
        SubText = subText;
        OriginalPosition = originalPosition;
        PackageDetailId = packageDetailId;
        this.Student = student;
        ClaimId = claimId;
        this.ClaimType = claimType;
    }

    public enum ClaimsListViewItemType
    {
        UnreconciledHeader,
        UnreconciledItem,
        PendingHeader,
        PendingItem,
        PaidHeader,
        PaidItem
    }
}




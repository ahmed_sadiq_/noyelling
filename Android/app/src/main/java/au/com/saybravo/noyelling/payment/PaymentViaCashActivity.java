package au.com.saybravo.noyelling.payment;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.Locale;

import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.app.BaseActivity;
import au.com.saybravo.noyelling.app.ProgressButton.ProgressButton;
import au.com.saybravo.noyelling.app.ProgressButton.ProgressButtonCallback;
import au.com.saybravo.noyelling.app.Services.JWTRefreshService;
import au.com.saybravo.noyelling.app.Services.ServiceGenerator;
import au.com.saybravo.noyelling.app.util.AppConstants;
import au.com.saybravo.noyelling.payment.Domain.Payment;
import au.com.saybravo.noyelling.payment.Domain.PaymentBody;
import au.com.saybravo.noyelling.student.Domain.EmailBody;
import au.com.saybravo.noyelling.student.Domain.Student;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentViaCashActivity extends BaseActivity {


    ProgressButton actionButton;
    Button cancelButton;
    TextView toolBarTitle;
    Boolean isLoading = false;
    EditText amountReceived;
    TextView labelCashDue;

    private Student student;
    private PaymentBody paymentBody;
    double amountReceivedDouble;
    double amountToDeduct;

    public PaymentViaCashActivity() {
        super(R.layout.activity_payment_via_cash);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            student = (Student) bundle.getSerializable(AppConstants.INTENT_KEY_STUDENT);
            paymentBody = (PaymentBody) bundle.getSerializable(AppConstants.INTENT_KEY_PAYMENT);
        }
        init();
    }

    private String current = "";
    private void init() {
        toolBarTitle = (TextView) findViewById(R.id.toolbar_title);
        amountReceived = (EditText) findViewById(R.id.amountReceived);
        labelCashDue = (TextView) findViewById(R.id.labelCashDue);
        amountReceived.requestFocus();
        labelCashDue.setText(String.format(getString(R.string.Title_Change_Due),0d));
        amountToDeduct = paymentBody.getPayment().getPackage().getSalePrice();
        amountReceived.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().equals(current)){
                    amountReceived.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[$,.]", "");
                    double parcedValue = Double.parseDouble(cleanString);
                    amountReceivedDouble = parcedValue/100;
                    Locale locale = new Locale("en", "US");
                    String formatted = NumberFormat.getCurrencyInstance(locale).format(amountReceivedDouble);
                    double changeDue = amountReceivedDouble - amountToDeduct;
                    if(changeDue < 0) {
                        changeDue = 0.00f;
                    }
                    labelCashDue.setText(String.format(getString(R.string.Title_Change_Due),changeDue));
                    current = formatted;
                    PaymentViaCashActivity.this.amountReceived.setText(formatted);
                    PaymentViaCashActivity.this.amountReceived.setSelection(formatted.length());
                    PaymentViaCashActivity.this.amountReceived.addTextChangedListener(this);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        cancelButton = (Button) findViewById(R.id.CancelButton);
        View layoutContainer = findViewById(R.id.layoutContainer);
        actionButton = new ProgressButton(layoutContainer, R.id.ActionButton,
                R.string.PaymentMethod_Cash_Action,
                R.string.LessonPackage_CreateBtnProcessingText,
                isLoading != null && isLoading.booleanValue(),
                new ProgressButtonCallback() {
                    @Override
                    public void callback() {
                        if (isLoading == null || isLoading == false) {
                            isLoading = true;
                            processInputs();
                        }
                    }
                });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(paymentBody != null) {
                    Payment paymentInfo = paymentBody.getPayment();
                    deletePayment(paymentInfo.getStudent().getId(), paymentInfo.getId());
                    onBackPressed();
                }
            }
        });
        if(paymentBody != null) {
            toolBarTitle.setText("Total $"+paymentBody.getPayment().getPackage().getSalePrice());
        }
    }

    private void processInputs(){
        String validityError = checkInputValidity();

        if(validityError.isEmpty()){
            markCashPaymentAsPaid();
        } else {
            Toast.makeText(this, validityError, Toast.LENGTH_LONG).show();
            updateProgressButton(false, getString(R.string.PaymentMethod_Cash_Action));
        }
    }

    private String checkInputValidity() {
        String errorString = "";

        if(amountReceivedDouble == 0 ) {
            errorString = getString(R.string.error_amount_validity);
        }else {
            if(amountReceivedDouble < amountToDeduct) {
                errorString = getString(R.string.error_amount_validity);
            }
        }
        return errorString;
    }

    private void updateProgressButton(boolean loadingStatus, String text) {
        if(actionButton != null) {
            isLoading = loadingStatus;
            actionButton.setLoadingStatus(loadingStatus);
            actionButton.setText(text);
        }
    }

    public void markCashPaymentAsPaid() {
        updateProgressButton(true, getString(R.string.LessonPackage_CreateBtnProcessingText));
        ServiceGenerator.StudentService studentService =
                ServiceGenerator.createService(ServiceGenerator.StudentService.class, this );

        Call<PaymentBody> call = studentService.ChargeCashPayment(student.Id, paymentBody.getPayment().getId());

        JWTRefreshService.enqueueSafe(call, this, new Callback<PaymentBody>() {
            @Override
            public void onResponse(Call<PaymentBody> call, Response<PaymentBody> response) {
                if(response.isSuccessful()) {

                    Intent intent = new Intent(PaymentViaCashActivity.this, PaymentSuccessfulActivity.class);
                    intent.putExtra(AppConstants.INTENT_KEY_STUDENT, student);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(PaymentViaCashActivity.this,response.message()+"/"+response.code(), Toast.LENGTH_LONG).show();
                    updateProgressButton(false, getString(R.string.Send_Payment_Link));
                }
            }
            @Override
            public void onFailure(Call<PaymentBody> call, Throwable t) {
                Toast.makeText(PaymentViaCashActivity.this,"Something Went Wrong.", Toast.LENGTH_LONG).show();
                updateProgressButton(false, getString(R.string.PaymentMethod_Cash_Action));
            }
        });
    }

    private void deletePayment(final int studentId, final int paymentId){

        ServiceGenerator.ClaimService productService = ServiceGenerator.createService(ServiceGenerator.ClaimService.class, getApplicationContext());
        Call<Void> call = productService.DeletePendingPayment(studentId, paymentId);

        JWTRefreshService.enqueueSafe(call, this, new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Toast.makeText(getApplicationContext(), R.string.Payment_Cancel_Message, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.i("","");
            }
        });

    }

    @Override
    public void onBackPressed() {
        if(paymentBody != null) {
            Payment paymentInfo = paymentBody.getPayment();
            deletePayment(paymentInfo.getStudent().getId(), paymentInfo.getId());
        }
        super.onBackPressed();
    }
}

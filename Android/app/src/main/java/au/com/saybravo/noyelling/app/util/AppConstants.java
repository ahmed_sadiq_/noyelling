package au.com.saybravo.noyelling.app.util;

/**
 * Created by ahmed.sadiq on 24/11/2016.
 */

public class AppConstants {
    public enum PAYMENT_METHOD {
        TAP_AND_GO(1), CREDIT_CARD(2), SEND_LINK(3), CASH(4);
        int value;
        PAYMENT_METHOD(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    };

    public enum CLAIM_TYPE{
        CREDIT("Credit"), DEBIT("Debit");
        String value;
        CLAIM_TYPE(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
    public enum PAYMENT_TYPE {
        CASH("CASH"), EFT("EFT");

        String value;
        PAYMENT_TYPE(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    };

    public static String INTENT_KEY_PAYMENT_METHOD = "payment_method";
    public static String INTENT_KEY_PAYMENT = "payment";
    public static String INTENT_KEY_STUDENT = "student";

    public static int CREDIT_CARD_NUMBER_LENGTH = 16;
    public static int CREDIT_CARD_CVC_NUMBER_LENGTH = 3;
}

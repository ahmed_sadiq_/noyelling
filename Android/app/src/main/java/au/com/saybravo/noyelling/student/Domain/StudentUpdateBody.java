package au.com.saybravo.noyelling.student.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import au.com.saybravo.noyelling.student.Domain.StudentUpdate;

public class StudentUpdateBody {

    @SerializedName("student")
    @Expose
    public StudentUpdate student;

    public StudentUpdateBody(StudentUpdate student)
    {
        this.student = student;
    }
}

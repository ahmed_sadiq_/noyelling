package au.com.saybravo.noyelling.lessonPackage.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import au.com.saybravo.noyelling.lessonPackage.Domain.Product;

/**
 * Created by ahmed.sadiq on 30/05/16.
 */
public class ProductsBody {

    @SerializedName("products")
    @Expose
    private List<Product> products = new ArrayList<Product>();

    /**
     *
     * @return
     * The products
     */
    public List<Product> getProducts() {
        return products;
    }

    /**
     *
     * @param products
     * The products
     */
    public void setProducts(List<Product> products) {
        this.products = products;
    }


}
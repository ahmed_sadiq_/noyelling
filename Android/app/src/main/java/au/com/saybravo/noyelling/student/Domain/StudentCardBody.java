package au.com.saybravo.noyelling.student.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StudentCardBody {

    @SerializedName("card")
    @Expose
    public StudentCard studentCard;

    public StudentCardBody(StudentCard student)
    {
        this.studentCard = student;
    }
}

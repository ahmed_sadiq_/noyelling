package au.com.saybravo.noyelling.student.Domain;

import java.util.Date;

import io.realm.RealmObject;

/**
 * Created by ahmed.sadiq on 18/04/16.
 */
public class StudentRealmModel extends RealmObject {

    public Integer id;

    public String firstName;

    public String lastName;

    public String email;

    public String suburb;

    public Date lastAccessed;

}

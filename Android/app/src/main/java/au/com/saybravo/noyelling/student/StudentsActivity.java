package au.com.saybravo.noyelling.student;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.View;
import java.util.List;

import au.com.saybravo.noyelling.app.BaseActivity;
import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.student.ViewPager.StudentsFragment;
import au.com.saybravo.noyelling.student.ViewPager.StudentsViewPagerAdapter;

public class StudentsActivity extends BaseActivity implements SearchView.OnQueryTextListener {

    private ViewPager viewPager;
    private SearchView searchView;
    private CharSequence tabs[];


    public StudentsActivity() {
        super(R.layout.activity_students, R.string.Students_HeaderText);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        tabs = new CharSequence[] { getResources().getText(R.string.Students_LatestActive), getResources().getText(R.string.Students_A_Z)};

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        searchView = (SearchView) findViewById(R.id.studentsearch);

        String searchHint = getResources().getText(R.string.Students_SearchHint).toString();
        searchView.setQueryHint(searchHint);

        searchView.setIconified(false);
        searchView.setIconifiedByDefault(false);

        searchView.setFocusable(false);
        searchView.clearFocus();
        searchView.setOnQueryTextListener(this);

        final StudentsViewPagerAdapter viewPagerAdapter = new StudentsViewPagerAdapter(getSupportFragmentManager(), tabs);

        viewPager.setAdapter(viewPagerAdapter);


        viewPager.requestFocus();


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                String title = viewPagerAdapter.getPageTitle(position).toString();
                String allTab = getResources().getText(R.string.Students_A_Z).toString();

               if( title.compareTo(allTab) == 0  ){
                   List<Fragment> fragments = getSupportFragmentManager().getFragments();
                   for (Fragment fragment: fragments) {
                       if( fragment instanceof StudentsFragment){
                           StudentsFragment studentsFragment = (StudentsFragment)fragment;
                           studentsFragment.initializeStudents();
                       }

                   }

                   return;
               }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tablayout);

        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        searchView.clearFocus();
        viewPager.requestFocus();
        setNewQueryString(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        if(newText.isEmpty()){
            setNewQueryString(null);
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.menu_students, menu);

        return true;
    }

    @Override
    protected void onAddStudentClicked(){

        startActivity(AddStudentActivity.class);
    }

    public void skipRecentStudents(){

        viewPager.setCurrentItem(1);
  }

    public boolean isNetworkConnected(){
        return this.IsNetworkAvailable();
    }

    public void showNoConnectionNotification(View.OnClickListener retryListener){

        ShowNoConnectionNotification(findViewById(R.id.StudentsView), retryListener);
    }

    private void setNewQueryString(String query){

        List<Fragment> fragments = getSupportFragmentManager().getFragments();

        for(Fragment fragment: fragments) {

            QueryExecutor executor = (QueryExecutor)fragment;
            executor.setQueryString(query);
        }
    }
}


package au.com.saybravo.noyelling.student.ViewPager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by ahmed.sadiq on 13/04/16.
 */
public class StudentsViewPagerAdapter extends FragmentStatePagerAdapter {

    private CharSequence[] tabNames;

    public StudentsViewPagerAdapter(FragmentManager fm, CharSequence[] tabNames){
        super(fm);
        this.tabNames = tabNames;
    }
    @Override
    public Fragment getItem(int position) {

        if(position == 0 ) {
            Fragment fragment =  new RecentStudentsFragment();
            return fragment;

        }else{
           Fragment fragment = new StudentsFragment();
            return fragment;
        }
    }

    @Override
    public CharSequence getPageTitle(int position){
        return tabNames[position];
    }

    @Override
    public int getCount() {
        return tabNames.length;
    }
}

package au.com.saybravo.noyelling.instructor;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.DateTime;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import au.com.saybravo.noyelling.app.BaseActivity;
import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.app.Helpers.StringHelper;
import au.com.saybravo.noyelling.app.NoYellingApplication;
import au.com.saybravo.noyelling.app.Services.JWTRefreshService;
import au.com.saybravo.noyelling.app.Services.ServiceGenerator;
import au.com.saybravo.noyelling.app.util.AppConstants;
import au.com.saybravo.noyelling.claim.ClaimActivity;
import au.com.saybravo.noyelling.instructor.Domain.ClaimListViewItem;
import au.com.saybravo.noyelling.instructor.Domain.ClaimsBody;
import au.com.saybravo.noyelling.instructor.Domain.ClaimsStatistics;
import au.com.saybravo.noyelling.instructor.Domain.ClaimsStatisticsBody;
import au.com.saybravo.noyelling.instructor.Domain.Instructor;
import cn.jeesoft.widget.pickerview.CharacterPickerWindow;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransactionsActivity extends BaseActivity  {

    private TextView weekRangeTextView;
    private TextView weeklyEarningsTextView;
    private TextView weeklyHoursTextView;
    private ListView listView;
    private FrameLayout overlayFrame;
    private RelativeLayout noTransactionsTextViewLinearLayout;
    private Call<ClaimsStatisticsBody> onGoingClaimStatisticsCall;
    private Call<ClaimsBody> ongoingClaimCall;


    private ImageButton leftButton;
    private ImageButton rightButton;
    private CharacterPickerWindow characterPickerWindow;


    private Integer currentWeek = 0;
    private Instructor instructor;
    private android.os.Handler delayExecutionHandler = new android.os.Handler();
    private Runnable delayedGetData;

    private DateTime firstTransactionWeekStartDate;
    private boolean initialLoadDone = false;
    private List<String> pickerOptions = new ArrayList<>();

    TransactionsListViewAdapter transactionsListViewAdapter;

    public TransactionsActivity() {
        super(R.layout.activity_transactions, R.string.TransactionsDetails_HeaderText);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        bindTransactionToView();

        setupTransactionSelectionButtons();
        overlayFrame.getForeground().setAlpha(0);

        instructor = GetCurrentInstructor();

        if(instructor == null){
            finish();
            return;
        }
        startLoading();
        LoadWeekData();
    }

    private void bindTransactionToView(){

        leftButton = (ImageButton) findViewById(R.id.LeftButton);
        rightButton = (ImageButton) findViewById(R.id.RightButton);
        weekRangeTextView = (TextView) findViewById(R.id.WeekRangeTextView);
        weeklyEarningsTextView = (TextView) findViewById(R.id.WeeklyEarningsTextView);
        noTransactionsTextViewLinearLayout = (RelativeLayout) findViewById(R.id.NoTransactionsTextViewLinearLayout);
        weeklyHoursTextView = (TextView) findViewById(R.id.WeeklyHoursTextView);
        overlayFrame = (FrameLayout) findViewById(R.id.overlayframe);
        listView = (ListView) findViewById(R.id.ClaimsListView);
        weekRangeTextView.setText("");

        transactionsListViewAdapter = new TransactionsListViewAdapter(this, R.layout.layout_transactions_listview);

        listView.setAdapter(transactionsListViewAdapter);

        setupCharacterView();

    }

    private void setupCharacterView(){

        characterPickerWindow = new CharacterPickerWindow(this);
        characterPickerWindow.setPicker(pickerOptions);
        characterPickerWindow.setSelectOptions(0);
        characterPickerWindow.setOnoptionsSelectListener(new CharacterPickerWindow.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int option2, int options3) {
                weekSelected(options1);

            }
        });

        characterPickerWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                startFadeAnimation(false);
            }
        });

    }

    private void setupTransactionSelectionButtons(){

        rightButton.setVisibility(View.INVISIBLE);

        weekRangeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (initialLoadDone == false) return;

                startFadeAnimation(true);

                Rect loc = locateView(view);
                if(loc != null) {

                    if(characterPickerWindow.isShowing())
                    {
                        characterPickerWindow.dismiss();
                    }else{
                        characterPickerWindow.showAtLocation(view, Gravity.TOP, 0, loc.bottom);
                    }
                }
            }

        });

        leftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(initialLoadDone == false){
                    return;
                }
                showWeeks(false);
            }
        });

        rightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showWeeks(true);
            }
        });

    }

    private void startFadeAnimation(boolean fadeIn)
    {
        int start = 0;
        int end = 120;

        if(!fadeIn){
            start = 120;
            end =0;
        }
        ValueAnimator alphaAnimator = ValueAnimator.ofInt(start,end);

        alphaAnimator.setDuration(500);

        alphaAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {

                overlayFrame.getForeground().setAlpha((int)valueAnimator.getAnimatedValue());
            }
        });

        alphaAnimator.start();
    }

    @Override
    protected void onStart() {
        super.onStart();

        NoYellingApplication app = (NoYellingApplication)getApplication();
        instructor = GetCurrentInstructor();
        app.AddedClaim = false;
    }

    @Override
    protected  void onRestart(){
        super.onRestart();

        NoYellingApplication app = (NoYellingApplication)getApplication();

        if(app.AddedClaim){

            startLoading();
            LoadWeekData();

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        if(resultCode == BaseActivity.RESULT_CLAIM_DELETED){
            startLoading();
            LoadWeekData();

        }
    }

    private Rect locateView(View v)
    {
        int[] loc_int = new int[2];
        if (v == null) return null;
        try
        {
            v.getLocationOnScreen(loc_int);
        } catch (NullPointerException npe)
        {
            //Happens when the view doesn't exist on screen anymore.
            return null;
        }
        Rect location = new Rect();
        location.left = loc_int[0];
        location.top = loc_int[1] + 35;
        location.right = location.left + v.getWidth();
        location.bottom = location.top + v.getHeight();
        return location;
    }

    private void startLoading()
    {
        transactionsListViewAdapter.clear();
        noTransactionsTextViewLinearLayout.setVisibility(LinearLayout.GONE);
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(ProgressBar.VISIBLE);
    }

    private void stopLoading()
    {
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(ProgressBar.GONE);
    }

    public void ShowTransactionDetails(final ClaimListViewItem listViewItem){

        if (!IsNetworkAvailable())
        {
            ShowNoConnectionNotification(findViewById(R.id.TansactionsView), new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    ShowTransactionDetails(listViewItem);
                }
            });

            return;
        }


        Intent intent = new Intent(getApplicationContext(), ClaimActivity.class);
        intent.putExtra("claimId", listViewItem.ClaimId);
        intent.putExtra("student", listViewItem.Student);
        intent.putExtra("hideViewCustomer", false);
        intent.putExtra("claim_type", listViewItem.ClaimType);
        startActivityForResult(intent , 0);
    }

    private void showWeeks(final Boolean forwardWeek)
    {

        if (!IsNetworkAvailable())
        {
            ShowNoConnectionNotification(findViewById(R.id.TansactionsView), new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    showWeeks(forwardWeek);
                }
            });

            return;
        }

        startLoading();


        if(forwardWeek)
            currentWeek--;
        else
            currentWeek++;

        // You cant go past this week
        if(currentWeek == 0){
            rightButton.setVisibility(View.INVISIBLE);
        }else{
            rightButton.setVisibility(View.VISIBLE);
        }
        if(currentWeek < 0)
        {
            currentWeek = 0;
            stopLoading();
            return;
        }

        if(delayedGetData != null) {
            delayExecutionHandler.removeCallbacks(delayedGetData);

        }
        delayedGetData = new Runnable() {
            public void run() {
                transactionsListViewAdapter.clearItemHeaderValues();
                LoadWeekData();

            }
        };
        delayExecutionHandler.postDelayed(
                        delayedGetData,
                        500);


        setWeekRangeDisplayTillLoad();

    }


    private void setWeekRangeDisplayTillLoad()
    {
        int daysToGoBack = 7 * currentWeek;

        DateTime startDate = firstTransactionWeekStartDate.minusDays(daysToGoBack);

        DateTime endDate = startDate.plusDays(6);

        String weekRangeText = startDate.toString("MMM dd, yyyy").toUpperCase() + " - " + endDate.toString("MMM dd, yyyy").toUpperCase()+"  "+'\u25bc';
        SpannableString content = new SpannableString(weekRangeText);
        content.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.ColorPrimaryDark)), weekRangeText.length()-1, weekRangeText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        weekRangeTextView.setText(content);
        characterPickerWindow.setSelectOptions(currentWeek);

    }

    private void LoadWeekData()
    {
        ServiceGenerator.InstructorService instructorService = ServiceGenerator.createService(ServiceGenerator.InstructorService.class, getApplicationContext());
       // if(instructor == null)
        //{
        //    ShowGenericError();
         //   return;
        //}


        if(ongoingClaimCall != null){
            ongoingClaimCall.cancel();
        }

        Call<ClaimsBody> call = instructorService.GetInstructorClaims(instructor.id, currentWeek);

        ongoingClaimCall = call;

        JWTRefreshService.enqueueSafe(call, this, new Callback<ClaimsBody>() {
            @Override
            public void onResponse(Call<ClaimsBody> call, Response<ClaimsBody> response) {

                ongoingClaimCall = null;
                try {
                    if (response.isSuccessful()) {
                        ClaimsBody result = response.body();
                        if(result.getClaims().hasTransactions())
                        {
                            listView.setVisibility(ListView.VISIBLE);
                            noTransactionsTextViewLinearLayout.setVisibility(LinearLayout.GONE);
                        }
                        else
                        {
                            listView.setVisibility(ListView.GONE);
                            noTransactionsTextViewLinearLayout.setVisibility(LinearLayout.VISIBLE);
                        }

                        SetWeekTextView(result.getWeek().getCommencing(), result.getWeek().getConcluding());
                        transactionsListViewAdapter.populateItems(result.getClaims());

                        weeklyHoursTextView.setText(StringHelper.FormatOptionalDecimal(transactionsListViewAdapter.totalHours).toString());
                        LoadStatisticsData();

                    } else {
                        ShowGenericError();
                        stopLoading();

                        logInfo("LoadWeekData failed" + currentWeek);
                    }



                } catch (Exception e) {
                    logInfo("LoadWeekData catch error " + e.getMessage());
                    ShowGenericError();
                    stopLoading();
                }
            }

            @Override
            public void onFailure(Call<ClaimsBody> call, Throwable t)
            {
                ongoingClaimCall = null;
                if(!call.isCanceled()) {
                    logInfo("LoadWeekData onFailure " + currentWeek);
                    ShowGenericError();
                    stopLoading();
                }
            }
        });
    }

    private void LoadStatisticsData()
    {
        ServiceGenerator.InstructorService instructorService = ServiceGenerator.createService(ServiceGenerator.InstructorService.class, getApplicationContext());
        //if(instructor == null)
        //{
        //    ShowGenericError();
        //    return;
        //}

        if(onGoingClaimStatisticsCall != null){
            onGoingClaimStatisticsCall.cancel();
        }
        Call<ClaimsStatisticsBody> call = instructorService.GetInstructorEarnings(instructor.id, currentWeek);

        onGoingClaimStatisticsCall = call;

        JWTRefreshService.enqueueSafe(call, this, new Callback<ClaimsStatisticsBody>() {
            @Override
            public void onResponse(Call<ClaimsStatisticsBody> call, Response<ClaimsStatisticsBody> response) {

                onGoingClaimStatisticsCall = null;
                try {
                    if (response.isSuccessful()) {

                        ClaimsStatisticsBody result = response.body();

                        NumberFormat n = NumberFormat.getCurrencyInstance(Locale.US);

                        ClaimsStatistics claimsStatistics = result.getClaimsStatistics();
                        String earnings = n.format(claimsStatistics.getTotal().getEarnings());

                        transactionsListViewAdapter.setClaimItemHeaderValues(claimsStatistics.getUnreconciled().getValue(),
                                claimsStatistics.getPending().getEarnings(), claimsStatistics.getPaid().getEarnings());

                        weeklyEarningsTextView.setText(earnings);

                    } else {
                        // Handle errors or 401
                        Toast.makeText(getApplicationContext(), "LoadWeekData failed", Toast.LENGTH_LONG).show();

                    }
                    stopLoading();

                } catch (Exception e) {

                    ShowGenericError();
                    stopLoading();
                }
            }

            @Override
            public void onFailure(Call<ClaimsStatisticsBody> call, Throwable t)
            {
                onGoingClaimStatisticsCall = null;
                if(!call.isCanceled()) {
                    ShowGenericError();
                    stopLoading();

                }
            }
        });
    }

    private void populateWeekSelector(DateTime initialStartDate)
    {
        pickerOptions.clear();
        firstTransactionWeekStartDate = initialStartDate;
        for(int i = 0; i < 52; i++){
            pickerOptions.add("W/C - "+ initialStartDate.toString("MMM dd, yyyy"));
            initialStartDate = initialStartDate.minusDays(7);
        }

    }

    private void weekSelected(final int position){

        if (!IsNetworkAvailable())
        {
            ShowNoConnectionNotification(findViewById(R.id.TansactionsView), new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    weekSelected(position);
                }
            });

            return;
        }

        startLoading();
        currentWeek = position;

        // You cant go past this week
        if(currentWeek == 0){
            rightButton.setVisibility(View.INVISIBLE);
        }else{
            rightButton.setVisibility(View.VISIBLE);
        }
        transactionsListViewAdapter.clearItemHeaderValues();
        LoadWeekData();

    }


    private void SetWeekTextView(String weekCommencingResult, String weekConcludingResult)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Calendar weekCommencingcalendar = Calendar.getInstance();
        Calendar weekConcludingcalendar = Calendar.getInstance();

        try {

            Date commencingDate = sdf.parse(weekCommencingResult);
            Date concludingDate = sdf.parse(weekConcludingResult);

            weekCommencingcalendar.setTime(commencingDate);
            weekConcludingcalendar.setTime(concludingDate);


            DateTime wcomencing = new DateTime(weekCommencingcalendar.get(Calendar.YEAR),
                    weekCommencingcalendar.get(Calendar.MONTH)+1, weekCommencingcalendar.get(Calendar.DAY_OF_MONTH), 0, 0,0);

            DateTime wconcluding = new DateTime(weekConcludingcalendar.get(Calendar.YEAR),
                    weekConcludingcalendar.get(Calendar.MONTH)+1, weekConcludingcalendar.get(Calendar.DAY_OF_MONTH), 0, 0,0);

            String weekRangeText = wcomencing.toString("MMM dd, yyyy").toUpperCase() + " - " + wconcluding.toString("MMM dd, yyyy").toUpperCase()+"  "+'\u25bc';
            SpannableString content = new SpannableString(weekRangeText);
            content.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.ColorPrimaryDark)), weekRangeText.length()-1, weekRangeText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            weekRangeTextView.setText(content);
            characterPickerWindow.setSelectOptions(currentWeek);


            if(initialLoadDone == false){
                populateWeekSelector(wcomencing);
                initialLoadDone = true;
            }

        }catch(ParseException ex){

            if(!NoYellingApplication.IsLive)
            {
                Log.i("Error", "Parsing date threw an exception "+ ex.getMessage());
            }
        }
    }

}

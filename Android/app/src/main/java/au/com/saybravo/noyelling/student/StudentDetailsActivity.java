package au.com.saybravo.noyelling.student;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.app.BaseActivity;
import au.com.saybravo.noyelling.app.DelayAutoCompleteTextView;
import au.com.saybravo.noyelling.app.Helpers.ValidationHelper;
import au.com.saybravo.noyelling.app.ProgressButton.ProgressButton;
import au.com.saybravo.noyelling.app.ProgressButton.ProgressButtonCallback;
import au.com.saybravo.noyelling.app.Services.JWTRefreshService;
import au.com.saybravo.noyelling.app.Services.ServiceGenerator;
import au.com.saybravo.noyelling.student.Domain.AddressAttributes;
import au.com.saybravo.noyelling.student.Domain.AddressAutocompleteItem;
import au.com.saybravo.noyelling.student.Domain.Student;
import au.com.saybravo.noyelling.student.Domain.StudentBody;
import au.com.saybravo.noyelling.student.Domain.StudentUpdate;
import au.com.saybravo.noyelling.student.Domain.StudentUpdateBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StudentDetailsActivity extends BaseActivity {

    public StudentDetailsActivity() {
        super(R.layout.activity_student_details, R.string.StudentDetails_HeaderText);
    }

    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

    Activity activity;
    private Student student;
    private ProgressButton saveButton;
    DelayAutoCompleteTextView addressDelayAutoComplete;
    private String newAddressPlaceId = null;
    Button btn_clear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = this;
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        student = (Student) getIntent().getSerializableExtra(IntentPayloadIdentifier);

        btn_clear = (Button) findViewById(R.id.clearable_button_clear);
        addressDelayAutoComplete = (DelayAutoCompleteTextView) findViewById(R.id.AddressDelayAutoComplete);
        btn_clear.setVisibility(View.INVISIBLE );
        clearText();
        showHideClearButton();

        bindStudentDetailsToView();

        saveButton = new ProgressButton(this, R.id.SaveButton, R.string.StudentDetails_SaveButton_Text, R.string.StudentDetails_SaveButton_LoadingText, new ProgressButtonCallback() {
            @Override
            public void callback() {
                save();
            }
        });

        setupGooglePlaces();
    }

    void clearText()
    {
        btn_clear.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                addressDelayAutoComplete.setText("");
            }
        });
    }

    void showHideClearButton()
    {
        addressDelayAutoComplete.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            // TODO Auto-generated method stub
                if (s.length() > 0)
                    btn_clear.setVisibility(View.VISIBLE);
                else
                    btn_clear.setVisibility(View.INVISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    private void bindStudentDetailsToView() {

        setText(R.id.FirstName, student.FirstName);
        setText(R.id.LastName, student.LastName);
        setText(R.id.MobileNumber, student.MobileNumber);
        setText(R.id.EmailAddress, student.GetEmail());

        int selectedTransmission = student.Transmission.equalsIgnoreCase("automatic")
                ? R.id.TransmissionAutomatic
                : R.id.TransmissionManual;

        ((RadioButton) activity.findViewById(selectedTransmission)).setChecked(true);
    }

    private void setupGooglePlaces() {

        String apiKey = "";

        try {
            ApplicationInfo ai = getPackageManager().getApplicationInfo(activity.getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = ai.metaData;
            apiKey = bundle.getString("GooglePlacesApiKey");
        } catch (PackageManager.NameNotFoundException e) {
            logError("Failed to load meta-data, NameNotFound: " + e.getMessage());
        } catch (NullPointerException e) {
            logError("Failed to load meta-data, NullPointer: " + e.getMessage());
        }


        addressDelayAutoComplete.setText(student.GetAddress());
        addressDelayAutoComplete.setThreshold(3);
        addressDelayAutoComplete.setAdapter(new AddressAutocompleteAdapter(this, apiKey)); // 'this' is Activity instance
        addressDelayAutoComplete.setLoadingIndicator((android.widget.ProgressBar) findViewById(R.id.AddressDelayAutoCompleteProgressBar));
        addressDelayAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                AddressAutocompleteItem item = (AddressAutocompleteItem) adapterView.getItemAtPosition(position);

                addressDelayAutoComplete.setText(item.Title);
                newAddressPlaceId = item.PlaceId;
            }
        });
    }

    private void save() {

        EditText mobileNumber = (EditText) activity.findViewById(R.id.MobileNumber);

        boolean hasErrors = false;

        if (TextUtils.isEmpty(mobileNumber.getText())) {

            mobileNumber.setError(getString(R.string.Validation_MobileNumberIsRequired));
            hasErrors = true;
        }
        else if (!ValidationHelper.IsValidMobileNumber(mobileNumber.getText().toString()))
        {
            mobileNumber.setError(getString(R.string.Validation_MobileNumberIsNotValid));
            hasErrors = true;
        }

        if (hasErrors)
        {
            resetSaveButtonText();

            return;
        }

        if (!IsNetworkAvailable())
        {
            resetSaveButtonText();

            ShowNoConnectionNotification(findViewById(R.id.StudentDetailsView), new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    save();
                }
            });

            return;
        }


        ServiceGenerator.StudentService studentService = ServiceGenerator.createService(ServiceGenerator.StudentService.class, getApplicationContext());

        StudentUpdate studentUpdate = new StudentUpdate();

        if (newAddressPlaceId != null)
        {
            studentUpdate.AddressAttributes = new AddressAttributes(newAddressPlaceId);
        }

        studentUpdate.mobileNumber = mobileNumber.getText().toString();
        studentUpdate.transmission = getTransmissionValue();

        StudentUpdateBody body = new StudentUpdateBody(studentUpdate);

        Call<StudentBody> call = studentService.Update(GetCurrentInstructorId(), student.Id, body);

        JWTRefreshService.enqueueSafe(call, this, new Callback<StudentBody>() {
            @Override
            public void onResponse(Call<StudentBody> call, Response<StudentBody> response) {

                if (response.isSuccessful()) {

                    student = response.body().student;
                    newAddressPlaceId = null;
                    resetSaveButtonText();
                }
                else
                {
                    ShowGenericError();
                    resetSaveButtonText();
                }
            }

            @Override
            public void onFailure(Call<StudentBody> call, Throwable t) {

                ShowGenericError();
                resetSaveButtonText();
            }
        });
    }

    private String getTransmissionValue()
    {
        RadioGroup transmissionRadioGroup = (RadioGroup) activity.findViewById(R.id.TransmissionRadioGroup);

        if (transmissionRadioGroup.getCheckedRadioButtonId() == R.id.TransmissionAutomatic)
        {
            return "automatic";
        }

        return "manual";
    }

    private void resetSaveButtonText()
    {
        saveButton.hideLoading();
    }

    @Override
    public void onBackPressed() {

        boolean showConfirmation = false;

        if (newAddressPlaceId != null)
        {
            showConfirmation = true;
        }

        EditText mobileNumber = (EditText) activity.findViewById(R.id.MobileNumber);
        if (!mobileNumber.getText().toString().equals(student.MobileNumber))
        {
            showConfirmation = true;
        }

        if (!getTransmissionValue().equals(student.Transmission))
        {
            showConfirmation = true;
        }

        if (showConfirmation)
        {
            new AlertDialog.Builder(this, R.style.AlertDialogCustom).setTitle(GetTextFromResource(R.string.ChangesMadeConfirmation_Title))
                    .setMessage(GetTextFromResource(R.string.ChangesMadeConfirmation_Text))
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int whichButton) {
                            goBack();
                        }
                    }).setNegativeButton("no", null).show();
        }
        else {

            goBack();
        }
    }

    private void goBack()
    {
        super.onBackPressed();
    }
}

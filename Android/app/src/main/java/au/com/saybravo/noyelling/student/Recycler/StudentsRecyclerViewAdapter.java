package au.com.saybravo.noyelling.student.Recycler;


import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import java.util.List;

import au.com.saybravo.noyelling.student.Domain.Student;
import au.com.saybravo.noyelling.student.Recycler.BaseStudentRecyclerViewAdapter;


/**
 * Created by ahmed.sadiq on 13/04/16.
 */
public class StudentsRecyclerViewAdapter extends BaseStudentRecyclerViewAdapter
{
    private boolean loading;
    private int totalItemCount = 0;
    private int lastVisibleItem = 0;
    private int visibleThreshold = 5;

    private OnLoadMoreListener onLoadMoreListener;

    // OnLoadMoreListener section
    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener){
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public void setLoaded(){
        loading = false;
    }

    public StudentsRecyclerViewAdapter(List<Student> students, RecyclerView recyclerView){

        this.students = students;

        recyclerView.addOnScrollListener( new RecyclerView.OnScrollListener(){

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy){
                super.onScrolled(recyclerView, dx, dy);
                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    loading = true;
                    // End has been reached
                    // Do something
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                }
            }
        });
    }

}

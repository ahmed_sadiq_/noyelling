package au.com.saybravo.noyelling.lessonPackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.List;

import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.lessonPackage.Domain.LessonPackageItem;
import au.com.saybravo.noyelling.payment.Domain.CashPackage;

/**
 * Created by ahmed.sadiq on 06/12/2016.
 */

public class LessonCashPackageAdapter extends ArrayAdapter<CashPackage> {

    int selectedIndex = -1;

    public LessonCashPackageAdapter(Context context, int activity_radio_button, List<CashPackage> saveItems) {
        super(context, activity_radio_button, saveItems);
    }

    public void setSelectedIndex(int index) {
        selectedIndex = index;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.custom_radio_item, null);
        }

        RadioButton rbSelect = (RadioButton) v
                .findViewById(R.id.radioButton);
        if(selectedIndex == position){  // check the position to update correct radio button.
            rbSelect.setChecked(true);
        }
        else{
            rbSelect.setChecked(false);
        }

        CashPackage itemContents = getItem(position);

        if (itemContents != null) {
            TextView itemName = (TextView) v.findViewById(R.id.ItemName);
            TextView itemValue = (TextView) v.findViewById(R.id.ItemValue);
            itemName.setText(itemContents.getName());
            itemValue.setText("$"+itemContents.getSalePrice());
        }

        return v;
    }
}

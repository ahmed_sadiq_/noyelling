package au.com.saybravo.noyelling.app;

/**
 * Created by ahmed.sadiq on 25/04/2016.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.app.ProgressButton.ProgressButton;
import au.com.saybravo.noyelling.app.ProgressButton.ProgressButtonCallback;
import au.com.saybravo.noyelling.dashboard.DashboardActivity;

public class CrashActivity extends Activity {

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        setContentView(R.layout.activity_crashreport);

        ProgressButton backToDashboardButton = new ProgressButton(this, R.id.BackToDashboardButton, R.string.CrashPage_BackToDashboardButton_Text, R.string.CrashPage_BackToDashboardButton_LoadingText, new ProgressButtonCallback() {
            @Override
            public void callback() {
                Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                startActivity(intent);
            }
        });
    }
}

package au.com.saybravo.noyelling.lessons.Domain;

import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class StudentHistory
{
    @SerializedName("id")
    @Expose
    public Integer Id;

    @SerializedName("class_name")
    @Expose
    public String ClassName;

    @SerializedName("category")
    @Expose
    public String Category;

    @SerializedName("payment_type")
    @Expose
    public String paymentType;

    @SerializedName("created_at")
    @Expose
    public Date CreatedAt;

    @SerializedName("notes")
    @Expose
    public String Notes;

    @SerializedName("package")
    @Expose
    public Package Package;

    public class Package
    {
        @SerializedName("minutes")
        @Expose
        public Integer Minutes;

        @SerializedName("sale_price")
        @Expose
        public Double SalePrice;

        @SerializedName("items")
        @Expose
        public Item[] Items;

        public class Item
        {
            @SerializedName("package_detail_id")
            @Expose
            public Integer PackageDetailId;

            @SerializedName("product_name")
            @Expose
            public String ProductName;

            @SerializedName("sale_price")
            @Expose
            public Double SalePrice;

            @SerializedName("claim")
            @Expose
            public Claim Claim;

            public class Claim
            {
                @SerializedName("id")
                @Expose
                public Integer Id;

                @SerializedName("package_detail_id")
                @Expose
                public Integer PackageDetailId;

                @SerializedName("instructor_id")
                @Expose
                public Integer InstructorId;

                @SerializedName("status")
                @Expose
                public String Status;

                @SerializedName("created_at")
                @Expose
                public Date CreatedAt;

                @SerializedName("reconciled_at")
                @Expose
                @Nullable
                public Date ReconciledAt;

                @SerializedName("type")
                @Expose
                @Nullable
                public String type;
            }
        }
    }
}







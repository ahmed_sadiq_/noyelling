package au.com.saybravo.noyelling.student.Domain.CreateStudent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import au.com.saybravo.noyelling.student.Domain.AddressAttributes;

/**
 * Created by ahmed.sadiq on 31/05/16.
 *
 *
 */

public class Student {

    @SerializedName("fname")
    @Expose
    private String fname;
    @SerializedName("lname")
    @Expose
    private String lname;
    @SerializedName("mobile_number")
    @Expose
    private String mobileNumber;
    @SerializedName("transmission")
    @Expose
    private String transmission;
    @SerializedName("customer_attributes")
    @Expose
    private CustomerAttributes customerAttributes;
    @SerializedName("address_attributes")
    @Expose
    private AddressAttributes addressAttributes;

    /**
     * @return The fname
     */
    public String getFname() {
        return fname;
    }

    /**
     * @param fname The fname
     */
    public void setFname(String fname) {
        this.fname = fname;
    }

    /**
     * @return The lname
     */
    public String getLname() {
        return lname;
    }

    /**
     * @param lname The lname
     */
    public void setLname(String lname) {
        this.lname = lname;
    }

    /**
     * @return The mobileNumber
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * @param mobileNumber The mobile_number
     */
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    /**
     * @return The transmission
     */
    public String getTransmission() {
        return transmission;
    }

    /**
     * @param transmission The transmission
     */
    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    /**
     * @return The customerAttributes
     */
    public CustomerAttributes getCustomerAttributes() {
        return customerAttributes;
    }

    /**
     * @param customerAttributes The customer_attributes
     */
    public void setCustomerAttributes(CustomerAttributes customerAttributes) {
        this.customerAttributes = customerAttributes;
    }

    /**
     * @return The addressAttributes
     */
    public AddressAttributes getAddressAttributes() {
        return addressAttributes;
    }

    /**
     * @param addressAttributes The address_attributes
     */
    public void setAddressAttributes(AddressAttributes addressAttributes) {
        this.addressAttributes = addressAttributes;
    }

}
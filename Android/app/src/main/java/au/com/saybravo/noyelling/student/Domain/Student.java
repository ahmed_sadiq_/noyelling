package au.com.saybravo.noyelling.student.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import au.com.saybravo.noyelling.student.Domain.Customer;

public class Student implements Serializable {

    @SerializedName("id")
    @Expose
    public Integer Id;

    @SerializedName("fname")
    @Expose
    public String FirstName;

    @SerializedName("lname")
    @Expose
    public String LastName;

    @SerializedName("mobile_number")
    @Expose
    public String MobileNumber;

    @SerializedName("transmission")
    @Expose
    public String Transmission;

    @SerializedName("lessons_remaining")
    @Expose
    public Integer LessonsRemaining;

    @SerializedName("customer")
    @Expose
    public au.com.saybravo.noyelling.student.Domain.Customer Customer;

    @SerializedName("address")
    @Expose
    public au.com.saybravo.noyelling.student.Domain.Address Address;

    public Student(Integer id, String firstName, String lastName, String phone, Customer customer)
    {
        FirstName = firstName;
        LastName = lastName;
        Id = id;
        MobileNumber = phone;
        Customer = customer;
    }

    public String GetEmail()
    {
        return Customer.Email;
    }

    public String GetAddress() {

        return Address.FullAddress;
    }
}

package au.com.saybravo.noyelling.claim;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.app.BaseActivity;
import au.com.saybravo.noyelling.app.Helpers.StringHelper;
import au.com.saybravo.noyelling.app.NoYellingApplication;
import au.com.saybravo.noyelling.app.Services.JWTRefreshService;
import au.com.saybravo.noyelling.app.Services.ServiceGenerator;
import au.com.saybravo.noyelling.app.util.AppConstants;
import au.com.saybravo.noyelling.claim.Domain.Claim;
import au.com.saybravo.noyelling.claim.Domain.ClaimBody;
import au.com.saybravo.noyelling.student.Domain.Student;
import au.com.saybravo.noyelling.student.StudentActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ahmed.sadiq on 22/04/16.
 */
public class ClaimActivity extends BaseActivity implements View.OnClickListener {

    private LinearLayout viewStudentLinearLayout;
    private LinearLayout abandonClaimLinearLayout;
    private LinearLayout queryLinearLayout;
    private RelativeLayout statusBannerLayout;
    private RelativeLayout selectionLayout;
    private TextView claimStatus;
    private RelativeLayout statusLogo;
    private ProgressBar progressBar;

    private NoYellingApplication noYellingApplication;
    private ClaimActivity MainActivity;

    private Student student;
    private Integer claimId;
    private String claimType;
    private boolean loading;
    private boolean removeViewStudent;
    private boolean deletingClaim;


    public ClaimActivity() {
        super(R.layout.activity_claim);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MainActivity = this;
        noYellingApplication = (NoYellingApplication)getApplication();
        bindClaimToView();

        Bundle extras = getIntent().getExtras();

        if(extras != null){

            claimId = extras.getInt("claimId");
            student = (Student)extras.getSerializable("student");
            removeViewStudent = extras.getBoolean("hideViewStudent");
            claimType = extras.getString("claim_type");

            if(removeViewStudent){
                removeViewStudentLinearLayout();
            }
            if(claimType != null &&
                    claimType.equalsIgnoreCase(AppConstants.CLAIM_TYPE.DEBIT.getValue())) {
                removeAbandonClaimLinearLayout();
            }

            if(student != null) {
                setText(R.id.StudentFirstName ,student.FirstName);
                setText( R.id.StudentLastName ,student.LastName);
                setText(R.id.Transmission, student.Transmission.equalsIgnoreCase("automatic")? "Automatic" : "Manual");
                loading = true;
                showLoading();
                getClaim();
            }

        }
    }

    @Override
    public void onRestart(){
        super.onRestart();
        boolean alreadyAbandoned = noYellingApplication.AbandonedClaimsList.contains(claimId);

        if(alreadyAbandoned){

            Intent data = new Intent();
            data.putExtra("calimId", claimId);
            setResult(BaseActivity.RESULT_CLAIM_DELETED, data);

            finish();
        }
    }

    @Override
    public void onClick(View view) {
        if(loading == true){
            return;
        }

        if (!IsNetworkAvailable())
        {
            final View calledView = view;
            ShowNoConnectionNotification(findViewById(R.id.ClaimView), new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                   MainActivity.onClick(calledView);
                }
            });

            return;
        }


        if(view.getId() == R.id.AbandonClaimLinearLayout) {

            new AlertDialog.Builder(this).setTitle(GetTextFromResource(R.string.AbandonClaimConfirmation_Title))
                    .setMessage(GetTextFromResource(R.string.AbandonClaimConfirmation_Text))
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int whichButton) {
                            loading = true;
                            deletingClaim = true;
                            showLoading();
                            deleteClaim();

                        }
                    }).setNegativeButton("no", null).show();
        }

        if(view.getId() == R.id.ViewStudentLinearLayout){

            Intent intent = new Intent(getApplicationContext(), StudentActivity.class);
            intent.putExtra(BaseActivity.IntentPayloadIdentifier, student.Id);
            intent.putExtra("student", student);

            startActivity(intent);
        }

        if(view.getId() == R.id.QueryLinearLayout){

            StartEmailIntent(R.string.Claim_SendQuery_Subject, student.FirstName + " " + student.LastName, R.string.Claim_SendQuery_EmailTo);
        }
    }
    @Override
    public void onBackPressed(){
        if(deletingClaim == false) {
            super.onBackPressed();
        }
    }


    private void bindClaimToView(){

        viewStudentLinearLayout = (LinearLayout) findViewById(R.id.ViewStudentLinearLayout);
        abandonClaimLinearLayout = (LinearLayout) findViewById(R.id.AbandonClaimLinearLayout);
        queryLinearLayout = (LinearLayout) findViewById(R.id.QueryLinearLayout);
        selectionLayout = (RelativeLayout) findViewById(R.id.SelectionLayout);

        viewStudentLinearLayout.setOnClickListener(this);
        abandonClaimLinearLayout.setOnClickListener(this);
        queryLinearLayout.setOnClickListener(this);

        statusBannerLayout = (RelativeLayout) findViewById(R.id.StatusBannerLayout);
        progressBar = (ProgressBar) findViewById(R.id.ClaimLoadingProgressBar);
        claimStatus = (TextView) findViewById(R.id.ClaimStatus);
        statusLogo = (RelativeLayout) findViewById(R.id.StatusLogo);

        setText(R.id.ProductName, "");
    }

    private void deleteClaim(){

        if (!IsNetworkAvailable())
        {
            ShowNoConnectionNotification(findViewById(R.id.ClaimView), new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    deleteClaim();
                }
            });

            return;
        }

        ServiceGenerator.ClaimService claimService = ServiceGenerator.createService(ServiceGenerator.ClaimService.class, getApplicationContext());

        Call<ClaimBody> call = claimService.DeleteClaim(GetCurrentInstructorId(), claimId);

        JWTRefreshService.enqueueSafe(call, this, new Callback<ClaimBody>() {
            @Override
            public void onResponse(Call<ClaimBody> call, Response<ClaimBody> response) {

                deletingClaim = false;
                if(response.isSuccessful()){

                    Toast.makeText(MainActivity, "claim abandoned", Toast.LENGTH_SHORT).show();

                    Intent data = new Intent();
                    data.putExtra("calimId", claimId);
                    setResult(BaseActivity.RESULT_CLAIM_DELETED, data);
                    noYellingApplication.AbandonedClaimsList.add(claimId);
                    onBackPressed();
                }
            }

            @Override
            public void onFailure(Call<ClaimBody> call, Throwable t) {
                deletingClaim = false;
            }
        });
    }


    private void getClaim(){

        ServiceGenerator.ClaimService claimService = ServiceGenerator.createService(ServiceGenerator.ClaimService.class, getApplicationContext());

        Call<ClaimBody> call = claimService.GetClaim(GetCurrentInstructorId(), claimId );

        JWTRefreshService.enqueueSafe(call, this, new Callback<ClaimBody>() {
            @Override
            public void onResponse(Call<ClaimBody> call, Response<ClaimBody> response) {

                if(response.isSuccessful()){
                    Claim claim = response.body().getClaim();

                    if(claim != null){
                        setTransactionStatus(claim.getStatus(), claim.getReconciledAt());
                        if(claim.getPackageDetail() != null && claim.getPackageDetail().getSalePrice() != null) {

                            Double salePrice = claim.getPackageDetail().getSalePrice();
                            String salePriceString = StringHelper.FormatOptionalDecimal(salePrice);

                            setText(R.id.Amount, "$" + salePriceString);
                        }
                        Double minutesInCar = 0.0;

                        if(claim.getPackageDetail().getProduct().getMinutesInCar() != null) {
                            minutesInCar = (claim.getPackageDetail().getProduct().getMinutesInCar()) / 60.0;
                        }
                        setText(R.id.ProductName, claim.getPackageDetail().getProduct().getName());
                        setText(R.id.Commission, "Commission paid: $"+StringHelper.FormatCurrency(claim.getCommissionPaid()));
                        setText(R.id.Transmission, claim.getStudent().Transmission.equalsIgnoreCase("automatic")? "Automatic" : "Manual" );

                    }
                }

                loading = false;
                deletingClaim = false;
                showLoading();
            }

            @Override
            public void onFailure(Call<ClaimBody> call, Throwable t) {
                loading =false;
                deletingClaim = false;
                showLoading();
            }
        });

    }

    private void removeAbandonClaimLinearLayout(){

        if(abandonClaimLinearLayout != null) {
            ViewManager parent = (ViewManager) abandonClaimLinearLayout.getParent();
            if(parent != null) {
                parent.removeView(abandonClaimLinearLayout);
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) queryLinearLayout.getLayoutParams();
                if (removeViewStudent) {
                    params.addRule(RelativeLayout.BELOW, R.id.StatusBannerLayout);
                } else {
                    params.addRule(RelativeLayout.BELOW, R.id.ViewStudentLinearLayout);
                }
                queryLinearLayout.setLayoutParams(params);
            }
        }
    }

    private void removeViewStudentLinearLayout(){
        ((ViewManager)viewStudentLinearLayout.getParent()).removeView(viewStudentLinearLayout);
    }

    private void showLoading(){
        if(loading == true){
            progressBar.setVisibility(View.VISIBLE);
            selectionLayout.setVisibility(View.GONE);
            statusBannerLayout.setBackgroundColor(ContextCompat.getColor(this,R.color.White));
        }else{
            selectionLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.INVISIBLE);
        }
    }


    private void setTransactionStatus(String status, Object reconciledAt ){

        switch(status){
            case "paid":
                statusBannerLayout.setBackgroundColor(ContextCompat.getColor(this,R.color.PaidClaimStatus));
                String cStatus = "Paid";
                if(reconciledAt != null){
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date rdate = null;
                    try {
                        rdate = sdf.parse((String)reconciledAt);
                        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
                        cStatus+= " "+df.format(rdate);

                    } catch (ParseException e) {

                    }
                }
                claimStatus.setText(cStatus);
                removeAbandonClaimLinearLayout();
                break;

            case "pending":

                statusBannerLayout.setBackgroundColor(ContextCompat.getColor(this,R.color.PendingClaim));
                claimStatus.setText("Pending Claim");
                statusLogo.setBackgroundResource( R.drawable.icon_pendingbig);
                break;

            case "unreconciled":

                statusBannerLayout.setBackgroundColor(ContextCompat.getColor(this,R.color.UnreconciledClaim));
                claimStatus.setText("Unreconciled Claim");
                statusLogo.setBackgroundResource( R.drawable.icon_unreconciledbig );
                break;

        }

        //Overriding behavior - in case of claimType == debit, setting text 'Cash Received'
        if(claimType != null &&
                claimType.equalsIgnoreCase(AppConstants.CLAIM_TYPE.DEBIT.getValue())) {
            claimStatus.setText("Cash Received");
        }
    }

}

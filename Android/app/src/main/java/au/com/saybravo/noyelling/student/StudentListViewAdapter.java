package au.com.saybravo.noyelling.student;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import au.com.saybravo.noyelling.student.Domain.Student;

public class StudentListViewAdapter extends ArrayAdapter<Student> {

    private int layoutResourceId;

    public StudentListViewAdapter(Context context, int resource, Student[] students) {
        super(context, resource, students);

        layoutResourceId = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            Student student = getItem(position);
            View view;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                view = inflater.inflate(layoutResourceId, null);

            } else {
                view = convertView;
            }

            return view;

        } catch (Exception ex) {
            Log.e("StudentListViewAdapter", "error", ex);
            return null;
        }
    }
}

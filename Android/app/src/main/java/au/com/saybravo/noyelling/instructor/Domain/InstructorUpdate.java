package au.com.saybravo.noyelling.instructor.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InstructorUpdate
{
    @SerializedName("gender")
    @Expose
    public String gender;

    @SerializedName("fname")
    @Expose
    public String fname;

    @SerializedName("lname")
    @Expose
    public String lname;

    @SerializedName("mobile_number")
    @Expose
    public String mobileNumber;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("about")
    @Expose
    public String about;
}


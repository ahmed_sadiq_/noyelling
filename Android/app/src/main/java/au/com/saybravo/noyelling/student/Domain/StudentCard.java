package au.com.saybravo.noyelling.student.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StudentCard
{
    @SerializedName("token")
    @Expose
    public String token;

    public StudentCard(String token) {
        this.token = token;
    }
}


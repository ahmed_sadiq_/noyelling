package au.com.saybravo.noyelling.student.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.student.Domain.Address;
import au.com.saybravo.noyelling.student.Domain.Customer;
import au.com.saybravo.noyelling.student.Domain.Student;
import au.com.saybravo.noyelling.student.Domain.StudentRealmModel;
import au.com.saybravo.noyelling.student.QueryExecutor;
import au.com.saybravo.noyelling.student.Recycler.BaseStudentRecyclerViewAdapter;
import au.com.saybravo.noyelling.student.Recycler.RecentStudentsRecyclerViewAdapter;
import au.com.saybravo.noyelling.student.StudentActivity;
import au.com.saybravo.noyelling.student.StudentsActivity;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by ahmed.sadiq on 15/04/16.
 */
public class RecentStudentsFragment extends Fragment implements QueryExecutor {

    private RecyclerView recyclerView;
    private RelativeLayout noStudents;
    private RecentStudentsRecyclerViewAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Student> students = new ArrayList<>();
    private String queryString = null;
    private Realm realm = null;
    private RealmConfiguration realmConfiguration;
    private boolean firstCall;


    private void putStudentToRecentViewed(Student student){

        realm.beginTransaction();

        StudentRealmModel foundStudent =  realm.where(StudentRealmModel.class).equalTo("id", student.Id).findFirst();

        if(foundStudent != null){
            // update the lastAccessed
            foundStudent.lastAccessed = new Date();
        }

        realm.commitTransaction();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.layout_students_fragment, container, false);

        recyclerView =  (RecyclerView) v.findViewById(R.id.recyclerview);
        noStudents = (RelativeLayout) v.findViewById(R.id.NoStudents);
        noStudents.setVisibility(View.GONE);

        recyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getActivity());
        firstCall = true;

        recyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new RecentStudentsRecyclerViewAdapter(students);

        mAdapter.setOnItemClickListener(new BaseStudentRecyclerViewAdapter.OnItemClickListener(){
            @Override
            public void onItemClick(View view, final int position) {

                final BaseStudentRecyclerViewAdapter.OnItemClickListener listener = this;
                final View clickedView = view;

                StudentsActivity studentsActivity = (StudentsActivity)getActivity();
                if(!studentsActivity.isNetworkConnected()){
                    studentsActivity.showNoConnectionNotification(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listener.onItemClick(clickedView, position);
                        }
                    });

                    return;
                }
                Intent intent = new Intent(getContext(), StudentActivity.class);
                intent.putExtra("Payload", students.get(position).Id);
                intent.putExtra("student", students.get(position));
                startActivity(intent);

                putStudentToRecentViewed(students.get(position));
            }
        });

        recyclerView.setAdapter(mAdapter);

        realmConfiguration = new RealmConfiguration.Builder(getContext()).name("recentStudents.realm").build();
        realm = Realm.getInstance(realmConfiguration);

        return v;
    }

    @Override
    public void setQueryString(String query) {
        queryString = query;
        reloadRecentStudentData();
    }

    @Override
    public void onStart(){
        super.onStart();

        reloadRecentStudentData();
    }



    private void reloadRecentStudentData(){

        students.clear();
        noStudents.setVisibility(View.GONE);

        RealmResults<StudentRealmModel> storedStudents = null;
        storedStudents = realm.where(StudentRealmModel.class).findAllSorted("lastAccessed", Sort.DESCENDING);

        if(queryString == null) {

        }else{
            ((StudentsActivity)getActivity()).skipRecentStudents();

            // Code Change per UAT : Bravo 13/07/2016
            // Previously we would search through the Local list of students instead of hit the server.
            // Now we have changed it so we move to the A-Z tab and hit the server for each search.

            /*String[] querySegments = queryString.split(" ");

            if(querySegments.length == 1) {

                storedStudents = realm.where(StudentRealmModel.class)
                        .beginsWith("firstName", queryString, Case.INSENSITIVE)
                        .or()
                        .beginsWith("lastName", queryString, Case.INSENSITIVE)
                        .or()
                        .beginsWith("email", queryString, Case.INSENSITIVE)
                        .or()
                        .beginsWith("suburb", queryString, Case.INSENSITIVE).findAllSorted("lastAccessed", Sort.DESCENDING);


            }else if(querySegments.length > 1){

                storedStudents = realm.where(StudentRealmModel.class)
                        .equalTo("firstName", querySegments[0], Case.INSENSITIVE)
                        .equalTo("lastName", querySegments[1], Case.INSENSITIVE)
                        .or()
                        .beginsWith("email", queryString, Case.INSENSITIVE)
                        .or()
                        .beginsWith("suburb", queryString, Case.INSENSITIVE).findAllSorted("lastAccessed", Sort.DESCENDING);
            }*/
        }
        for(StudentRealmModel studentModel: storedStudents){
            Student student = new Student(studentModel.id, studentModel.firstName, studentModel.lastName, null, new Customer(studentModel.email));
            student.Address = new Address();
            student.Address.Suburb = studentModel.suburb;
            students.add(student);
        }

        if (students.size() == 0)
        {
            noStudents.setVisibility(View.VISIBLE);

        }

        mAdapter.notifyDataSetChanged();
    }
}

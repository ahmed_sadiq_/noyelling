package au.com.saybravo.noyelling.payment.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmed.sadiq on 1/06/16.
 */
public class ReconcileBody {

    @SerializedName("transaction")
    @Expose
    private Transaction transaction;

    /**
     *
     * @return
     * The transaction
     */
    public Transaction getTransaction() {
        return transaction;
    }

    /**
     *
     * @param transaction
     * The transaction
     */
    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

}

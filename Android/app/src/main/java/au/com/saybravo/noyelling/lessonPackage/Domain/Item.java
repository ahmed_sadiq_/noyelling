package au.com.saybravo.noyelling.lessonPackage.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmed.sadiq on 30/05/16.
 */
public class Item {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("minutes_in_car")
    @Expose
    private Double minutesInCar;
    @SerializedName("base_rate")
    @Expose
    private Double baseRate;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The minutesInCar
     */
    public Double getMinutesInCar() {
        return minutesInCar;
    }

    /**
     *
     * @param minutesInCar
     * The minutes_in_car
     */
    public void setMinutesInCar(Double minutesInCar) {
        this.minutesInCar = minutesInCar;
    }

    /**
     *
     * @return
     * The baseRate
     */
    public Double getBaseRate() {
        return baseRate;
    }

    /**
     *
     * @param baseRate
     * The base_rate
     */
    public void setBaseRate(Double baseRate) {
        this.baseRate = baseRate;
    }

}
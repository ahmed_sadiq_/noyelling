package au.com.saybravo.noyelling.lessons.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Claim
{
    @SerializedName("package_detail_id")
    @Expose
    public Integer PackageDetailId;

    public Claim(Integer packageDetailId)
    {
        PackageDetailId = packageDetailId;
    }
}

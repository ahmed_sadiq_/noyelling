package au.com.saybravo.noyelling.app.Helpers;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.InputStream;


/**
 * Created by ahmed.sadiq on 11/04/16.
 */
public class DowloadImageTask extends AsyncTask<String, Void, Bitmap>{

    ImageView bmImage;
    Boolean cropImage = false;


    public DowloadImageTask(ImageView bmImage, boolean cropImage) {

        this.bmImage = bmImage;
        this.cropImage = cropImage;
    }

    @Override
    protected Bitmap doInBackground(String... urls) {

        String urldisplay  = urls[0];
        Bitmap image = null;
        try{
            InputStream in = new java.net.URL(urldisplay).openStream();
            image = BitmapFactory.decodeStream(in);

        }catch(Exception e){

            e.printStackTrace();
            return null;
        }

        return image;
    }


    protected void onPostExecute(Bitmap result){
        if(cropImage) {
            Bitmap crop = BitmapHelper.getCroppedBitmap(result, true, 120, 120);
            bmImage.setImageBitmap(crop);
        }else{
            bmImage.setImageBitmap(result);
        }
    }


}

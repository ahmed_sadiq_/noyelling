package au.com.saybravo.noyelling.lessons.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import au.com.saybravo.noyelling.student.Domain.Student;

/**
 * Created by ahmed.sadiq on 21/04/2016.
 */
public class StudentHistoryBody {
    @SerializedName("history")
    @Expose
    public StudentHistory[] history;
}


package au.com.saybravo.noyelling.payment.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahmed.sadiq on 31/05/16.
 */
public class Package implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("sale_price")
    @Expose
    private Double salePrice;
    @SerializedName("minutes")
    @Expose
    private Integer minutes;
    @SerializedName("package_details")
    @Expose
    private List<PackageDetail> packageDetails = new ArrayList<PackageDetail>();

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The salePrice
     */
    public Double getSalePrice() {
        return salePrice;
    }

    /**
     *
     * @param salePrice
     * The sale_price
     */
    public void setSalePrice(Double salePrice) {
        this.salePrice = salePrice;
    }

    /**
     *
     * @return
     * The minutes
     */
    public Integer getMinutes() {
        return minutes;
    }

    /**
     *
     * @param minutes
     * The minutes
     */
    public void setMinutes(Integer minutes) {
        this.minutes = minutes;
    }

    /**
     *
     * @return
     * The packageDetails
     */
    public List<PackageDetail> getPackageDetails() {
        return packageDetails;
    }

    /**
     *
     * @param packageDetails
     * The package_details
     */
    public void setPackageDetails(List<PackageDetail> packageDetails) {
        this.packageDetails = packageDetails;
    }

}
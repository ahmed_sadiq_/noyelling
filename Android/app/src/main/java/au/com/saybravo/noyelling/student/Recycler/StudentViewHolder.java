package au.com.saybravo.noyelling.student.Recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.student.Domain.Student;
import au.com.saybravo.noyelling.student.Recycler.BaseStudentRecyclerViewAdapter;


/**
 * Created by ahmed.sadiq on 15/04/16.
 */
public class StudentViewHolder extends  RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView name;
    public TextView emailSuburb;

    public Student student;
    //public FragmentActivity fragmentActivity;
    BaseStudentRecyclerViewAdapter.OnItemClickListener mItemClickListener;


    public StudentViewHolder(View itemView, BaseStudentRecyclerViewAdapter.OnItemClickListener onItemClickListener) {
        super(itemView);

        this.mItemClickListener = onItemClickListener;
        itemView.setOnClickListener(this);

        name = (TextView) itemView.findViewById(R.id.StudentName);
        emailSuburb = (TextView) itemView.findViewById(R.id.EmailSuburb);

    }

    @Override
    public void onClick(View view) {
        if(mItemClickListener != null){
            mItemClickListener.onItemClick(view, getLayoutPosition());
        }
    }
}

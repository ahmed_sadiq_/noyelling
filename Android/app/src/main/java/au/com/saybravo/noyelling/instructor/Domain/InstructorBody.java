package au.com.saybravo.noyelling.instructor.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InstructorBody {

    @SerializedName("instructor")
    @Expose
    private Instructor instructor;

    /**
     *
     * @return
     * The instructor
     */
    public Instructor getInstructor() {
        return instructor;
    }

    /**
     *
     * @param instructor
     * The instructor
     */
    public void setInstructor(Instructor instructor) {
        this.instructor = instructor;
    }
}


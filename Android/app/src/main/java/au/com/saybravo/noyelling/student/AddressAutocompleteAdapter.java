package au.com.saybravo.noyelling.student;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.student.Domain.AddressAutocompleteItem;

/**
 * Created by ahmed.sadiq on 30/05/2016.
 * Based on: http://makovkastar.github.io/blog/2014/04/12/android-autocompletetextview-with-suggestions-from-a-web-service/
 * And https://examples.javacodegeeks.com/android/android-google-places-autocomplete-api-example/
 */
public class AddressAutocompleteAdapter extends BaseAdapter implements Filterable {

    private static final int MAX_RESULTS = 10;
    private Context mContext;
    private String apiKey;
    private List<AddressAutocompleteItem> resultList = new ArrayList<>();

    public AddressAutocompleteAdapter(Context context, String apiKey) {
        mContext = context;
        this.apiKey = apiKey;
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public AddressAutocompleteItem getItem(int index) {
        return resultList.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_address_autocomplete, parent, false);
        }

        ((TextView) convertView.findViewById(R.id.Address)).setText(getItem(position).Title);

        return convertView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                Filter.FilterResults filterResults = new Filter.FilterResults();

                if (constraint != null) {
                    List<AddressAutocompleteItem> items = findItems(mContext, constraint.toString());

                    // Assign the data to the FilterResults
                    filterResults.values = items;
                    filterResults.count = items.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    resultList = (List<AddressAutocompleteItem>) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
        return filter;
    }

    private List<AddressAutocompleteItem> findItems(Context context, String input) {

        String logTag = "Google Places Autocomplete";
        String placesApiBase = "https://maps.googleapis.com/maps/api/place";
        String typeAutocomplete = "/autocomplete";
        String outJson = "/json";

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(placesApiBase + typeAutocomplete + outJson);
            sb.append("?key=" + apiKey);
            sb.append("&components=country:au");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {

            Log.e(logTag, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(logTag, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            List<AddressAutocompleteItem> results = new ArrayList<>();
            for (int i = 0; i < predsJsonArray.length(); i++) {

                JSONObject jsonObject = predsJsonArray.getJSONObject(i);
                AddressAutocompleteItem item = new AddressAutocompleteItem(jsonObject.getString("place_id"), jsonObject.getString("description"));
                results.add(item);
            }

            return results;

        } catch (JSONException e) {
            Log.e(logTag, "Cannot process JSON results", e);
        }

        return resultList;
    }
}

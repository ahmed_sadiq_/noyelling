package au.com.saybravo.noyelling.app.Helpers;

import com.paypal.merchant.sdk.PayPalHereSDK;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Created by ahmed.sadiq on 4/05/2016.
 */
public class StringHelper {

    public static String FormatCurrency(double value)
    {
        return new DecimalFormat("#.00").format(value);
    }

    public static String FormatCurrency(BigDecimal value)
    {
        return new DecimalFormat("#.00").format(value);
    }

    public static String FormatOptionalDecimal(double value)
    {
        return new DecimalFormat("#.##").format(value);
    }


    public static String FormatCurrencyOptinalTwoDecimal(double value)
    {
        String val = new DecimalFormat("#.00").format(value);

        if(val.endsWith(".00")){
            return new DecimalFormat("#.##").format(value);
        }

        return val;
    }
}

package au.com.saybravo.noyelling.login.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmed.sadiq on 27/04/2016.
 */
public class ForgotPasswordResponse {

    @SerializedName("message")
    @Expose
    public String Message;
}

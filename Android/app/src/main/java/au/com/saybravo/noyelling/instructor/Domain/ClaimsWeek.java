package au.com.saybravo.noyelling.instructor.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmed.sadiq on 19/04/2016.
 */
public class ClaimsWeek {
    @SerializedName("commencing")
    @Expose
    private String commencing;
    @SerializedName("concluding")
    @Expose
    private String concluding;

    /**
     *
     * @return
     * The commencing
     */
    public String getCommencing() {
        return commencing;
    }

    /**
     *
     * @param commencing
     * The commencing
     */
    public void setCommencing(String commencing) {
        this.commencing = commencing;
    }

    /**
     *
     * @return
     * The concluding
     */
    public String getConcluding() {
        return concluding;
    }

    /**
     *
     * @param concluding
     * The concluding
     */
    public void setConcluding(String concluding) {
        this.concluding = concluding;
    }
}

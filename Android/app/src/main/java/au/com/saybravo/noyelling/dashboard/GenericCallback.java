package au.com.saybravo.noyelling.dashboard;

/**
 * Created by ahmed.sadiq on 4/05/2016.
 */
public interface GenericCallback<T> {
    void success(T value);
    void failure(Exception e);
}

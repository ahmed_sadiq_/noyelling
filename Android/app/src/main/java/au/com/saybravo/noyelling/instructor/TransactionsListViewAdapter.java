package au.com.saybravo.noyelling.instructor;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.app.Helpers.DateHelper;
import au.com.saybravo.noyelling.app.Helpers.StringHelper;
import au.com.saybravo.noyelling.instructor.Domain.ClaimListViewItem;
import au.com.saybravo.noyelling.instructor.Domain.Claims;
import au.com.saybravo.noyelling.instructor.Domain.ClaimsPaid;
import au.com.saybravo.noyelling.instructor.Domain.ClaimsPending;
import au.com.saybravo.noyelling.instructor.Domain.ClaimsUnreconciled;

public class TransactionsListViewAdapter extends ArrayAdapter<ClaimListViewItem> {

    private Context callingActivity;
    private int layoutResourceId;
    public Double totalHours = 0.0;

    private ClaimListViewItem unreconciledItemsHeader;
    private ClaimListViewItem pendingItemsHeader;
    private ClaimListViewItem paidItemsHeader;

    private Double unreconciledValue;
    private Double pendingValue;
    private Double paidValue;

    public TransactionsListViewAdapter(Context context, int resource) {
        super(context, resource);

        callingActivity = context;
        layoutResourceId = resource;
    }

    public void populateItems(Claims claims) {


        ArrayList<ClaimListViewItem> items = new ArrayList<ClaimListViewItem>();
        Double hoursCount = 0.0;
        BigDecimal priceCount = BigDecimal.ZERO;

        // cleare existing data from the listview
        this.clear();
        totalHours = 0.0;

        // Unreconciled Items
        List<ClaimsUnreconciled> claimsUnreconciled = claims.getUnreconciled();

        if(claimsUnreconciled.size() > 0) {
            unreconciledItemsHeader = new ClaimListViewItem(ClaimListViewItem.ClaimsListViewItemType.UnreconciledHeader, null, null, null, null, null, null, null);
            items.add(unreconciledItemsHeader);
        }

        for (int i = 0; i < claimsUnreconciled.size(); i++)
        {
            ClaimsUnreconciled unreconciledClaims = claimsUnreconciled.get(i);

            Double timeInCarHrs = unreconciledClaims.getTimeInCar() != null ? unreconciledClaims.getTimeInCar()/60.0 : 0;

            hoursCount += timeInCarHrs;
            priceCount = priceCount.add(new BigDecimal(unreconciledClaims.getNetValue()));

            String mainText = String.format("$%s, %s",StringHelper.FormatCurrency(unreconciledClaims.getNetValue()), unreconciledClaims.getStudent().FirstName + " " + unreconciledClaims.getStudent().LastName);
            String subText = timeInCarHrs > 0
                ? String.format("%s hour%s, %s", StringHelper.FormatOptionalDecimal(timeInCarHrs), timeInCarHrs !=1 ? "s" : "", DateHelper.FormatDateFromString(unreconciledClaims.getCreatedAt()))
                : DateHelper.FormatDateFromString(unreconciledClaims.getCreatedAt());

            ClaimListViewItem item = new ClaimListViewItem(ClaimListViewItem.ClaimsListViewItemType.UnreconciledItem, mainText, subText, i, unreconciledClaims.getId(), unreconciledClaims.getPackageDetailId(), unreconciledClaims.getStudent(), unreconciledClaims.getType());
            items.add(item);
        }
        if(claimsUnreconciled.size() > 0) {
            unreconciledItemsHeader.MainText = "Unreconciled";
            unreconciledItemsHeader.SubText = String.format("%s hour%s, $%s", StringHelper.FormatOptionalDecimal(hoursCount), hoursCount != 1 ? "s" : "",
                    (unreconciledValue != null)?StringHelper.FormatCurrency(unreconciledValue): StringHelper.FormatCurrency(priceCount));
            unreconciledItemsHeader.TotalTimeInHours = hoursCount;
        }
        totalHours += hoursCount;
        hoursCount = 0.0;
        priceCount = BigDecimal.ZERO;

        // Pending Items
        List<ClaimsPending> claimsPending = claims.getPending();

        if(claimsPending.size() > 0)
        {
            pendingItemsHeader = new ClaimListViewItem(ClaimListViewItem.ClaimsListViewItemType.PendingHeader, null, null, null, null, null, null, null);
            items.add(pendingItemsHeader);
        }

        for (int i = 0; i < claimsPending.size(); i++)
        {
            ClaimsPending pendingClaims = claimsPending.get(i);

            Double timeInCarHrs = pendingClaims.getTimeInCar() != null ? pendingClaims.getTimeInCar()/60.0 : 0;

            hoursCount += timeInCarHrs;
            priceCount = priceCount.add(new BigDecimal(pendingClaims.getNetValue()));

            String mainText = String.format("$%s, %s", StringHelper.FormatCurrency(pendingClaims.getNetValue()), pendingClaims.getStudent().FirstName + " " + pendingClaims.getStudent().LastName);
            String subText = timeInCarHrs > 0
                    ? String.format("%s hour%s, %s", StringHelper.FormatOptionalDecimal(timeInCarHrs), timeInCarHrs!=1 ? "s" : "", DateHelper.FormatDateFromString(pendingClaims.getCreatedAt()))
                    : DateHelper.FormatDateFromString(pendingClaims.getCreatedAt());

            ClaimListViewItem item = new ClaimListViewItem(ClaimListViewItem.ClaimsListViewItemType.PendingItem, mainText, subText, i, pendingClaims.getId(), pendingClaims.getPackageDetailId(), pendingClaims.getStudent(), pendingClaims.getType());
            items.add(item);
        }
        if(claimsPending.size() > 0) {
            pendingItemsHeader.MainText = "Pending";
            pendingItemsHeader.SubText = String.format("%s hour%s, $%s", StringHelper.FormatOptionalDecimal(hoursCount), hoursCount != 1 ? "s" : "",
                    (pendingValue != null)?StringHelper.FormatCurrency(pendingValue): StringHelper.FormatCurrency(priceCount));
            pendingItemsHeader.TotalTimeInHours = hoursCount;
        }
        totalHours += hoursCount;
        hoursCount = 0.0;
        priceCount = BigDecimal.ZERO;

        // Paid Items
        List<ClaimsPaid> claimsPaid = claims.getPaid();

        if(claimsPaid.size() > 0)
        {
            paidItemsHeader = new ClaimListViewItem(ClaimListViewItem.ClaimsListViewItemType.PaidHeader, null, null, null, null, null, null, null);
            items.add(paidItemsHeader);
        }

        for (int i = 0; i < claimsPaid.size(); i++)
        {
            ClaimsPaid paidClaims = claimsPaid.get(i);

            Double timeInCarHrs = paidClaims.getTimeInCar() != null ? paidClaims.getTimeInCar() /60.0 : 0;

            hoursCount += timeInCarHrs;
            priceCount = priceCount.add(new BigDecimal(paidClaims.getNetValue()));

            String mainText = String.format("$%s, %s", StringHelper.FormatCurrency(paidClaims.getNetValue()), paidClaims.getStudent().FirstName + " " + paidClaims.getStudent().LastName);

            String subText = timeInCarHrs > 0
                    ? String.format("%s hour%s, %s",StringHelper.FormatOptionalDecimal(timeInCarHrs),  timeInCarHrs !=1 ? "s" : "", DateHelper.FormatDateFromString(paidClaims.getCreatedAt()))
                    : DateHelper.FormatDateFromString(paidClaims.getCreatedAt());

            ClaimListViewItem item = new ClaimListViewItem(ClaimListViewItem.ClaimsListViewItemType.PaidItem, mainText, subText, i, paidClaims.getId(), paidClaims.getPackageDetailId(), paidClaims.getStudent(), paidClaims.getType());
            items.add(item);
        }

        if(claimsPaid.size() > 0) {
            paidItemsHeader.MainText = "Paid";

            paidItemsHeader.SubText = String.format("%s hour%s, $%s", StringHelper.FormatOptionalDecimal(hoursCount), hoursCount != 1 ? "s" : "", (paidValue != null)?  StringHelper.FormatCurrency(paidValue): StringHelper.FormatCurrency(priceCount));
            paidItemsHeader.SubText = String.format("%s hour%s, $%s", StringHelper.FormatOptionalDecimal(hoursCount), hoursCount != 1 ? "s" : "", (paidValue != null)?  StringHelper.FormatCurrency(paidValue): StringHelper.FormatCurrency(priceCount));
            paidItemsHeader.TotalTimeInHours = hoursCount;
        }
        totalHours += hoursCount;

        if(items.size() > 0) {
            addAll(items);
        }
    }

    public void setClaimItemHeaderValues(Double unreconciled, Double pending, Double paid)
    {
        unreconciledValue = unreconciled;
        pendingValue = pending;
        paidValue = paid;

        if(unreconciledItemsHeader != null){
            unreconciledItemsHeader.SubText =  String.format("%s hour%s, $%s", StringHelper.FormatOptionalDecimal(unreconciledItemsHeader.TotalTimeInHours),
                    unreconciledItemsHeader.TotalTimeInHours != 1 ? "s" : "", StringHelper.FormatCurrency(unreconciledValue));
        }
        if(pendingItemsHeader != null){
            pendingItemsHeader.SubText =  String.format("%s hour%s, $%s", StringHelper.FormatOptionalDecimal(pendingItemsHeader.TotalTimeInHours),
                    pendingItemsHeader.TotalTimeInHours != 1 ? "s" : "", StringHelper.FormatCurrency(pendingValue));
        }

        if(paidItemsHeader != null){
            paidItemsHeader.SubText = String.format("%s hour%s, $%s", StringHelper.FormatOptionalDecimal(paidItemsHeader.TotalTimeInHours),
                    paidItemsHeader.TotalTimeInHours != 1 ? "s" : "", StringHelper.FormatCurrency(paidValue));
        }

    }

    public void clearItemHeaderValues()
    {
        unreconciledValue = null;
        pendingValue = null;
        paidValue = null;
        unreconciledItemsHeader = null;
        pendingItemsHeader = null;
        paidItemsHeader = null;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        try {
            final ClaimListViewItem claimListViewItem = getItem(position);
            View view;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                view = inflater.inflate(layoutResourceId, null);

            } else {
                view = convertView;
            }

            RelativeLayout unreconciledHeaderContainer = (RelativeLayout) view.findViewById(R.id.UnreconciledHeader);
            RelativeLayout pendingHeaderContainer = (RelativeLayout) view.findViewById(R.id.PendingHeader);
            RelativeLayout paidHeaderContainer = (RelativeLayout) view.findViewById(R.id.PaidHeader);

            RelativeLayout unreconciledItemContainer = (RelativeLayout) view.findViewById(R.id.UnreconciledItem);
            RelativeLayout pendingItemContainer = (RelativeLayout) view.findViewById(R.id.PendingItem);
            RelativeLayout paidItemContainer = (RelativeLayout) view.findViewById(R.id.PaidItem);


            unreconciledHeaderContainer.setVisibility(View.GONE);
            pendingHeaderContainer.setVisibility(View.GONE);
            paidHeaderContainer.setVisibility(View.GONE);
            unreconciledItemContainer.setVisibility(View.GONE);
            pendingItemContainer.setVisibility(View.GONE);
            paidItemContainer.setVisibility(View.GONE);

            switch (claimListViewItem.Type)
            {
                case UnreconciledHeader:
                    unreconciledHeaderContainer.setVisibility(View.VISIBLE);
                    ((TextView) unreconciledHeaderContainer.findViewById(R.id.UnreconciledHeaderPriceTextView)).setText(claimListViewItem.SubText);
                    //((TextView) unreconciledHeaderContainer.findViewById(R.id.HoursPriceTextView)).setText(claimListViewItem.SubText);
                    break;

                case PendingHeader:
                    pendingHeaderContainer.setVisibility(View.VISIBLE);
                    ((TextView) pendingHeaderContainer.findViewById(R.id.PendingHeaderPriceTextView)).setText(claimListViewItem.SubText);
                    //((TextView) pendingHeaderContainer.findViewById(R.id.PriceTextView)).setText(claimListViewItem.SubText);

                    break;

                case PaidHeader:
                    paidHeaderContainer.setVisibility(View.VISIBLE);
                    ((TextView) paidHeaderContainer.findViewById(R.id.PaidHeaderPriceTextView)).setText(claimListViewItem.SubText);
                    //((TextView) paidHeaderContainer.findViewById(R.id.DatePriceTextView)).setText(claimListViewItem.SubText);
                    break;

                case UnreconciledItem:
                    unreconciledItemContainer.setVisibility(View.VISIBLE);
                    ((TextView) unreconciledItemContainer.findViewById(R.id.UnreconciledItemMainTextView)).setText(claimListViewItem.MainText);
                    ((TextView) unreconciledItemContainer.findViewById(R.id.UnreconciledItemSubTextView)).setText(claimListViewItem.SubText);
                    unreconciledItemContainer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            showDetails(claimListViewItem);
                        }
                    });
                    break;

                case PaidItem:
                    paidItemContainer.setVisibility(View.VISIBLE);
                    ((TextView) paidItemContainer.findViewById(R.id.PaidItemMainTextView)).setText(claimListViewItem.MainText);
                    ((TextView) paidItemContainer.findViewById(R.id.PaidItemSubTextView)).setText(claimListViewItem.SubText);
                    paidItemContainer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            showDetails(claimListViewItem);
                        }
                    });
                    break;

                case PendingItem:
                    pendingItemContainer.setVisibility(View.VISIBLE);
                    ((TextView) pendingItemContainer.findViewById(R.id.PendingItemMainTextView)).setText(claimListViewItem.MainText);
                    ((TextView) pendingItemContainer.findViewById(R.id.PendingItemSubTextView)).setText(claimListViewItem.SubText);
                    pendingItemContainer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            showDetails(claimListViewItem);
                        }
                    });
                    break;
            }

            return view;

        } catch (Exception ex) {
            Log.e("TransListViewAdapter", "error", ex);
            return null;
        }
    }

    private void showDetails(ClaimListViewItem claimListViewItem){
        ((TransactionsActivity) callingActivity).ShowTransactionDetails(claimListViewItem);
    }
}

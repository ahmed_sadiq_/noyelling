package au.com.saybravo.noyelling.student.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EmailBody
{
    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("fname")
    @Expose
    public String fname;

    @SerializedName("lname")
    @Expose
    public String lname;

    public EmailBody(String email, String fname, String lname) {
        this.email = email;
        this.fname = fname;
        this.lname = lname;
    }
}


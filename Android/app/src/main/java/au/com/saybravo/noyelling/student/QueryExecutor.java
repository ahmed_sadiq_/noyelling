package au.com.saybravo.noyelling.student;

import android.support.v4.app.Fragment;

/**
 * Created by ahmed.sadiq on 18/04/16.
 */
public interface QueryExecutor  {

    void setQueryString(String query);

}

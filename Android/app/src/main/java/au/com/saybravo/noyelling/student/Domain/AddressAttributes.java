package au.com.saybravo.noyelling.student.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddressAttributes {

    @SerializedName("place_id")
    @Expose
    public String PlaceId;

    public AddressAttributes(String placeId)
    {
        PlaceId = placeId;
    }
}

package au.com.saybravo.noyelling.student.Domain.CreateStudent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmed.sadiq on 31/05/16.
 */
public class CustomerAttributes {

    @SerializedName("email")
    @Expose
    private String email;

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

}
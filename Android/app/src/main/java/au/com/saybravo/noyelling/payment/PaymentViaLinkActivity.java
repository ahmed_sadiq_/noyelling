package au.com.saybravo.noyelling.payment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.stripe.android.util.TextUtils;

import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.app.BaseActivity;
import au.com.saybravo.noyelling.app.ProgressButton.ProgressButton;
import au.com.saybravo.noyelling.app.ProgressButton.ProgressButtonCallback;
import au.com.saybravo.noyelling.app.Services.JWTRefreshService;
import au.com.saybravo.noyelling.app.Services.ServiceGenerator;
import au.com.saybravo.noyelling.app.util.AppConstants;
import au.com.saybravo.noyelling.app.util.ValidationUtil;
import au.com.saybravo.noyelling.lessonPackage.LessonPackageActivity;
import au.com.saybravo.noyelling.payment.Domain.Payment;
import au.com.saybravo.noyelling.payment.Domain.PaymentBody;
import au.com.saybravo.noyelling.student.Domain.EmailBody;
import au.com.saybravo.noyelling.student.Domain.Student;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentViaLinkActivity extends BaseActivity {


    EditText emailEditText;
    EditText firstNameEditText;
    EditText lastNameEditText;
    ProgressButton actionButton;
    Button cancelButton;
    TextView toolBarTitle;
    Boolean isLoading = false;

    private Student student;
    private PaymentBody paymentBody;

    public PaymentViaLinkActivity() {
        super(R.layout.activity_payment_via_link);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            student = (Student) bundle.getSerializable(AppConstants.INTENT_KEY_STUDENT);
            paymentBody = (PaymentBody) bundle.getSerializable(AppConstants.INTENT_KEY_PAYMENT);
        }
        init();
    }

    private void init() {
        toolBarTitle = (TextView) findViewById(R.id.toolbar_title);
        emailEditText = (EditText) findViewById(R.id.email);
        firstNameEditText = (EditText) findViewById(R.id.FirstName);
        lastNameEditText = (EditText) findViewById(R.id.LastName);
        cancelButton = (Button) findViewById(R.id.CancelButton);
        View layoutContainer = findViewById(R.id.layoutContainer);
        actionButton = new ProgressButton(layoutContainer, R.id.ActionButton,
                R.string.Send_Payment_Link,
                R.string.LessonPackage_CreateBtnProcessingText,
                isLoading != null && isLoading.booleanValue(),
                new ProgressButtonCallback() {
                    @Override
                    public void callback() {
                        if (isLoading == null || isLoading == false) {
                            isLoading = true;
                            processInputs();
                        }
                    }
                });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(paymentBody != null) {
                    Payment paymentInfo = paymentBody.getPayment();
                    deletePayment(paymentInfo.getStudent().getId(), paymentInfo.getId());
                    onBackPressed();
                }
            }
        });
        if(paymentBody != null) {
            toolBarTitle.setText("Total $"+paymentBody.getPayment().getPackage().getSalePrice());
        } if(student != null) {
            emailEditText.setText(student.Customer.Email);
            firstNameEditText.setText(student.FirstName);
            lastNameEditText.setText(student.LastName);
        }
    }

    private void processInputs(){
        String email = emailEditText.getText().toString();
        String firstName = firstNameEditText.getText().toString();
        String lastName = lastNameEditText.getText().toString();

        String validityError = checkInputValidity(email, firstName, lastName);
        if(validityError.isEmpty()){
            submitEmailForPaymentLink(paymentBody, email, firstName, lastName);
        } else {
            Toast.makeText(this, validityError, Toast.LENGTH_LONG).show();
            updateProgressButton(false, getString(R.string.Send_Payment_Link));
        }
    }

    private String checkInputValidity(String email, String firstName, String lastName) {
        String errorString = "";
        if(TextUtils.isBlank(email) || !ValidationUtil.isEmailValid(email)) {
            errorString = getString(R.string.error_email_validity);
        } else if(TextUtils.isBlank(firstName)) {
            errorString = getString(R.string.first_name_validity);
        } else if(TextUtils.isBlank(lastName)) {
            errorString = getString(R.string.last_name_validity);
        }
        return errorString;
    }

    private void updateProgressButton(boolean loadingStatus, String text) {
        if(actionButton != null) {
            isLoading = loadingStatus;
            actionButton.setLoadingStatus(loadingStatus);
            actionButton.setText(text);
        }
    }

    public void submitEmailForPaymentLink(final PaymentBody paymentBody, String email, String firstName, String lastName) {
        updateProgressButton(true, getString(R.string.LessonPackage_CreateBtnProcessingText));
        ServiceGenerator.StudentService studentService =
                ServiceGenerator.createService(ServiceGenerator.StudentService.class, this );

        EmailBody emailBody = new EmailBody(email, firstName, lastName);
        Call<ResponseBody> call = studentService.ProcessEmailLink(student.Id, paymentBody.getPayment().getId(),
                emailBody);

        JWTRefreshService.enqueueSafe(call, this, new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()) {
                    Toast.makeText(PaymentViaLinkActivity.this, PaymentViaLinkActivity.this.getString(R.string.success_message_payment_link)+" "+student.Customer.Email,
                            Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(PaymentViaLinkActivity.this, PaymentSuccessfulActivity.class);
                    intent.putExtra(AppConstants.INTENT_KEY_STUDENT, student);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(PaymentViaLinkActivity.this,response.message()+"/"+response.code(), Toast.LENGTH_LONG).show();
                    updateProgressButton(false, getString(R.string.Send_Payment_Link));
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(PaymentViaLinkActivity.this,"Something Went Wrong.", Toast.LENGTH_LONG).show();
                updateProgressButton(false, getString(R.string.Send_Payment_Link));
            }
        });
    }

    private void deletePayment(final int studentId, final int paymentId){

        ServiceGenerator.ClaimService productService = ServiceGenerator.createService(ServiceGenerator.ClaimService.class, getApplicationContext());
        Call<Void> call = productService.DeletePendingPayment(studentId, paymentId);

        JWTRefreshService.enqueueSafe(call, this, new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Toast.makeText(getApplicationContext(), R.string.Payment_Cancel_Message, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.i("","");
            }
        });

    }

    @Override
    public void onBackPressed() {
        if(paymentBody != null) {
            Payment paymentInfo = paymentBody.getPayment();
            deletePayment(paymentInfo.getStudent().getId(), paymentInfo.getId());
        }
        super.onBackPressed();
    }
}

package au.com.saybravo.noyelling.lessonPackage.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmed.sadiq on 31/05/16.
 */
public class ProductItem {

    @SerializedName("product_id")
    @Expose
    public Integer productId;

    @SerializedName("quantity")
    @Expose
    public Integer quantity;
}

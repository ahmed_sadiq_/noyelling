package au.com.saybravo.noyelling.lessons;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.app.BaseActivity;
import au.com.saybravo.noyelling.app.NoYellingApplication;
import au.com.saybravo.noyelling.app.ProgressButton.ProgressButton;
import au.com.saybravo.noyelling.app.ProgressButton.ProgressButtonCallback;
import au.com.saybravo.noyelling.app.Services.JWTRefreshService;
import au.com.saybravo.noyelling.app.Services.ServiceGenerator;
import au.com.saybravo.noyelling.app.util.AppConstants;
import au.com.saybravo.noyelling.claim.ClaimActivity;
import au.com.saybravo.noyelling.lessons.Domain.Claim;
import au.com.saybravo.noyelling.lessons.Domain.StudentHistory;
import au.com.saybravo.noyelling.lessons.Domain.StudentHistoryBody;
import au.com.saybravo.noyelling.lessons.Domain.StudentPostClaimsBody;
import au.com.saybravo.noyelling.lessons.Domain.StudentPostClaimsResponseBody;
import au.com.saybravo.noyelling.student.Domain.Student;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LessonsActivity extends BaseActivity {

    public LessonsActivity() {
        super(R.layout.activity_lessons);
    }

    private Student student;

    private ServiceGenerator.StudentService studentService;

    private ListView listView;
    private TextView noItemsText;
    private ProgressBar loadingProgressBar;
    private LinearLayout lessonsLinearLayout;

    private List<StudentHistory> payments = new ArrayList<>();
    private ArrayList<PaymentToClaim> paymentsToClaim;

    ProgressButton claimLessonsButton;

    LessonListViewAdapter lessonListViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        noItemsText = (TextView) findViewById(R.id.NoItemsText);
        loadingProgressBar = (ProgressBar) findViewById(R.id.LoadingProgressBar);
        lessonsLinearLayout = (LinearLayout) findViewById(R.id.LessonsLinearLayout);

        loadingProgressBar.setIndeterminate(true);
        noItemsText.setVisibility(View.GONE);

        startLoading();

        student = (Student) getIntent().getSerializableExtra(IntentPayloadIdentifier);

        setToolbarTitle(student.FirstName + "'s Lessons");

        studentService = ServiceGenerator.createService(ServiceGenerator.StudentService.class, getApplicationContext());

        listView = (ListView) findViewById(R.id.LessonsListView);
        lessonListViewAdapter = new LessonListViewAdapter(this, R.layout.layout_lessons_listview);
        listView.setAdapter(lessonListViewAdapter);


        claimLessonsButton = new ProgressButton(this, R.id.ClaimLessonsButton, R.string.StudentLessons_ClaimButton_Text, R.string.StudentLessons_ClaimButton_LoadingText, new ProgressButtonCallback() {
            @Override
            public void callback() {
                claimLessons();
                ((NoYellingApplication)getApplication()).AddedClaim = true;
            }
        });

        claimLessonsButton.setVisibility(View.GONE);
    }

    @Override
    protected void onStart() {
        super.onStart();

        loadStudentHistory();

        paymentsToClaim = new ArrayList<>();
    }

    private void loadStudentHistory() {

        claimLessonsButton.setVisibility(View.GONE);
        Call<StudentHistoryBody> call = studentService.GetStudentHistory(GetCurrentInstructorId(), student.Id);

        JWTRefreshService.enqueueSafe(call, this,new Callback<StudentHistoryBody>() {
            @Override
            public void onResponse(Call<StudentHistoryBody> call, Response<StudentHistoryBody> response) {
                try {

                    if (response.isSuccessful()) {

                        bindAdapter(response.body().history);

                    } else {
                        ShowGenericError();
                    }

                    stopLoading();

                } catch (Exception e) {

                    ShowGenericError(e);
                    stopLoading();
                }
            }

            @Override
            public void onFailure(Call<StudentHistoryBody> call, Throwable t) {

                ShowGenericError();
                stopLoading();
            }
        });
    }

    private void bindAdapter(StudentHistory[] history)
    {
        payments = new ArrayList<>();
        for (StudentHistory item : history)
        {
            if (item.Category.equals("payment"))
            {
                payments.add(item);
            }
        }

        if (payments.size() == 0)
        {
            noItemsText.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        }

        lessonListViewAdapter.populateItems(payments);
    }

    private void startLoading()
    {
        lessonsLinearLayout.setVisibility(View.GONE);
        loadingProgressBar.setVisibility(View.VISIBLE);

    }

    private void stopLoading()
    {
        lessonsLinearLayout.setVisibility(View.VISIBLE);
        loadingProgressBar.setVisibility(View.GONE);
    }

    // TODO SD - There is an issue here, if you check an item, then view a claimed item, then go back the counter is not at 0
    public void CheckboxClickHandler(int paymentId, int packageDetailId, boolean checked)
    {
        int totalClaims = 0;
        PaymentToClaim paymentToClaim = null;
        for (int i = 0; i < paymentsToClaim.size(); i++)
        {
            PaymentToClaim tempPaymentToClaim = paymentsToClaim.get(i);

            if (tempPaymentToClaim.PaymentId == paymentId)
            {
                paymentToClaim = paymentsToClaim.get(i);
            }

            totalClaims += tempPaymentToClaim.PackageDetailIds.size();
        }

        if (checked)
        {
            if (paymentToClaim == null)
            {
                paymentToClaim = new PaymentToClaim(paymentId);
                paymentsToClaim.add(paymentToClaim);
            }

            paymentToClaim.PackageDetailIds.add(packageDetailId);

            totalClaims ++;
        }
        else
        {
            if(paymentToClaim != null && paymentToClaim.PackageDetailIds != null) {
                int indexToRemove = paymentToClaim.PackageDetailIds.indexOf(packageDetailId);
                if(indexToRemove >= 0) {
                    paymentToClaim.PackageDetailIds.remove(indexToRemove);
                }

                if (paymentToClaim.PackageDetailIds.size() == 0) {
                    paymentsToClaim.remove(paymentToClaim);
                }

                totalClaims--;
            }
        }

        if (totalClaims == 0)
        {
            claimLessonsButton.setVisibility(View.GONE);
        }
        else
        {
            String buttonText = String.format("Claim %s Lesson%s", totalClaims, totalClaims != 1 ? "s" : "");
            claimLessonsButton.setText(buttonText);

            claimLessonsButton.setVisibility(View.VISIBLE);
        }
    }

    private void claimLessons()
    {
        if (paymentsToClaim.size() == 0)
        {
            claimLessonsButton.hideLoading();
            loadStudentHistory();

            return;
        }

        startLoading();

        final PaymentToClaim paymentToClaim = paymentsToClaim.get(0);

        List<Claim> claimsList = new ArrayList<>();
        for (int i = 0; i < paymentToClaim.PackageDetailIds.size(); i++)
        {
            claimsList.add(new Claim(paymentToClaim.PackageDetailIds.get(i)));
        }

        Claim[] claims = claimsList.toArray(new Claim[claimsList.size()]);
        StudentPostClaimsBody body = new StudentPostClaimsBody(claims);

        Call<StudentPostClaimsResponseBody> call = studentService.PostClaims(GetCurrentInstructorId(), student.Id, paymentToClaim.PaymentId, body);

        JWTRefreshService.enqueueSafe(call, this, new Callback<StudentPostClaimsResponseBody>() {
            @Override
            public void onResponse(Call<StudentPostClaimsResponseBody> call, Response<StudentPostClaimsResponseBody> response) {
                try {

                    if (response.isSuccessful()) {

                        paymentsToClaim.remove(paymentToClaim);

                        claimLessons();

                    } else {
                        ShowGenericError();
                    }

                } catch (Exception e) {

                    ShowGenericError(e);
                    stopLoading();
                }
            }

            @Override
            public void onFailure(Call<StudentPostClaimsResponseBody> call, Throwable t) {

                ShowGenericError();
                stopLoading();
            }
        });
    }

    public void ClaimedClickHandler(int claimId, String claimType) {

        Intent intent = new Intent(getApplicationContext(), ClaimActivity.class);
        intent.putExtra("claimId", claimId);
        intent.putExtra("student", student);
        intent.putExtra("hideViewStudent", true);
        intent.putExtra("claim_type", claimType);
        startActivityForResult(intent , 0);

        paymentsToClaim = new ArrayList<>();
    }

    class PaymentToClaim
    {
        public int PaymentId;
        public List<Integer> PackageDetailIds;

        public PaymentToClaim(Integer paymentId)
        {
            PaymentId = paymentId;
            PackageDetailIds = new ArrayList<>();
        }
    }
}


package au.com.saybravo.noyelling.student.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import au.com.saybravo.noyelling.app.NoYellingApplication;
import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.app.Services.JWTRefreshService;
import au.com.saybravo.noyelling.app.Services.ServiceGenerator;
import au.com.saybravo.noyelling.student.Domain.SearchStudentsBody;
import au.com.saybravo.noyelling.student.Domain.Student;
import au.com.saybravo.noyelling.student.Domain.StudentRealmModel;
import au.com.saybravo.noyelling.student.QueryExecutor;
import au.com.saybravo.noyelling.student.Recycler.BaseStudentRecyclerViewAdapter;
import au.com.saybravo.noyelling.student.Recycler.StudentsRecyclerViewAdapter;
import au.com.saybravo.noyelling.student.StudentActivity;
import au.com.saybravo.noyelling.student.StudentsActivity;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by ahmed.sadiq on 13/04/16.
 */
public class StudentsFragment extends Fragment implements QueryExecutor {

    private RecyclerView recyclerView;
    private RelativeLayout noStudents;
    private StudentsRecyclerViewAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private int studentListPageNumber = 1;
    private boolean noMoreStudents = false;
    private String queryString = null;
    private RealmConfiguration realmConfiguration;
    private Realm realm;
    private final int maxRecentStudents = 50;
    private Call<SearchStudentsBody> currentSearch = null;

    private boolean loading = false;

    public List<Student> students = new ArrayList<Student>();

    public void initializeStudents(){
        if(students.size() == 0){
            getMoreStudentsFromServer();
        }
    }

    private void getMoreStudentsFromServer(){

        NoYellingApplication app = (NoYellingApplication)getActivity().getApplication();

        if(loading == true || noMoreStudents == true || app.CurrentInstructor == null){

            return;
        }
        loading = true;

        students.add(null);
        mAdapter.notifyItemInserted(students.size() -1);
        
        ServiceGenerator.StudentService studentService =  ServiceGenerator.createService(ServiceGenerator.StudentService.class, getContext());

        currentSearch =  studentService.SearchStudents(app.CurrentInstructor.id, queryString, NoYellingApplication.NumberOfStudentsPerPage, studentListPageNumber );
        JWTRefreshService.enqueueSafe(currentSearch, getActivity(), new Callback<SearchStudentsBody>() {
            @Override
            public void onResponse(Call<SearchStudentsBody> call, Response<SearchStudentsBody> response) {
                if(response.isSuccessful()){
                    currentSearch = null;
                    students.remove(students.size() -1);


                    SearchStudentsBody result = response.body();
                    if(result.students.length < NoYellingApplication.NumberOfStudentsPerPage){
                        // no more students;
                        noMoreStudents = true;
                    }

                    studentListPageNumber++;
                    students.addAll(Arrays.asList(result.students));
                    mAdapter.notifyDataSetChanged();

                    if (students.size() == 0)
                    {
                        noStudents.setVisibility(View.VISIBLE);
                    }else{
                        noStudents.setVisibility(View.GONE);
                    }
                }
                mAdapter.setLoaded();
                loading = false;

            }

            @Override
            public void onFailure(Call<SearchStudentsBody> call, Throwable t) {
                loading = false;
            }

        });
    }

    private void putStudentToRecentViewed(Student student){

        //TODO should do this on a background thread and not hold up the UI thread

        long numberOfRecent =  realm.where(StudentRealmModel.class).count();

        if(numberOfRecent >= maxRecentStudents){

            RealmResults<StudentRealmModel> result = realm.where(StudentRealmModel.class).findAllSorted("lastAccessed", Sort.ASCENDING);

            realm.beginTransaction();

            int loop = (int)numberOfRecent - maxRecentStudents;

            for(int i = 0 ; i < loop; i++ ) {
                result.remove(0);
            }

            realm.commitTransaction();
        }

        realm.beginTransaction();

        StudentRealmModel foundStudent =  realm.where(StudentRealmModel.class).equalTo("id", student.Id).findFirst();

        if(foundStudent != null){
            // update the lastAccessed
            foundStudent.email = student.GetEmail();
            foundStudent.firstName = student.FirstName;
            foundStudent.lastName = student.LastName;
            foundStudent.suburb = student.Address.Suburb;
            foundStudent.lastAccessed = new Date();

        }else{
            StudentRealmModel newStudent = realm.createObject(StudentRealmModel.class);

            newStudent.id = student.Id;
            newStudent.email = student.GetEmail();
            newStudent.firstName = student.FirstName;
            newStudent.lastName = student.LastName;
            newStudent.suburb = student.Address.Suburb;
            newStudent.lastAccessed = new Date();
        }


        realm.commitTransaction();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        super.onCreateView(inflater,container,savedInstanceState);

        View v = inflater.inflate(R.layout.layout_students_fragment, container, false);

        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerview);
        noStudents = (RelativeLayout) v.findViewById(R.id.NoStudents);
        noStudents.setVisibility(View.GONE);

        recyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new StudentsRecyclerViewAdapter(students, recyclerView);

        mAdapter.setOnItemClickListener(new BaseStudentRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {

                final BaseStudentRecyclerViewAdapter.OnItemClickListener listener = this;
                final View clickedView = view;

                StudentsActivity studentsActivity = (StudentsActivity)getActivity();
                if(!studentsActivity.isNetworkConnected()){
                    studentsActivity.showNoConnectionNotification(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listener.onItemClick(clickedView, position);
                        }
                    });

                    return;
                }
                Intent intent = new Intent(getContext(), StudentActivity.class);
                intent.putExtra("Payload", students.get(position).Id);
                intent.putExtra("student", students.get(position));
                startActivity(intent);

                putStudentToRecentViewed(students.get(position));
            }
        });

        mAdapter.setOnLoadMoreListener(new StudentsRecyclerViewAdapter.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {

                getMoreStudentsFromServer();



            }
        });

        recyclerView.setAdapter(mAdapter);


        realmConfiguration = new RealmConfiguration.Builder(getContext()).name("recentStudents.realm").build();
        realm = Realm.getInstance(realmConfiguration);

        return v;
    }
    @Override
    public void onStart(){
        super.onStart();
        long numberOfRecent =  realm.where(StudentRealmModel.class).count();

        if (numberOfRecent == 0)
        {
            ((StudentsActivity)getActivity()).skipRecentStudents();
        }

    }

    @Override
    public void setQueryString(String query) {

        if(loading == false) {

            studentListPageNumber = 1;
            noMoreStudents = false;
            queryString = query;
            students.clear();
            mAdapter.notifyDataSetChanged();

            getMoreStudentsFromServer();

        }else{

            if(currentSearch != null){

                currentSearch.cancel();
                students.remove(students.size() -1);
                mAdapter.notifyDataSetChanged();
                loading = false;
                queryString = query;
                getMoreStudentsFromServer();
            }
        }
    }
}

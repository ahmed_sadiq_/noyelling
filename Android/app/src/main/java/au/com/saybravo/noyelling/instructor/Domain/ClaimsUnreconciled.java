package au.com.saybravo.noyelling.instructor.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import au.com.saybravo.noyelling.student.Domain.Student;

/**
 * Created by ahmed.sadiq on 19/04/2016.
 */
public class ClaimsUnreconciled {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("student")
    @Expose
    private Student student;
    @SerializedName("time_in_car")
    @Expose
    private Integer timeInCar;
    @SerializedName("package_detail_id")
    @Expose
    private Integer packageDetailId;
    @SerializedName("instructor_id")
    @Expose
    private Integer instructorId;
    @SerializedName("payment_id")
    @Expose
    private Integer paymentId;
    @SerializedName("commission_rate")
    @Expose
    private Double commissionRate;
    @SerializedName("net_value")
    @Expose
    private Double netValue;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("reconciled_at")
    @Expose
    private Object reconciledAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("type")
    @Expose
    private String type;


    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The student
     */
    public Student getStudent() {
        return student;
    }

    /**
     *
     * @param student
     * The student
     */
    public void setStudent(Student student) {
        this.student = student;
    }

    /**
     *
     * @return
     * The timeInCar
     */
    public Integer getTimeInCar() {
        return timeInCar;
    }

    /**
     *
     * @param timeInCar
     * The time_in_car
     */
    public void setTimeInCar(Integer timeInCar) {
        this.timeInCar = timeInCar;
    }

    /**
     *
     * @return
     * The packageDetailId
     */
    public Integer getPackageDetailId() {
        return packageDetailId;
    }

    /**
     *
     * @param packageDetailId
     * The package_detail_id
     */
    public void setPackageDetailId(Integer packageDetailId) {
        this.packageDetailId = packageDetailId;
    }

    /**
     *
     * @return
     * The instructorId
     */
    public Integer getInstructorId() {
        return instructorId;
    }

    /**
     *
     * @param instructorId
     * The instructor_id
     */
    public void setInstructorId(Integer instructorId) {
        this.instructorId = instructorId;
    }

    /**
     *
     * @return
     * The paymentId
     */
    public Integer getPaymentId() {
        return paymentId;
    }

    /**
     *
     * @param paymentId
     * The payment_id
     */
    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    /**
     *
     * @return
     * The commissionRate
     */
    public Double getCommissionRate() {
        return commissionRate;
    }

    /**
     *
     * @param commissionRate
     * The commission_rate
     */
    public void setCommissionRate(Double commissionRate) {
        this.commissionRate = commissionRate;
    }

    /**
     *
     * @return
     * The netValue
     */
    public Double getNetValue() {
        return netValue;
    }

    /**
     *
     * @param netValue
     * The net_value
     */
    public void setNetValue(Double netValue) {
        this.netValue = netValue;
    }

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The reconciledAt
     */
    public Object getReconciledAt() {
        return reconciledAt;
    }

    /**
     *
     * @param reconciledAt
     * The reconciled_at
     */
    public void setReconciledAt(Object reconciledAt) {
        this.reconciledAt = reconciledAt;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

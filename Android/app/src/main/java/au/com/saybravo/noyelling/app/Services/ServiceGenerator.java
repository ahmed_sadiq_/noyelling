package au.com.saybravo.noyelling.app.Services;

/**
 * Created by ahmed.sadiq on 6/04/2016.
 */

import android.content.Context;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import au.com.saybravo.noyelling.AppConfig;
import au.com.saybravo.noyelling.app.Domain.AuthBody;
import au.com.saybravo.noyelling.app.Domain.JWToken;
import au.com.saybravo.noyelling.app.Domain.PayPalAccessToken;
import au.com.saybravo.noyelling.app.NoYellingApplication;
import au.com.saybravo.noyelling.claim.Domain.ClaimBody;
import au.com.saybravo.noyelling.instructor.Domain.ClaimsBody;
import au.com.saybravo.noyelling.instructor.Domain.ClaimsStatisticsBody;
import au.com.saybravo.noyelling.instructor.Domain.InstructorAvailableBody;
import au.com.saybravo.noyelling.instructor.Domain.InstructorBody;
import au.com.saybravo.noyelling.instructor.Domain.InstructorUpdateBody;
import au.com.saybravo.noyelling.lessonPackage.Domain.DiscountValueBody;
import au.com.saybravo.noyelling.lessonPackage.Domain.PackageDetailsBody;
import au.com.saybravo.noyelling.lessonPackage.Domain.ProductsBody;
import au.com.saybravo.noyelling.lessons.Domain.StudentHistoryBody;
import au.com.saybravo.noyelling.lessons.Domain.StudentPostClaimsBody;
import au.com.saybravo.noyelling.lessons.Domain.StudentPostClaimsResponseBody;
import au.com.saybravo.noyelling.login.Domain.ForgotPasswordBody;
import au.com.saybravo.noyelling.login.Domain.ForgotPasswordResponse;
import au.com.saybravo.noyelling.payment.Domain.CashPackageBody;
import au.com.saybravo.noyelling.payment.Domain.PaymentBody;
import au.com.saybravo.noyelling.payment.Domain.ReconcileBody;
import au.com.saybravo.noyelling.student.Domain.CreateStudent.StudentCreateBody;
import au.com.saybravo.noyelling.student.Domain.EmailBody;
import au.com.saybravo.noyelling.student.Domain.SearchStudentsBody;
import au.com.saybravo.noyelling.student.Domain.StudentBody;
import au.com.saybravo.noyelling.student.Domain.StudentCardBody;
import au.com.saybravo.noyelling.student.Domain.StudentUpdateBody;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class ServiceGenerator {

    public static final String BASICAUTH_USERNAME = "admin";
    public static final String BASICAUTH_PASSWORD = "PlzNoTelling";
    private static Context _context;

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(AppConfig.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    public static <S> S createService(Class<S> serviceClass, Context context) {
        return createService(serviceClass, context, false);
    }

    public static <S> S createService(Class<S> serviceClass, Context context, Boolean isLoginService) {
        _context = context;

        OkHttpClient.Builder httpClient =  new OkHttpClient.Builder();

        /* TEMP REMOVAL
        if(isLoginService) {
                String credentials = BASICAUTH_USERNAME + ":" + BASICAUTH_PASSWORD;
                final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                if(!NoYellingApplication.IsLive) {
                    HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                    logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                    httpClient.interceptors().add(logging);
                }

                httpClient.addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Interceptor.Chain chain) throws IOException {
                        Request original = chain.request();
                        Request.Builder requestBuilder = original.newBuilder();
                        requestBuilder.header("Authorization", basic);
                        requestBuilder.header("Accept", "application/json");
                        requestBuilder.method(original.method(), original.body());
                        Request request = requestBuilder.build();
                        return chain.proceed(request);
                    }
                });
        } */

        if(!isLoginService)
        {
            if(!NoYellingApplication.IsLive)
            {
                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                httpClient.interceptors().add(logging);
            }

            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Interceptor.Chain chain) throws IOException {
                    Request original = chain.request();
                    Request.Builder requestBuilder = original.newBuilder();
                    String jWToken = JWTokenService.getToken(_context);
                    if (jWToken != null && !jWToken.isEmpty() && !jWToken.equals("null"))
                    {
                       //Log.i(this.getClass().getSimpleName(), " ################ BRAVO LOG ################ : " + jWToken);
                        // 401 will be thrown so catch in Activty
                    }else
                    {
                        //Log.i(this.getClass().getSimpleName(), " ################ BRAVO LOG ################ : 401!!!!");
                    }

                    requestBuilder.header("Authorization", "Bearer " + jWToken);
                    requestBuilder.header("Accept", "application/json");
                    requestBuilder.method(original.method(), original.body());
                    Request request = requestBuilder.build();

                    return chain.proceed(request);
                }
            });
        }

        OkHttpClient client = httpClient
                .connectTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES)
                .build();

        Retrofit retrofit = builder.client(client).build();
        return retrofit.create(serviceClass);
    }

    public interface LoginService {
        @POST("/instructor_token")
        Call<JWToken> Basiclogin(@Body AuthBody authBody);

        @GET("/api/v1/paypal/refresh")
        Call<PayPalAccessToken> GetPayPalAccessToken();
    }

    public interface InstructorService {

        @POST("/api/v1/instructors/{email}/password_reset")
        Call<ForgotPasswordResponse> ForgotPassword(@Path("email") String email, @Body ForgotPasswordBody body);

        @GET("/api/v1/current_instructor/")
        Call<InstructorBody> GetInstructor();

        @GET("/api/v1/instructors/{id}/statistics/claims/")
        Call<ClaimsStatisticsBody> GetInstructorEarnings(@Path("id") int instructorId, @Query("week") int week);

        @GET("/api/v1/instructors/{id}/claims")
        Call<ClaimsBody> GetInstructorClaims(@Path("id") int instructorId, @Query("week") int week);

        @PATCH("/api/v1/current_instructor/")
        Call<Void> SetAvailability(@Body InstructorAvailableBody instructorAvailableBody);

        @PATCH("/api/v1/current_instructor/")
        Call<InstructorBody> Update(@Body InstructorUpdateBody instructorUpdateBody);
    }

    public interface StudentService
    {
        @GET("/api/v1/instructors/{instructor_id}/students")
        Call<SearchStudentsBody> SearchStudents(@Path("instructor_id") int instructorId, @Query("search") String search, @Query("per_page") int perPage, @Query("page") int page);

        @GET("/api/v1/instructors/{instructor_id}/students/{student_id} ")
        Call<StudentBody> GetStudent(@Path("instructor_id") int instructorId, @Path("student_id") int studentId);

        @PATCH("/api/v1/instructors/{instructor_id}/students/{student_id}")
        Call<StudentBody> Update(@Path("instructor_id") int instructorId, @Path("student_id") int studentId, @Body StudentUpdateBody studentUpdateBody);

        @GET("/api/v1/instructors/{instructor_id}/students/{student_id}/history")
        Call<StudentHistoryBody> GetStudentHistory(@Path("instructor_id") int instructorId, @Path("student_id") int studentId);

        @POST("/api/v1/instructors/{instructor_id}/students/{student_id}/payments/{payment_id}/claim ")
        Call<StudentPostClaimsResponseBody> PostClaims(@Path("instructor_id") int instructorId, @Path("student_id") int studentId, @Path("payment_id") int paymentId, @Body StudentPostClaimsBody body);

        @POST("/api/v1/instructors/{instructor_id}/students")
        Call<StudentBody> CreateStudent(@Path("instructor_id") int instructorId, @Body StudentCreateBody studentCreateBody);

        @PATCH("/api/v1/students/{student_id}/payments/{id}/charge")
        Call<PaymentBody> ProcessCreditCard(@Path("student_id") int studentId, @Path("id") int paymentId,
                                            @Body StudentCardBody studentCardBody);

        @PATCH("/api/v1/students/{student_id}/cash_payments/{cash_payment_id}/tender")
        Call<PaymentBody> ChargeCashPayment(@Path("student_id") int studentId, @Path("cash_payment_id") int cash_payment_id);


        @POST("/api/v1/students/{student_id}/payments/{id}/payment_link")
        Call<ResponseBody> ProcessEmailLink(@Path("student_id") int studentId, @Path("id") int paymentId, @Body EmailBody emailLinkBody);
    }

    public interface  ClaimService
    {
        @GET("/api/v1/instructors/{instructor_id}/claims/{claim_id}")
        Call<ClaimBody> GetClaim(@Path("instructor_id") int instructorId, @Path("claim_id") int claimId);

        @DELETE("/api/v1/instructors/{instructor_id}/claims/{claim_id}")
        Call<ClaimBody> DeleteClaim(@Path("instructor_id") int instructorId, @Path("claim_id") int claimId);

        @DELETE("/api/v1/students/{student_id}/payments/{payment_id}")
        Call<Void> DeletePendingPayment(@Path("student_id") int studentId, @Path("payment_id") int paymentId);

    }


    public interface ProductService {

        @GET("/api/v1/instructors/{instructor_id}/products")
        Call<ProductsBody> GetProducts(@Path("instructor_id") int instructor_id);

        @GET("/api/v1/pub/packages?tags=cash")
        Call<CashPackageBody> GetCashPackages();

        @POST("/api/v1/pub/discount/package")
        Call<DiscountValueBody> PostDiscont(@Body PackageDetailsBody discountPackageBody);

        @POST("/api/v1/students/{student_id}/payments")
        Call<PaymentBody> PostPayment(@Path("student_id") int student_id, @Body PackageDetailsBody packageDetailsBody);

        @POST("/api/v1/students/{student_id}/cash_payments")
        Call<PaymentBody> CreateUnpaidCashPayment(@Path("student_id") int student_id, @Query("package_id") int package_id);

        @PATCH("/api/v1/students/{student_id}/payments/{payment_id}/reconcile")
        Call<PaymentBody> ReconcilePayment(@Path("student_id") int student_id, @Path("payment_id") int payment_id, @Body ReconcileBody reconcileBody);

    }

}
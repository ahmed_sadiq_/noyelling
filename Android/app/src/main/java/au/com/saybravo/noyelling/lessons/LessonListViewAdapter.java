package au.com.saybravo.noyelling.lessons;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.app.Helpers.DateHelper;
import au.com.saybravo.noyelling.app.Helpers.StringHelper;
import au.com.saybravo.noyelling.app.util.AppConstants;
import au.com.saybravo.noyelling.lessons.Domain.LessonListViewItem;
import au.com.saybravo.noyelling.lessons.Domain.StudentHistory;

public class LessonListViewAdapter extends ArrayAdapter<LessonListViewItem> {

    private Context callingActivity;
    private int layoutResourceId;
    private List<Integer> currentlyChecked = new ArrayList<>();

    public LessonListViewAdapter(Context context, int resource) {
        super(context, resource);

        callingActivity = context;
        layoutResourceId = resource;
    }

    public void populateItems(List<StudentHistory> history) {

        clear();
        currentlyChecked = new ArrayList<>();

        ArrayList<LessonListViewItem> items = new ArrayList<LessonListViewItem>();

        for (int i = 0; i < history.size(); i++) {

            StudentHistory historyItem = history.get(i);

            double hours = (double) historyItem.Package.Minutes / (double)60;
            String subText = String.format("%s hour%s, $%s %s",
                    StringHelper.FormatCurrency(hours), hours != 1 ? "s" : "",
                    historyItem.Package.SalePrice,
                    historyItem.paymentType.equals(AppConstants.PAYMENT_TYPE.CASH.getValue())
                            ?AppConstants.PAYMENT_TYPE.CASH.getValue()
                            :AppConstants.PAYMENT_TYPE.EFT.getValue());
            LessonListViewItem header = new LessonListViewItem(LessonListViewItem.ItemType.Header, DateHelper.FormatDate(historyItem.CreatedAt), subText, null, null, -1);

            items.add(header);

            for (int j = 0; j < historyItem.Package.Items.length; j++) {

                StudentHistory.Package.Item item = historyItem.Package.Items[j];

                LessonListViewItem listItem;

                if (item.Claim != null)
                {
                    subText = String.format("%s, $%s", DateHelper.FormatDate(item.Claim.CreatedAt), StringHelper.FormatCurrency(item.SalePrice));
                    listItem = new LessonListViewItem(LessonListViewItem.ItemType.Claimed, item.ProductName, subText, historyItem.Id, item.PackageDetailId, j);
                    listItem.ClaimId = item.Claim.Id;
                    listItem.ClaimType = item.Claim.type;
                }
                else
                {
                    listItem = new LessonListViewItem(LessonListViewItem.ItemType.Checkbox, item.ProductName, "$" + item.SalePrice, historyItem.Id, item.PackageDetailId, j);
                }

                items.add(listItem);
            }
        }

        addAll(items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            final LessonListViewItem listViewItem = getItem(position);
            View view;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                view = inflater.inflate(layoutResourceId, null);

            } else {
                view = convertView;
            }

            RelativeLayout headerContainer = (RelativeLayout) view.findViewById(R.id.Header);
            RelativeLayout checkboxContainer = (RelativeLayout) view.findViewById(R.id.Checkbox);
            RelativeLayout claimedContainer = (RelativeLayout) view.findViewById(R.id.Claimed);

            headerContainer.setVisibility(View.GONE);
            checkboxContainer.setVisibility(View.GONE);
            claimedContainer.setVisibility(View.GONE);

            switch (listViewItem.Type)
            {
                case Header:
                    headerContainer.setVisibility(View.VISIBLE);
                    ((TextView) headerContainer.findViewById(R.id.DateTextView)).setText(listViewItem.MainText);
                    ((TextView) headerContainer.findViewById(R.id.HoursPriceTextView)).setText(listViewItem.SubText);
                    break;

                case Checkbox:
                    checkboxContainer.setVisibility(View.VISIBLE);
                    ((TextView) checkboxContainer.findViewById(R.id.CheckboxTitleTextView)).setText(listViewItem.MainText);
                    ((TextView) checkboxContainer.findViewById(R.id.PriceTextView)).setText(listViewItem.SubText);
                    ((CheckBox) checkboxContainer.findViewById(R.id.LessonCheckbox)).setChecked(currentlyChecked.indexOf(listViewItem.OriginalPosition) > -1);

                    setupCheckboxClickHandler(listViewItem.PaymentId, listViewItem.PackageDetailId, checkboxContainer, listViewItem.OriginalPosition);

                    break;

                case Claimed:
                    claimedContainer.setVisibility(View.VISIBLE);
                    ((TextView) claimedContainer.findViewById(R.id.ClaimedTitleTextView)).setText(listViewItem.MainText);
                    ((TextView) claimedContainer.findViewById(R.id.DatePriceTextView)).setText(listViewItem.SubText);

                    claimedContainer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            sendClaimedClickToActivity(listViewItem.ClaimId, listViewItem.ClaimType);
                        }
                    });

                    break;
            }

            return view;

        } catch (Exception ex) {
            Log.e("LessonListViewAdapter", "error", ex);
            return null;
        }
    }

    private void setupCheckboxClickHandler(final int paymentId, final int packageDetailId, RelativeLayout checkboxContainer, final Integer originalPosition) {

        final CheckBox checkBox = (CheckBox) checkboxContainer.findViewById(R.id.LessonCheckbox);

        checkboxContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkBox.setChecked(!checkBox.isChecked());

                sendCheckboxClickToActivity(paymentId, packageDetailId, checkBox.isChecked(), originalPosition);
            }
         });

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sendCheckboxClickToActivity(paymentId, packageDetailId, checkBox.isChecked(), originalPosition);
            }
        });
    }

    private void sendCheckboxClickToActivity(int paymentId, int packageDetailId, boolean checked, Integer originalPosition)
    {
        Integer index = currentlyChecked.indexOf(originalPosition);
        if(index > -1)
        {
            currentlyChecked.remove(index);
        }else
        {
            currentlyChecked.add(originalPosition);
        }

        ((LessonsActivity) callingActivity).CheckboxClickHandler(paymentId, packageDetailId, checked);
    }

    private void sendClaimedClickToActivity(int claimId, String claimType)
    {
        ((LessonsActivity) callingActivity).ClaimedClickHandler(claimId,claimType);
    }
}

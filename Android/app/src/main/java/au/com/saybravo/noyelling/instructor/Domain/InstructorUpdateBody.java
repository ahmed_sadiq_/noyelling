package au.com.saybravo.noyelling.instructor.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InstructorUpdateBody {

    @SerializedName("instructor")
    @Expose
    public InstructorUpdate instructor;

    public InstructorUpdateBody(InstructorUpdate instructor)
    {
        this.instructor = instructor;
    }
}


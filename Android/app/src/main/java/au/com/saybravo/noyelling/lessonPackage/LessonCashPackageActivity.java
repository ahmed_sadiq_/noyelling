package au.com.saybravo.noyelling.lessonPackage;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.app.BaseActivity;
import au.com.saybravo.noyelling.app.ProgressButton.ProgressButton;
import au.com.saybravo.noyelling.app.ProgressButton.ProgressButtonCallback;
import au.com.saybravo.noyelling.app.Services.JWTRefreshService;
import au.com.saybravo.noyelling.app.Services.ServiceGenerator;
import au.com.saybravo.noyelling.app.util.AppConstants;
import au.com.saybravo.noyelling.lessonPackage.Domain.LessonPackageItem;
import au.com.saybravo.noyelling.lessonPackage.Domain.PackageDetailsBody;
import au.com.saybravo.noyelling.lessonPackage.Domain.ProductsBody;
import au.com.saybravo.noyelling.payment.Domain.CashPackage;
import au.com.saybravo.noyelling.payment.Domain.CashPackageBody;
import au.com.saybravo.noyelling.payment.Domain.PaymentBody;
import au.com.saybravo.noyelling.payment.PaymentViaCashActivity;
import au.com.saybravo.noyelling.student.Domain.Student;
import au.com.saybravo.noyelling.student.StudentActivity;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LessonCashPackageActivity extends BaseActivity {

    private boolean loading;
    private Student student;
    private ProgressBar progressBar;
    private ListView lessonPackageListView;
    LessonCashPackageAdapter adapter;
    private View footerView;
    private ProgressButton submitButton;
    private Boolean isLoading;
    private TextView totalItemName;
    private TextView totalItemValue;
    CashPackage selectedPackage = null;

    public LessonCashPackageActivity() {
        super(R.layout.activity_cash_package, R.string.LessonCashPackage_HeaderText);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            student = (Student) bundle.getSerializable(AppConstants.INTENT_KEY_STUDENT);
        }
        init();
        getPackageDetails();
    }

    private void init()
    {
        lessonPackageListView = (ListView) findViewById(R.id.LessonPackageListView);
        progressBar = (ProgressBar) findViewById(R.id.LoadingProgressBar);
        footerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.cash_package_layout_footer, null, false);
        submitButton = new ProgressButton(footerView, R.id.CreatePackageButton, R.string.LessonPackage_CreateBtnText, R.string.LessonPackage_CreateBtnUploadingText, isLoading != null && isLoading.booleanValue(), new ProgressButtonCallback() {
            @Override
            public void callback() {
                if((isLoading == null || isLoading == false) && selectedPackage != null) {
                    isLoading = true;
                    createPackage(student.Id, selectedPackage.getId());
                }
            }
        });
        totalItemName = (TextView) footerView.findViewById(R.id.TotalItemName);
        totalItemValue = (TextView) footerView.findViewById(R.id.TotalItemValue);

    }


    private void createPackage(int studentId, int packageId){

        ServiceGenerator.ProductService productService = ServiceGenerator.createService(ServiceGenerator.ProductService.class, this.getApplicationContext() );
        Call<PaymentBody>call = productService.CreateUnpaidCashPayment(studentId, packageId);

        JWTRefreshService.enqueueSafe(call, LessonCashPackageActivity.this, new Callback<PaymentBody>() {
            @Override
            public void onResponse(Call<PaymentBody> call, Response<PaymentBody> response) {
                if(response.isSuccessful()){
                    PaymentBody paymentBody = response.body();
                    if(paymentBody != null) {
                        Intent intent = new Intent(LessonCashPackageActivity.this, PaymentViaCashActivity.class);
                        intent.putExtra(AppConstants.INTENT_KEY_STUDENT, student);
                        intent.putExtra(AppConstants.INTENT_KEY_PAYMENT, paymentBody);
                        startActivity(intent);
                        finish();
                    }
                }
                isLoading = false;
            }
            @Override
            public void onFailure(Call<PaymentBody> call, Throwable t) {
                isLoading = false;
            }
        });
    }
    private void getPackageDetails()
    {
        showLoading(true);
        ServiceGenerator.ProductService productService = ServiceGenerator.createService(ServiceGenerator.ProductService.class, getApplicationContext());
        Call<CashPackageBody> call = productService.GetCashPackages();

        JWTRefreshService.enqueueSafe(call, this, new Callback<CashPackageBody>() {
            @Override
            public void onResponse(Call<CashPackageBody> call, Response<CashPackageBody> response) {

                if(response.isSuccessful()){
                    CashPackageBody cashPackages = (CashPackageBody) response.body();
                    populatePackagesToUI(cashPackages.getPackages()); //dumy radio buttons
                }
                showLoading(false);
            }

            @Override
            public void onFailure(Call<CashPackageBody> call, Throwable t) {
                showLoading(false);
            }
        });

    }

    private void showLoading(boolean loading){
        this.loading = loading;
        if(loading){
            progressBar.setVisibility(View.VISIBLE);

        }else{
            progressBar.setVisibility(View.GONE);
        }
    }

    public void populatePackagesToUI(List<CashPackage> cashPackages) {

        adapter = new LessonCashPackageAdapter(this, R.layout.custom_radio_item, cashPackages);
        lessonPackageListView.setAdapter(adapter);
        lessonPackageListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        lessonPackageListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position < adapter.getCount()) {
                    adapter.setSelectedIndex(position);  // set selected position and notify the adapter
                    adapter.notifyDataSetChanged();
                    selectedPackage = adapter.getItem(position);
                    updateFooter(selectedPackage);
                }
            }
        });

    }

    public void updateFooter(CashPackage dataObject) {
        boolean footerAdded = lessonPackageListView.getFooterViewsCount() > 0? true: false;
        if(!footerAdded) {
            lessonPackageListView.addFooterView(footerView);
        }
        totalItemValue.setText("$"+dataObject.getSalePrice());
        totalItemName.setText(" x "+dataObject.getName());
    }
}

package au.com.saybravo.noyelling.instructor.Domain;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class Instructor implements Serializable {

    @SerializedName("id")
    @Expose
    public Integer id;

    @SerializedName("transmission")
    @Expose
    public String transmission;

    @SerializedName("gender")
    @Expose
    public String gender;

    @SerializedName("fname")
    @Expose
    public String fname;
    @SerializedName("lname")
    @Expose
    public String lname;
    @SerializedName("avatar")
    @Expose
    public Avatar avatar;
    @SerializedName("mobile_number")
    @Expose
    public String mobileNumber;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("username")
    @Expose
    public String username;
    @SerializedName("slug")
    @Expose
    public String slug;
    @SerializedName("about")
    @Expose
    public String about;
    @SerializedName("available")
    @Expose
    public Boolean available;
    @SerializedName("access_locked")
    @Expose
    public Boolean accessLocked;

    @SerializedName("car_info")
    @Expose
    public String carInfo;

    @SerializedName("car_rego")
    @Expose
    public String carRego;

    @SerializedName("bsb")
    @Expose
    public String bsb;
    @SerializedName("account_number")
    @Expose
    public String accountNumber;
    @SerializedName("gst_registered")
    @Expose
    public Boolean gstRegistered;
    @SerializedName("keys_to_drive")
    @Expose
    public Boolean keysToDrive;
}

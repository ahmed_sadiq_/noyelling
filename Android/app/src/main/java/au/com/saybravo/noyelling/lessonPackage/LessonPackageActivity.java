package au.com.saybravo.noyelling.lessonPackage;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.util.TextUtils;

import java.util.ArrayList;
import java.util.List;

import au.com.saybravo.noyelling.BuildConfig;
import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.app.BaseActivity;
import au.com.saybravo.noyelling.app.Services.JWTRefreshService;
import au.com.saybravo.noyelling.app.Services.ServiceGenerator;
import au.com.saybravo.noyelling.app.util.AppConstants;
import au.com.saybravo.noyelling.app.util.ValidationUtil;
import au.com.saybravo.noyelling.lessonPackage.Domain.Item;
import au.com.saybravo.noyelling.lessonPackage.Domain.LessonPackageItem;
import au.com.saybravo.noyelling.lessonPackage.Domain.Product;
import au.com.saybravo.noyelling.lessonPackage.Domain.ProductsBody;
import au.com.saybravo.noyelling.payment.Domain.PaymentBody;
import au.com.saybravo.noyelling.payment.PaymentActivity;
import au.com.saybravo.noyelling.payment.PaymentViaCardActivity;
import au.com.saybravo.noyelling.payment.PaymentViaLinkActivity;
import au.com.saybravo.noyelling.student.Domain.EmailBody;
import au.com.saybravo.noyelling.student.Domain.Student;
import au.com.saybravo.noyelling.student.Domain.StudentCard;
import au.com.saybravo.noyelling.student.Domain.StudentCardBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ahmed.sadiq on 26/05/16.
 */
public class LessonPackageActivity extends BaseActivity {


    private ListView lessonListView;
    private ProgressBar progressBar;
    private LessonPackageListviewAdapter lessonPackageListviewAdapter;
    private boolean loading;
    private Student student;
    private int paymentMethod;
    private boolean hasMovedToPayment = false;


    public LessonPackageActivity() {
        super(R.layout.activity_lesson_package, R.string.LessonPackage_HeaderText);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            student = (Student) bundle.getSerializable(AppConstants.INTENT_KEY_STUDENT);
            paymentMethod = bundle.getInt(AppConstants.INTENT_KEY_PAYMENT_METHOD);
        }
        bindLessonPackageToView();
        getPackageDetails();
    }

    @Override
    public void onRestart() {
        super.onRestart();
        if(hasMovedToPayment) {
            finish();
        }

    }

    public void setMovedToPayment(){
        hasMovedToPayment = true;
    }

    private void bindLessonPackageToView()
    {
        lessonListView = (ListView) findViewById(R.id.LessonPackageListView);
        progressBar = (ProgressBar) findViewById(R.id.LoadingProgressBar);
        lessonListView.setDivider(null);
        lessonPackageListviewAdapter = new LessonPackageListviewAdapter(this, R.layout.layout_lesson_package_listview, student.Id);
        lessonListView.setAdapter(lessonPackageListviewAdapter);

    }

    private void getPackageDetails()
    {

        showLoading(true);
        ServiceGenerator.ProductService productService = ServiceGenerator.createService(ServiceGenerator.ProductService.class, getApplicationContext());

        Call<ProductsBody> call = productService.GetProducts(GetCurrentInstructorId());

        JWTRefreshService.enqueueSafe(call, this, new Callback<ProductsBody>() {
            @Override
            public void onResponse(Call<ProductsBody> call, Response<ProductsBody> response) {

                if(response.isSuccessful()){
                    List<Product> productList = response.body().getProducts();
                    addConvertedProducts(productList);

                }

                showLoading(false);
            }

            @Override
            public void onFailure(Call<ProductsBody> call, Throwable t) {
                showLoading(false);
            }
        });

    }

    private void addConvertedProducts(List<Product> productList){

        lessonPackageListviewAdapter.clear();

        if(productList == null){
            return;
        }

        List<LessonPackageItem> packageItems = new ArrayList<LessonPackageItem>();

        for(Product product: productList){

            LessonPackageItem header = new LessonPackageItem();

            header.IsHeader = true;
            header.HeaderName = product.getName();

            packageItems.add(header);
            List<Item> items = product.getItems();

            if(items != null){

                for(Item item: items){
                    LessonPackageItem lesson = new LessonPackageItem();
                    lesson.ItemName = item.getName();
                    lesson.ItemPrice = item.getBaseRate();
                    lesson.Id = item.getId();

                    packageItems.add(lesson);
                }

            }

        }

        lessonPackageListviewAdapter.setAvailableLessonPackages(packageItems);

    }

    private void showLoading(boolean loading){
        this.loading = loading;
        if(loading){
            progressBar.setVisibility(View.VISIBLE);

        }else{
            progressBar.setVisibility(View.GONE);
        }
    }

    public void processPayment(final PaymentBody paymentBody) {
        if(paymentMethod == AppConstants.PAYMENT_METHOD.TAP_AND_GO.getValue()) {
            setMovedToPayment();
            Intent intent = new Intent(LessonPackageActivity.this, PaymentActivity.class);
            intent.putExtra("PaymentInformation", paymentBody.getPayment());
            startActivity(intent);

        }
        else if(paymentMethod == AppConstants.PAYMENT_METHOD.CREDIT_CARD.getValue()) {
            lessonPackageListviewAdapter.updateProgressButton(false, getString(R.string.LessonPackage_CreateBtnText));
            Intent intent = new Intent(LessonPackageActivity.this, PaymentViaCardActivity.class);
            intent.putExtra(AppConstants.INTENT_KEY_PAYMENT, paymentBody);
            intent.putExtra(AppConstants.INTENT_KEY_STUDENT, student);
            startActivity(intent);
            finish();
        }
        else if(paymentMethod == AppConstants.PAYMENT_METHOD.SEND_LINK.getValue() ) {
            lessonPackageListviewAdapter.updateProgressButton(false, getString(R.string.LessonPackage_CreateBtnText));
            Intent intent = new Intent(LessonPackageActivity.this, PaymentViaLinkActivity.class);
            intent.putExtra(AppConstants.INTENT_KEY_PAYMENT, paymentBody);
            intent.putExtra(AppConstants.INTENT_KEY_STUDENT, student);
            startActivity(intent);
            finish();
        } else if(paymentMethod == AppConstants.PAYMENT_METHOD.CASH.getValue()) {
            Toast.makeText(LessonPackageActivity.this, "Feature Not Available.", Toast.LENGTH_LONG).show();
        }
    }


}

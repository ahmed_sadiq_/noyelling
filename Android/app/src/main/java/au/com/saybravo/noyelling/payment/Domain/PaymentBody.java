package au.com.saybravo.noyelling.payment.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ahmed.sadiq on 31/05/16.
 */
public class PaymentBody implements Serializable {

    @SerializedName("payment")
    @Expose
    private Payment payment;

    /**
     *
     * @return
     * The payment
     */
    public Payment getPayment() {
        return payment;
    }

    /**
     *
     * @param payment
     * The payment
     */
    public void setPayment(Payment payment) {
        this.payment = payment;
    }

}
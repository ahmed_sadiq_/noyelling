package au.com.saybravo.noyelling.student.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StudentUpdate
{
    @SerializedName("mobile_number")
    @Expose
    public String mobileNumber;

    @SerializedName("transmission")
    @Expose
    public String transmission;

    @SerializedName("address_attributes")
    @Expose
    public AddressAttributes AddressAttributes;
}


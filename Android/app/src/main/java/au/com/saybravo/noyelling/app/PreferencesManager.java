package au.com.saybravo.noyelling.app;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import au.com.saybravo.noyelling.instructor.Domain.Instructor;

/**
 * Created by ahmed.sadiq on 9/04/2016.
 */
public class PreferencesManager {

    private static final String PREF_NAME = "au.com.saybravo.noyelling.PREF_NAME";
    private static final String Token_VALUE = "au.com.saybravo.noyelling.Token_VALUE";
    private static final String Password_VALUE = "au.com.saybravo.noyelling.Pass_VALUE";
    private static final String Username_VALUE = "au.com.saybravo.noyelling.User_VALUE";
    private static final String Instructor_VALUE = "au.com.saybravo.noyelling.Instructor";

    private static PreferencesManager sInstance;
    private final SharedPreferences mPref;

    private PreferencesManager(Context context) {
        mPref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static synchronized PreferencesManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new PreferencesManager(context);
        }
        return sInstance;
    }

    public void setTokenValue(String value) {
        mPref.edit()
                .putString(Token_VALUE, value)
                .commit();
    }

    public String getTokenValue() {
        return mPref.getString(Token_VALUE, "");
    }

    public void setPasswordValue(String value){
        mPref.edit().putString(Password_VALUE, value).commit();
    }

    public String getPasswordValue(){
        return mPref.getString(Password_VALUE,"");
    }

    public void setUsernameValue(String value){
        mPref.edit().putString(Username_VALUE, value).commit();
    }

    public String getUsernameValue(){
        return mPref.getString(Username_VALUE,"");
    }


    public void remove(String key) {
        mPref.edit()
                .remove(key)
                .commit();
    }

    public void setInstructor(Instructor instructor){

        if(instructor == null){
            remove(Instructor_VALUE);
        }else {
            Gson gson = new Gson();
            mPref.edit().putString(Instructor_VALUE, gson.toJson(instructor)).commit();
        }

    }

    public Instructor getInstructor(){
        String instructorString = mPref.getString(Instructor_VALUE, null);
        if(instructorString != null){
            Instructor inst = new Gson().fromJson(instructorString, Instructor.class);
            return inst;
        }

        return null;
    }

    public boolean clear() {
        return mPref.edit()
                .clear()
                .commit();
    }
}

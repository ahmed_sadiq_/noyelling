package au.com.saybravo.noyelling.lessonPackage.Domain;

/**
 * Created by ahmed.sadiq on 26/05/16.
 */
public class LessonPackageItem {

    public boolean IsTotal;
    public boolean IsSummary;

    public boolean IsSubmitButton;
    public boolean IsHeader;


    public Integer Id;
    public String HeaderName;
    public String ItemName;
    public double ItemPrice;
    public int ItemCount;

}

package au.com.saybravo.noyelling.instructor.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmed.sadiq on 19/04/2016.
 */
public class ClaimsBody {

    @SerializedName("week")
    @Expose
    private ClaimsWeek week;
    @SerializedName("claims")
    @Expose
    private Claims claims;

    /**
     *
     * @return
     * The week
     */
    public ClaimsWeek getWeek() {
        return week;
    }

    /**
     *
     * @param week
     * The week
     */
    public void setWeek(ClaimsWeek week) {
        this.week = week;
    }

    /**
     *
     * @return
     * The claims
     */
    public Claims getClaims() {
        return claims;
    }

    /**
     *
     * @param claims
     * The claims
     */
    public void setClaims(Claims claims) {
        this.claims = claims;
    }
}

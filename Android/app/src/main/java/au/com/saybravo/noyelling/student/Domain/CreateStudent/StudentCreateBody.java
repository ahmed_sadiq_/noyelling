package au.com.saybravo.noyelling.student.Domain.CreateStudent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmed.sadiq on 31/05/16.
 */
public class StudentCreateBody {

    @SerializedName("student")
    @Expose
    private Student student;

    /**
     *
     * @return
     * The student
     */
    public Student getStudent() {
        return student;
    }

    /**
     *
     * @param student
     * The student
     */
    public void setStudent(Student student) {
        this.student = student;
    }

}

package au.com.saybravo.noyelling.payment.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmed.sadiq on 1/06/16.
 */
public class Transaction {


    @SerializedName("transaction_id")
    @Expose
    private String transactionId;
    @SerializedName("payment_type")
    @Expose
    private String paymentType;

    /**
     *
     * @return
     * The transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     *
     * @param transactionId
     * The transaction_id
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    /**
     *
     * @return
     * The paymentType
     */
    public String getPaymentType() {
        return paymentType;
    }

    /**
     *
     * @param paymentType
     * The payment_type
     */
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

}

package au.com.saybravo.noyelling.student.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ahmed.sadiq on 18/04/2016.
 */
public class Customer implements Serializable {

    @SerializedName("id")
    @Expose
    public Integer Id;

    @SerializedName("email")
    @Expose
    public String Email;

    public Customer(String email)
    {
        Email = email;
    }
}
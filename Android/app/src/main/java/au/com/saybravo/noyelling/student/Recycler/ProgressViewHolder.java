package au.com.saybravo.noyelling.student.Recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import au.com.saybravo.noyelling.R;

/**
 * Created by ahmed.sadiq on 19/04/16.
 */
public class ProgressViewHolder extends RecyclerView.ViewHolder {

    public ProgressBar progressBar;

    public ProgressViewHolder(View itemView) {
        super(itemView);

        progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);

    }
}

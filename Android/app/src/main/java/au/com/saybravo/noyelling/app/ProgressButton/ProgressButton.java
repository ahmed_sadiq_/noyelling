package au.com.saybravo.noyelling.app.ProgressButton;

import android.app.Activity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import au.com.saybravo.noyelling.R;

/**
 * Created by ahmed.sadiq on 28/04/2016.
 */
public class ProgressButton {

    private RelativeLayout relativeLayout;

    private String defaultText;
    private String loadingText;

    public boolean isLoading = false;

    TextView buttonText;
    ProgressBar progressBar;

    public ProgressButton(View view, int id,int defaultTextResId, int loadingTextResId, boolean isLoading,  final ProgressButtonCallback onClickCallback){

        relativeLayout = (RelativeLayout) view.findViewById(id);
        final String defaultText = view.getResources().getText(defaultTextResId).toString();
        final String loadingText = view.getResources().getText(loadingTextResId).toString();

        buttonText = (TextView) relativeLayout.findViewById(R.id.ButtonText);
        progressBar = (ProgressBar) relativeLayout.findViewById(R.id.ButtonLoader);
        this.defaultText = defaultText;
        this.loadingText = loadingText;
        this.isLoading = isLoading;

        if(isLoading){

            buttonText.setText(loadingText);
            showLoading();
        }else{
            buttonText.setText(defaultText);
        }
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!buttonText.getText().equals(loadingText))
                {
                    showLoading();
                    onClickCallback.callback();
                }
            }
        });

    }


    public ProgressButton(Activity activity, int id, int defaultTextResId, int loadingTextResId, final ProgressButtonCallback onClickCallback) {

        relativeLayout = (RelativeLayout) activity.findViewById(id);

        final String defaultText = activity.getResources().getText(defaultTextResId).toString();
        final String loadingText = activity.getResources().getText(loadingTextResId).toString();


        buttonText = (TextView) relativeLayout.findViewById(R.id.ButtonText);
        progressBar = (ProgressBar) relativeLayout.findViewById(R.id.ButtonLoader);

        this.defaultText = defaultText;
        this.loadingText = loadingText;

        buttonText.setText(defaultText);

        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!buttonText.getText().equals(loadingText))
                {
                    showLoading();
                    onClickCallback.callback();
                }
            }
        });
    }

    private void showLoading()
    {
        isLoading = true;

        buttonText.setText(loadingText);
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideLoading()
    {
        isLoading = false;

        buttonText.setText(defaultText);
        progressBar.setVisibility(View.GONE);
    }

    public void setLoadingStatus(boolean status) {
        if(status) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    public void setVisibility(int visibility) {

        relativeLayout.setVisibility(visibility);
    }

    public void setText(String text)
    {
        buttonText.setText(text);
    }
}

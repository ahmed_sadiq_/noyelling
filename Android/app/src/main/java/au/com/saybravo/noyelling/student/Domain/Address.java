package au.com.saybravo.noyelling.student.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ahmed.sadiq on 18/04/2016.
 */
public class Address implements Serializable {

    @SerializedName("id")
    @Expose
    public Integer Id;

    @SerializedName("latitude")
    @Expose
    public String Latitude;

    @SerializedName("longitude")
    @Expose
    public String Longitude;

    @SerializedName("full_address")
    @Expose
    public String FullAddress;

    @SerializedName("suburb")
    @Expose
    public String Suburb;
}

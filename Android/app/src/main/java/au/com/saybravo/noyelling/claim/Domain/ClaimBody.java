package au.com.saybravo.noyelling.claim.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
/**
 * Created by ahmed.sadiq on 26/04/16.
 */
public class ClaimBody {

    @SerializedName("claim")
    @Expose
    private Claim claim;

    /**
     *
     * @return
     * The claim
     */
    public Claim getClaim() {
        return claim;
    }

    /**
     *
     * @param claim
     * The claim
     */
    public void setClaim(Claim claim) {
        this.claim = claim;
    }

}

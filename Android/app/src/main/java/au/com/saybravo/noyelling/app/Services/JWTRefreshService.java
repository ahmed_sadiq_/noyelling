package au.com.saybravo.noyelling.app.Services;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import au.com.saybravo.noyelling.app.Domain.Auth;
import au.com.saybravo.noyelling.app.Domain.AuthBody;
import au.com.saybravo.noyelling.app.Domain.JWToken;
import au.com.saybravo.noyelling.app.NoYellingApplication;
import au.com.saybravo.noyelling.app.PreferencesManager;
import au.com.saybravo.noyelling.login.LoginActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ahmed.sadiq on 28/04/16.
 */
public class JWTRefreshService {


    public static void gotoLoginActivity(Activity activity)
    {
        PreferencesManager.getInstance(activity.getApplicationContext()).clear();
        JWTokenService.setToken(null, activity.getApplicationContext());


        Intent intent = new Intent(activity, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
        activity.finish();

    }

    // Retry  mechanism

    public static <T> void enqueueSafe(Call<T> call,  Activity activity, final Callback<T> callBack)
    {
        final Call<T> copyCall = call.clone();
        final Activity appActivity = activity;

        call.enqueue(
                new Callback<T>() {
                    @Override
                    public void onResponse(Call<T> call, Response<T> response) {

                        if(response.isSuccessful() == false && response.raw().code() == 401){

                            ServiceGenerator.LoginService loginService = ServiceGenerator.createService(ServiceGenerator.LoginService.class, appActivity.getApplicationContext(), true);

                            AuthBody authBody = new AuthBody();
                            authBody.auth = new Auth();
                            authBody.auth.email = PasswordService.getUserName(appActivity.getApplicationContext());
                            authBody.auth.password = PasswordService.getPassword(appActivity.getApplicationContext());

                            if(!NoYellingApplication.IsLive)
                            {
                                Log.i("Refresh Login", authBody.auth.email+" :>>>> "+ authBody.auth.password);
                            }

                            Call<JWToken> loginCall = loginService.Basiclogin(authBody);

                            loginCall.enqueue(new Callback<JWToken>() {
                                @Override
                                public void onResponse(Call<JWToken> call, Response<JWToken> response) {
                                    if(response.isSuccessful() && response.body() != null){

                                        JWToken result = response.body();
                                        JWTokenService.setToken(result.jwt, appActivity.getApplicationContext());
                                        copyCall.enqueue(callBack);

                                    }else{
                                        if(!NoYellingApplication.IsLive)
                                        {
                                            Log.i("Refresh Login","Failed with "+response.raw().code());
                                        }
                                        gotoLoginActivity(appActivity);

                                    }

                                }

                                @Override
                                public void onFailure(Call<JWToken> call, Throwable t) {
                                    gotoLoginActivity(appActivity);
                                }
                            });

                        }else {

                            callBack.onResponse(call, response);
                        }
                    }

                    @Override
                    public void onFailure(Call<T> call, Throwable t) {

                        callBack.onFailure(call, t);
                    }
                }
        );
    }
}

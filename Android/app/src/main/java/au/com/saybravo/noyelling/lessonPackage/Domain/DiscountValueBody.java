package au.com.saybravo.noyelling.lessonPackage.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmed.sadiq on 31/05/16.
 */
public class DiscountValueBody {

    @SerializedName("discounted_price")
    @Expose
    private Double discount;


    public Double getDicount(){ return discount; }

}

package au.com.saybravo.noyelling.instructor.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmed.sadiq on 12/05/16.
 */
public class Total {

    @SerializedName("earnings")
    @Expose
    private Double earnings;
    @SerializedName("issues")
    @Expose
    private Integer issues;
    @SerializedName("size")
    @Expose
    private Integer size;

    /**
     *
     * @return
     * The earnings
     */
    public Double getEarnings() {
        return earnings;
    }

    /**
     *
     * @param earnings
     * The earnings
     */
    public void setEarnings(Double earnings) {
        this.earnings = earnings;
    }

    /**
     *
     * @return
     * The issues
     */
    public Integer getIssues() {
        return issues;
    }

    /**
     *
     * @param issues
     * The issues
     */
    public void setIssues(Integer issues) {
        this.issues = issues;
    }

    /**
     *
     * @return
     * The size
     */
    public Integer getSize() {
        return size;
    }

    /**
     *
     * @param size
     * The size
     */
    public void setSize(Integer size) {
        this.size = size;
    }

}
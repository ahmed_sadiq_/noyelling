package au.com.saybravo.noyelling.payment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.app.BaseActivity;
import au.com.saybravo.noyelling.app.ProgressButton.ProgressButton;
import au.com.saybravo.noyelling.app.ProgressButton.ProgressButtonCallback;
import au.com.saybravo.noyelling.app.util.AppConstants;
import au.com.saybravo.noyelling.payment.Domain.PaymentBody;
import au.com.saybravo.noyelling.student.Domain.Student;
import au.com.saybravo.noyelling.student.StudentActivity;

public class PaymentSuccessfulActivity extends BaseActivity {

    Button actionButton;
    private Student student;

    public PaymentSuccessfulActivity() {
        super(R.layout.activity_payment_successful);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            student = (Student) bundle.getSerializable(AppConstants.INTENT_KEY_STUDENT);
        }
        init();
    }

    private void init() {
        actionButton = (Button) findViewById(R.id.ActionButton);
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }
}

package au.com.saybravo.noyelling.payment.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahmed.sadiq on 10/12/2016.
 */

public class CashPackageBody {

    @SerializedName("packages")
    @Expose
    private List<CashPackage> packages = new ArrayList<CashPackage>();

    public List<CashPackage> getPackages() {
        return packages;
    }

    public void setPackages(List<CashPackage> packages) {
        this.packages = packages;
    }
}

package au.com.saybravo.noyelling.instructor.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmed.sadiq on 11/04/16.
 */
public class ClaimsStatistics {

    @SerializedName("total")
    @Expose
    private Total total;
    @SerializedName("pending")
    @Expose
    private Pending pending;
    @SerializedName("paid")
    @Expose
    private Paid paid;
    @SerializedName("unreconciled")
    @Expose
    private Unreconciled unreconciled;

    /**
     *
     * @return
     * The total
     */
    public Total getTotal() {
        return total;
    }

    /**
     *
     * @param total
     * The total
     */
    public void setTotal(Total total) {
        this.total = total;
    }

    /**
     *
     * @return
     * The pending
     */
    public Pending getPending() {
        return pending;
    }

    /**
     *
     * @param pending
     * The pending
     */
    public void setPending(Pending pending) {
        this.pending = pending;
    }

    /**
     *
     * @return
     * The paid
     */
    public Paid getPaid() {
        return paid;
    }

    /**
     *
     * @param paid
     * The paid
     */
    public void setPaid(Paid paid) {
        this.paid = paid;
    }

    /**
     *
     * @return
     * The unreconciled
     */
    public Unreconciled getUnreconciled() {
        return unreconciled;
    }

    /**
     *
     * @param unreconciled
     * The unreconciled
     */
    public void setUnreconciled(Unreconciled unreconciled) {
        this.unreconciled = unreconciled;
    }

}
package au.com.saybravo.noyelling.app.Services;

import android.content.Context;
import android.util.Base64;

import au.com.saybravo.noyelling.app.PreferencesManager;

/**
 * Created by ahmed.sadiq on 27/04/16.
 */
public class PasswordService {

    public static String getPassword(Context context){

        String passWord =  PreferencesManager.getInstance(context).getPasswordValue();
        byte[] decodedBase64 = Base64.decode(passWord, Base64.NO_WRAP);
        byte[] decodedXor =  processString(decodedBase64);

        return new String(decodedXor);
    }

    public static String getUserName(Context context){

        return PreferencesManager.getInstance(context).getUsernameValue();
    }

    public static void setUserName(String userName, Context context){

        PreferencesManager.getInstance(context).setUsernameValue(userName);
    }

    public static void setPassword(String password, Context context){

        byte[] encoded = processString(password.getBytes());
        if(encoded != null) {
            String base64encoded = Base64.encodeToString(encoded, Base64.NO_WRAP);

            PreferencesManager.getInstance(context).setPasswordValue(base64encoded);
        }
    }

    private static byte[] processString(byte[] st) {

        try {

            String key = "AkWERWds234SDFsdASDasdREwe234FDSdfcVdgyjtwegdwt@#4234SgsgfdgERsdgswrwew#";
            if (st == null) return null;

            byte[] keys = key.getBytes();

            int ml = st.length;
            int kl = keys.length;
            byte[] newmsg = new byte[ml];

            for (int i = 0; i < ml; i++) {
                newmsg[i] = (byte) (st[i] ^ keys[i % kl]);
            }//for i

            keys = null;
            return newmsg;
        } catch (Exception e) {
            return null;
        }
    }

}

package au.com.saybravo.noyelling.payment;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.util.TextUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import au.com.saybravo.noyelling.AppConfig;
import au.com.saybravo.noyelling.BuildConfig;
import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.app.BaseActivity;
import au.com.saybravo.noyelling.app.ProgressButton.ProgressButton;
import au.com.saybravo.noyelling.app.ProgressButton.ProgressButtonCallback;
import au.com.saybravo.noyelling.app.Services.JWTRefreshService;
import au.com.saybravo.noyelling.app.Services.ServiceGenerator;
import au.com.saybravo.noyelling.app.util.AppConstants;
import au.com.saybravo.noyelling.app.widget.SimpleDatePickerDialog;
import au.com.saybravo.noyelling.app.widget.SimpleDatePickerDialogFragment;
import au.com.saybravo.noyelling.payment.Domain.Payment;
import au.com.saybravo.noyelling.payment.Domain.PaymentBody;
import au.com.saybravo.noyelling.student.Domain.Student;
import au.com.saybravo.noyelling.student.Domain.StudentCard;
import au.com.saybravo.noyelling.student.Domain.StudentCardBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentViaCardActivity extends BaseActivity {

    EditText creditCardNumberEditText;
    EditText expiryEditText;
    EditText cvcEditText;
    ProgressButton actionButton;
    Button cancelButton;
    TextView toolBarTitle;
    Boolean isLoading = false;
    String selectedYear = "";
    String selectedMonth = "";

    private Student student;
    private PaymentBody paymentBody;
    private Payment paymentInfo;


    public PaymentViaCardActivity() {
        super(R.layout.activity_payment_via_card);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            student = (Student) bundle.getSerializable(AppConstants.INTENT_KEY_STUDENT);
            paymentBody = (PaymentBody) bundle.getSerializable(AppConstants.INTENT_KEY_PAYMENT);
            student = (Student) bundle.getSerializable(AppConstants.INTENT_KEY_STUDENT);
        }
        init();
    }

    private void init() {
        toolBarTitle = (TextView) findViewById(R.id.toolbar_title);
        cancelButton = (Button) findViewById(R.id.CancelButton);
        View layoutContainer = findViewById(R.id.layoutContainer);

        actionButton = new ProgressButton(layoutContainer, R.id.ActionButton,
                R.string.Payment_Charge_Card,
                R.string.LessonPackage_CreateBtnProcessingText,
                isLoading != null && isLoading.booleanValue(),
                new ProgressButtonCallback() {
                    @Override
                    public void callback() {
                        if (isLoading == null || isLoading == false) {
                            isLoading = true;
                            processCardInfo();
                        }
                    }
                });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(paymentBody != null) {
                    Payment paymentInfo = paymentBody.getPayment();
                    deletePayment(paymentInfo.getStudent().getId(), paymentInfo.getId());
                    onBackPressed();
                }
            }
        });
        creditCardNumberEditText = (EditText) findViewById(R.id.number);
        expiryEditText = (EditText) findViewById(R.id.cardExpiry);
        cvcEditText = (EditText) findViewById(R.id.cvc);

        if(paymentBody != null) {
            toolBarTitle.setText("Total $"+paymentBody.getPayment().getPackage().getSalePrice());
        }
        expiryEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP:
                        displaySimpleDatePickerDialogFragment();
                        return true;
                    default:
                        return false;
                }
            }
        });

        creditCardNumberEditText.addTextChangedListener(creditCardWatcher);
    }

    private void processCardInfo() {
        String creditCardNumber = creditCardNumberEditText.getText().toString();
        creditCardNumber = creditCardNumber.replace("-", "");
        String cvcNumber = cvcEditText.getText().toString();
        String expiryDate = expiryEditText.getText().toString();

        String validityError = checkCreditCardInputsValidity(creditCardNumber, cvcNumber, selectedMonth, selectedYear);
        if(validityError.isEmpty()){
            submitCard(paymentBody, creditCardNumber, selectedMonth, selectedYear, cvcNumber);
        } else {
            Toast.makeText(this, validityError, Toast.LENGTH_LONG).show();
            updateProgressButton(false, getString(R.string.Payment_Charge_Card));
        }
    }
    private String checkCreditCardInputsValidity(String creditCard, String cvcNumber, String month, String year) {
        String errorString = "";
        if(TextUtils.isBlank(creditCard) || creditCard.length() != AppConstants.CREDIT_CARD_NUMBER_LENGTH)
            errorString = getString(R.string.error_credit_card_number_validity);
        else if(TextUtils.isBlank(cvcNumber) || cvcNumber.length() != AppConstants.CREDIT_CARD_CVC_NUMBER_LENGTH)
            errorString = getString(R.string.error_credit_card_cvc_validity);
        else if(TextUtils.isBlank(month))
            errorString = getString(R.string.error_credit_card_month_expiry);
        else if(TextUtils.isBlank(year))
            errorString = getString(R.string.error_credit_card_year_expiry);

        return errorString;
    }

    public void submitCard(final PaymentBody paymentBody,
                           String cardNumber, String month, String year, String cvc) {

        final String publishableApiKey = AppConfig.STRIPE_PUBLISH_KEY;

        Card card = new com.stripe.android.model.Card(cardNumber,
                Integer.valueOf(month),
                Integer.valueOf(year),
                cvc);

        Stripe stripe = new Stripe();
        stripe.createToken(card, publishableApiKey, new TokenCallback() {
            public void onSuccess(Token token) {
                Log.i("Test","Token. + : "+token);
                Log.i("Test","Sending Call to Charge Payment. + paymentID: "+paymentBody.getPayment().getId());
                chargePaymentViaStripToken(token, paymentBody);
            }

            public void onError(Exception error) {
                Toast.makeText(PaymentViaCardActivity.this,error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                updateProgressButton(false, getString(R.string.Payment_Charge_Card));
            }
        });
    }

    public void chargePaymentViaStripToken(Token token, PaymentBody paymentBody) {
        ServiceGenerator.StudentService studentService =
                ServiceGenerator.createService(ServiceGenerator.StudentService.class, this );

        StudentCardBody cardBody = new StudentCardBody(new StudentCard(token.getId()));
        Call<PaymentBody> call = studentService.ProcessCreditCard(student.Id, paymentBody.getPayment().getId(),
                cardBody);

        JWTRefreshService.enqueueSafe(call, this, new Callback<PaymentBody>() {
            @Override
            public void onResponse(Call<PaymentBody> call, Response<PaymentBody> response) {
                if(response.isSuccessful()) {
                    PaymentBody paymentBody = response.body();
                    if(paymentBody != null) {
                        Toast.makeText(PaymentViaCardActivity.this,PaymentViaCardActivity.this.getString(R.string.success_message_credit_card),
                                Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(PaymentViaCardActivity.this, PaymentSuccessfulActivity.class);
                        intent.putExtra(AppConstants.INTENT_KEY_STUDENT, student);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    Toast.makeText(PaymentViaCardActivity.this,response.message()+"/"+response.code(), Toast.LENGTH_LONG).show();
                    updateProgressButton(false, getString(R.string.Payment_Charge_Card));
                }
            }
            @Override
            public void onFailure(Call<PaymentBody> call, Throwable t) {
                Toast.makeText(PaymentViaCardActivity.this,"Something Went Wrong.", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void setDateToView(int year, int month) {
        selectedMonth = String.valueOf(month);
        selectedYear = String.valueOf(year);
        String formattedYear = String.valueOf(year%100);
        String formattedMonth = String.format("%02d", month);
        expiryEditText.setText(formattedMonth+"/"+formattedYear);
    }
    private void displaySimpleDatePickerDialogFragment() {
        SimpleDatePickerDialogFragment datePickerDialogFragment;
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        datePickerDialogFragment = SimpleDatePickerDialogFragment.getInstance(
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH));
        datePickerDialogFragment.setOnDateSetListener(new SimpleDatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(int year, int monthOfYear) {
                setDateToView(year, monthOfYear+1);
            }
        });
        datePickerDialogFragment.show(getSupportFragmentManager(), "mDateFragment");
    }

    private void updateProgressButton(boolean loadingStatus, String text) {
        if(actionButton != null) {
            isLoading = loadingStatus;
            actionButton.setLoadingStatus(loadingStatus);
            actionButton.setText(text);
        }
    }

    private void deletePayment(final int studentId, final int paymentId) {

        ServiceGenerator.ClaimService productService = ServiceGenerator.createService(ServiceGenerator.ClaimService.class, getApplicationContext());
        Call<Void> call = productService.DeletePendingPayment(studentId, paymentId);

        JWTRefreshService.enqueueSafe(call, this, new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Toast.makeText(getApplicationContext(), R.string.Payment_Cancel_Message, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.i("","");
            }
        });

    }

    private TextWatcher creditCardWatcher = new TextWatcher() {

        private static final int TOTAL_SYMBOLS = 19; // size of pattern 0000-0000-0000-0000
        private static final int TOTAL_DIGITS = 16; // max numbers of digits in pattern: 0000 x 4
        private static final int DIVIDER_MODULO = 5; // means divider position is every 5th symbol beginning with 1
        private static final int DIVIDER_POSITION = DIVIDER_MODULO - 1; // means divider position is every 4th symbol beginning with 0
        private static final char DIVIDER = '-';

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // noop
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // noop
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (!isInputCorrect(s, TOTAL_SYMBOLS, DIVIDER_MODULO, DIVIDER)) {
                s.replace(0, s.length(), buildCorrecntString(getDigitArray(s, TOTAL_DIGITS), DIVIDER_POSITION, DIVIDER));
            }
        }

        private boolean isInputCorrect(Editable s, int totalSymbols, int dividerModulo, char divider) {
            boolean isCorrect = s.length() <= totalSymbols; // check size of entered string
            for (int i = 0; i < s.length(); i++) { // chech that every element is right
                if (i > 0 && (i + 1) % dividerModulo == 0) {
                    isCorrect &= divider == s.charAt(i);
                } else {
                    isCorrect &= Character.isDigit(s.charAt(i));
                }
            }
            return isCorrect;
        }

        private String buildCorrecntString(char[] digits, int dividerPosition, char divider) {
            final StringBuilder formatted = new StringBuilder();

            for (int i = 0; i < digits.length; i++) {
                if (digits[i] != 0) {
                    formatted.append(digits[i]);
                    if ((i > 0) && (i < (digits.length - 1)) && (((i + 1) % dividerPosition) == 0)) {
                        formatted.append(divider);
                    }
                }
            }

            return formatted.toString();
        }

        private char[] getDigitArray(final Editable s, final int size) {
            char[] digits = new char[size];
            int index = 0;
            for (int i = 0; i < s.length() && index < size; i++) {
                char current = s.charAt(i);
                if (Character.isDigit(current)) {
                    digits[index] = current;
                    index++;
                }
            }
            return digits;
        }
    };

    @Override
    public void onBackPressed() {
        if(paymentBody != null) {
            Payment paymentInfo = paymentBody.getPayment();
            deletePayment(paymentInfo.getStudent().getId(), paymentInfo.getId());
        }
        super.onBackPressed();
    }
}

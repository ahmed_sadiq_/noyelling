package au.com.saybravo.noyelling.app.Helpers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ahmed.sadiq on 20/04/2016.
 */
public class ValidationHelper {

    private static boolean IsMatch(String s, String pattern) {
        try {
            Pattern patt = Pattern.compile(pattern);
            Matcher matcher = patt.matcher(s);
            return matcher.matches();
        } catch (RuntimeException e) {
            return false;
        }
    }

    public static boolean IsValidMobileNumber(String s)
    {
        String pattern = "^04(\\d{8}$|\\d{2}\\s\\d{3}\\s\\d{3}$)|^614(\\d{8}$|\\d{2}\\s\\d{3}\\s\\d{3}$)|^\\+614(\\d{8}$|\\d{2}\\s\\d{3}\\s\\d{3}$)";

        return IsMatch(s, pattern);
    }

    public static boolean IsValidEmail(String s)
    {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(s).matches();
    }
}

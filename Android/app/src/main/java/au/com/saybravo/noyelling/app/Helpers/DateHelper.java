package au.com.saybravo.noyelling.app.Helpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateHelper
{
    public static Date GetDate(String dateIn)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        Date date = null;
        try {
            date = sdf.parse(dateIn);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

    public static String FormatDate(Date date)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        return sdf.format(date);
    }

    public static String FormatDateFromString(String dateIn)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Date date = null;
        String dateOut = "-";
        try {
            date = sdf.parse(dateIn);
            Calendar calendar = Calendar.getInstance();

            calendar.setTime(date);
            dateOut = sdf.format(calendar.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateOut;
    }
}

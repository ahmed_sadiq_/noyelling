package au.com.saybravo.noyelling.claim.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


import java.io.Serializable;

import au.com.saybravo.noyelling.student.Domain.Student;

/**
 * Created by ahmed.sadiq on 26/04/16.
 */
public class Claim  {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("student")
    @Expose
    private Student student;
    @SerializedName("instructor_id")
    @Expose
    private Integer instructorId;
    @SerializedName("payment_id")
    @Expose
    private Integer paymentId;
    @SerializedName("commission_rate")
    @Expose
    private Double commissionRate;
    @SerializedName("commission_paid")
    @Expose
    private Double commissionPaid;
    @SerializedName("net_value")
    @Expose
    private Double netValue;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("reconciled_at")
    @Expose
    private Object reconciledAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("package_detail")
    @Expose
    private PackageDetail packageDetail;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The student
     */
    public Student getStudent() {
        return student;
    }

    /**
     *
     * @param student
     * The student
     */
    public void setStudent(Student student) {
        this.student = student;
    }

    /**
     *
     * @return
     * The instructorId
     */
    public Integer getInstructorId() {
        return instructorId;
    }

    /**
     *
     * @param instructorId
     * The instructor_id
     */
    public void setInstructorId(Integer instructorId) {
        this.instructorId = instructorId;
    }

    /**
     *
     * @return
     * The paymentId
     */
    public Integer getPaymentId() {
        return paymentId;
    }

    /**
     *
     * @param paymentId
     * The payment_id
     */
    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    /**
     *
     * @return
     * The commissionRate
     */
    public Double getCommissionRate() {
        return commissionRate;
    }

    /**
     *
     * @param commissionRate
     * The commission_rate
     */
    public void setCommissionRate(Double commissionRate) {
        this.commissionRate = commissionRate;
    }

    /**
     *
     * @return
     * The commissionPaid
     */
    public Double getCommissionPaid() {
        return commissionPaid;
    }

    /**
     *
     * @param commissionPaid
     * The commission_paid
     */
    public void setCommissionPaid(Double commissionPaid) {
        this.commissionPaid = commissionPaid;
    }

    /**
     *
     * @return
     * The netValue
     */
    public Double getNetValue() {
        return netValue;
    }

    /**
     *
     * @param netValue
     * The net_value
     */
    public void setNetValue(Double netValue) {
        this.netValue = netValue;
    }

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The reconciledAt
     */
    public Object getReconciledAt() {
        return reconciledAt;
    }

    /**
     *
     * @param reconciledAt
     * The reconciled_at
     */
    public void setReconciledAt(Object reconciledAt) {
        this.reconciledAt = reconciledAt;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The packageDetail
     */
    public PackageDetail getPackageDetail() {
        return packageDetail;
    }

    /**
     *
     * @param packageDetail
     * The package_detail
     */
    public void setPackageDetail(PackageDetail packageDetail) {
        this.packageDetail = packageDetail;
    }


}
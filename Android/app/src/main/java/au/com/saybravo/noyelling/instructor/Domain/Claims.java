package au.com.saybravo.noyelling.instructor.Domain;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahmed.sadiq on 19/04/2016.
 */
public class Claims {
    @SerializedName("paid")
    @Expose
    private List<ClaimsPaid> paid = new ArrayList<ClaimsPaid>();
    @SerializedName("pending")
    @Expose
    private List<ClaimsPending> pending = new ArrayList<ClaimsPending>();
    @SerializedName("unreconciled")
    @Expose
    private List<ClaimsUnreconciled> unreconciled = new ArrayList<ClaimsUnreconciled>();

    /**
     *
     * @return
     * The paid
     */
    public List<ClaimsPaid> getPaid() {
        return paid;
    }

    /**
     *
     * @param paid
     * The paid
     */
    public void setPaid(List<ClaimsPaid> paid) {
        this.paid = paid;
    }

    /**
     *
     * @return
     * The pending
     */
    public List<ClaimsPending> getPending() {
        return pending;
    }

    /**
     *
     * @param pending
     * The pending
     */
    public void setPending(List<ClaimsPending> pending) {
        this.pending = pending;
    }

    /**
     *
     * @return
     * The unreconciled
     */
    public List<ClaimsUnreconciled> getUnreconciled() {
        return unreconciled;
    }

    /**
     *
     * @param unreconciled
     * The unreconciled
     */
    public void setUnreconciled(List<ClaimsUnreconciled> unreconciled) {
        this.unreconciled = unreconciled;
    }

    public Boolean hasTransactions() {

        return this.getPaid().size() + this.getPending().size() + this.getUnreconciled().size() > 0;
    }
}

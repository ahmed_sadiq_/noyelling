package au.com.saybravo.noyelling.instructor.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ahmed.sadiq on 11/04/16.
 */
public class AvatarData implements Serializable {

    @SerializedName("url")
    @Expose
    public String url;
}

package au.com.saybravo.noyelling.dashboard;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;

import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.app.BaseActivity;
import au.com.saybravo.noyelling.app.NoYellingApplication;
import au.com.saybravo.noyelling.app.PreferencesManager;
import au.com.saybravo.noyelling.app.Services.JWTRefreshService;
import au.com.saybravo.noyelling.app.Services.JWTokenService;
import au.com.saybravo.noyelling.app.Services.ServiceGenerator;
import au.com.saybravo.noyelling.app.Services.ServiceGenerator.InstructorService;
import au.com.saybravo.noyelling.instructor.Domain.Instructor;
import au.com.saybravo.noyelling.instructor.Domain.InstructorAvailable;
import au.com.saybravo.noyelling.instructor.Domain.InstructorAvailableBody;
import au.com.saybravo.noyelling.instructor.Domain.InstructorBody;
import au.com.saybravo.noyelling.instructor.InstructorDetailsActivity;
import au.com.saybravo.noyelling.instructor.TransactionsActivity;
import au.com.saybravo.noyelling.login.LoginActivity;
import au.com.saybravo.noyelling.student.Domain.StudentRealmModel;
import au.com.saybravo.noyelling.student.StudentsActivity;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import jp.wasabeef.glide.transformations.BlurTransformation;
import jp.wasabeef.glide.transformations.CropCircleTransformation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardActivity extends BaseActivity {

    public DashboardActivity() {
        super(R.layout.activity_dashboard);
    }

    private boolean isLoading;
    private ProgressBar progressBar = null;
    RelativeLayout mainLayout = null;
    private SwitchCompat acceptingStudentsSwitch;
    private CompoundButton.OnCheckedChangeListener acceptingStudentsCheckedChangeListener;
    private Instructor instructor;
    private ImageView unreconciledImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        startLoading();

        initButtons();

        loadDashboardData();
    }

    private void initButtons() {

        ImageButton instructorDetailsButton = (ImageButton) findViewById(R.id.InstructorDetailsButton);
        instructorDetailsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startInstructorDetails();
            }
        });

        RelativeLayout studentLayout = (RelativeLayout)findViewById(R.id.StudentsLayout);
        studentLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                gotoStudents();
            }
        });

        RelativeLayout transactionsLayout = (RelativeLayout)findViewById(R.id.TransactionsLayout);
        transactionsLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                gotoTransactions();
            }
        });

        unreconciledImageView = (ImageView)findViewById(R.id.UnreconciledImageView);
        unreconciledImageView.setVisibility(View.INVISIBLE);


        Button logoutButton = (Button) findViewById(R.id.LogoutButton);
        final Activity currentActivity = this;

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            new AlertDialog.Builder(currentActivity, R.style.AlertDialogCustom).setTitle("Logout")
                        .setMessage("Are you sure you want to Logout?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int whichButton) {
                                logout();
                            }
                        }).setNegativeButton("no", null).show();

            }
        });

        //region Accepting Students

        acceptingStudentsSwitch = (SwitchCompat) findViewById(R.id.AcceptingStudentsSwitch);
        acceptingStudentsCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                toggleInstructorAvailable(b);
            }
        };
        acceptingStudentsSwitch.setOnCheckedChangeListener(acceptingStudentsCheckedChangeListener);

        RelativeLayout acceptingNewStudentsLayout = (RelativeLayout)findViewById(R.id.AcceptingNewStudentsLayout);
        acceptingNewStudentsLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {

                if (isLoading)
                {
                    return;
                }

                boolean currentlyAccepting = acceptingStudentsSwitch.isChecked();

                toggleInstructorAvailable(!currentlyAccepting);
                acceptingStudentsSwitch.setChecked(!currentlyAccepting);
            }
        });



        //endregion
    }

    public void loadDashboardData() {

        InstructorService instructorService = ServiceGenerator.createService(InstructorService.class, getApplicationContext());
        Call<InstructorBody> call = instructorService.GetInstructor();

        JWTRefreshService.enqueueSafe(call, this, new Callback<InstructorBody>() {
            @Override
            public void onResponse(Call<InstructorBody> call, Response<InstructorBody> response) {

                try {

                    if (response.isSuccessful()) {
                        InstructorBody result = response.body();

                        instructor = result.getInstructor();
                        SetCurrentInstructor(instructor);

                        bindInstructorToView();

                    } else {
                        logInfo("NOT isSuccessful : " + response.code());

                        ShowGenericError();
                        stopLoading();
                    }

                } catch (Exception e) {
                    ShowGenericError();
                    stopLoading();
                }
            }

            @Override
            public void onFailure(Call<InstructorBody> call, Throwable t) {
                ShowGenericError();
                stopLoading();
            }
        });
    }

    private void startLoading()
    {
        isLoading = true;

        if (progressBar == null)
        {
            progressBar = (ProgressBar) findViewById(R.id.ProgressBar);
        }
        progressBar.setVisibility(View.VISIBLE);

        if (mainLayout == null)
        {
            mainLayout = (RelativeLayout) findViewById(R.id.ImageBackgroundLayout);
        }
        mainLayout.setVisibility(View.INVISIBLE);
    }

    private void stopLoading()
    {
        mainLayout.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.INVISIBLE);

        isLoading = false;
    }

    private void startInstructorDetails()
    {
        startActivity(InstructorDetailsActivity.class);
    }

    private void gotoStudents()
    {
        if (isLoading)
        {
            return;
        }

        if (!IsNetworkAvailable())
        {
            ShowNoConnectionNotification(findViewById(R.id.DashboardView), new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    gotoStudents();
                }
            });

            return;
        }

        startActivity(StudentsActivity.class);
    }

    private void gotoTransactions()
    {
        if (isLoading)
        {
            return;
        }

        if (!IsNetworkAvailable())
        {
            ShowNoConnectionNotification(findViewById(R.id.DashboardView), new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    gotoTransactions();
                }
            });

            return;
        }

        startActivity(TransactionsActivity.class);
    }

    private void toggleInstructorAvailable(final boolean yes)
    {
        if (isLoading)
        {
            return;
        }

        if (!IsNetworkAvailable())
        {
            acceptingStudentsSwitch.setChecked(!yes);

            ShowNoConnectionNotification(findViewById(R.id.DashboardView), new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    toggleInstructorAvailable(yes);
                }
            });

            return;
        }

        InstructorService instructorService = ServiceGenerator.createService(InstructorService.class, getApplicationContext());

        InstructorAvailableBody instructorAvailableBody = new InstructorAvailableBody();
        InstructorAvailable instructorAvailable = new InstructorAvailable();
        instructorAvailable.setAvailable(yes);
        instructor.available = yes;

        instructorAvailableBody.setInstructorAvailable(instructorAvailable);

        Call<Void> call = instructorService.SetAvailability(instructorAvailableBody);

        JWTRefreshService.enqueueSafe(call, this, new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (!response.isSuccessful()) {
                    ShowGenericError();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                ShowGenericError();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        instructor = GetCurrentInstructor();

        ((NoYellingApplication)getApplication()).AbandonedClaimsList.clear();

        // SD - This will fire when the back button is pressed from another view
        if (instructor != null)
        {
            bindInstructorToView();
        }
    }

    private void bindInstructorToView()
    {
        setText(R.id.InstructorFirstName, instructor.fname);
        setText(R.id.InstructorLastName, instructor.lname);

        acceptingStudentsSwitch.setOnCheckedChangeListener(null);
        acceptingStudentsSwitch.setChecked(instructor.available);
        acceptingStudentsSwitch.setOnCheckedChangeListener(acceptingStudentsCheckedChangeListener);

        Glide.with(this)
                .load(instructor.avatar.avatar.url)
                .placeholder(R.drawable.icon_usersettings)
                .bitmapTransform(new CropCircleTransformation(getApplicationContext()))
                .into((ImageView) findViewById(R.id.avatar));

        Glide.with(this)
                .load(instructor.avatar.avatar.url)
                .bitmapTransform(new BlurTransformation(getApplicationContext(),5))
                .into((ImageView) findViewById(R.id.instructorBackground));

        stopLoading();
    }

    private void logout() {

        // clear token
        JWTokenService.setToken("", getApplicationContext());
        PreferencesManager.getInstance(getApplicationContext()).clear();

        SetCurrentInstructor(null);

        clearRecentStudents();

        Intent myIntent = new Intent(DashboardActivity.this, LoginActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);// clear back stack
        startActivity(myIntent);
        finish();

        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);

    }

    private void clearRecentStudents()
    {
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(getBaseContext()).name("recentStudents.realm").build();
        Realm realm = Realm.getInstance(realmConfiguration);

        realm.beginTransaction();
        realm.clear(StudentRealmModel.class);
        realm.commitTransaction();
    }
}
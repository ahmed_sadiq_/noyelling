package au.com.saybravo.noyelling.instructor.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmed.sadiq on 11/04/16.
 */
public class ClaimsStatisticsBody {

    @SerializedName("claims_statistics")
    @Expose
    private ClaimsStatistics claimsStatistics;

    public ClaimsStatistics getClaimsStatistics(){
        return claimsStatistics;
    }

}

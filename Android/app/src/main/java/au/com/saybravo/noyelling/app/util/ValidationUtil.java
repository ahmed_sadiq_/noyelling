package au.com.saybravo.noyelling.app.util;

import android.text.TextUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ahmed.sadiq on 11/28/2016.
 */
public class ValidationUtil {

    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );

    /**
     * <p>
     * Checked is Null or Empty
     * </p>
     *
     * @param s is {@link String}
     * @return The {@link Boolean}
     */
    public static boolean isNullOrEmptyCount(String s) {
        return (isNullOrEmpty(s) || s.equalsIgnoreCase("0"));
    }

    /**
     * <p>
     * Checked is Null or Empty
     * </p>
     *
     * @param s is {@link String}
     * @return The {@link Boolean}
     */
    public static boolean isNullOrEmpty(String s) {
        return (TextUtils.isEmpty(s) || s.equalsIgnoreCase("null"));
    }

    /**
     * method is used for checking valid email id format.
     * @param email
     * @return boolean true for valid false for invalid
     */
    public static boolean isEmailValid(String email) {
        boolean isValid = false;
        CharSequence inputStr = email;
        Matcher matcher = EMAIL_ADDRESS_PATTERN.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }


}

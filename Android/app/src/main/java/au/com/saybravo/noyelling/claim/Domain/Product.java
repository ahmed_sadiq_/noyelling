package au.com.saybravo.noyelling.claim.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
/**
 * Created by ahmed.sadiq on 26/04/16.
 */
public class Product {


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("minutes_in_car")
    @Expose
    private Integer minutesInCar;
    @SerializedName("base_rate")
    @Expose
    private Double baseRate;
    @SerializedName("region")
    @Expose
    private Region region;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The minutesInCar
     */
    public Integer getMinutesInCar() {
        return minutesInCar;
    }

    /**
     *
     * @param minutesInCar
     * The minutes_in_car
     */
    public void setMinutesInCar(Integer minutesInCar) {
        this.minutesInCar = minutesInCar;
    }

    /**
     *
     * @return
     * The baseRate
     */
    public Double getBaseRate() {
        return baseRate;
    }

    /**
     *
     * @param baseRate
     * The base_rate
     */
    public void setBaseRate(Double baseRate) {
        this.baseRate = baseRate;
    }

    /**
     *
     * @return
     * The region
     */
    public Region getRegion() {
        return region;
    }

    /**
     *
     * @param region
     * The region
     */
    public void setRegion(Region region) {
        this.region = region;
    }


}
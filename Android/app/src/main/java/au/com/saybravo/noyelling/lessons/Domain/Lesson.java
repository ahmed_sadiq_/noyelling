package au.com.saybravo.noyelling.lessons.Domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Lesson implements Serializable {

    public Date Date;
    public String Title;
    public BigDecimal Price;
    public double Hours;
    public boolean IsClaimed;
    public Date DateClaimed;

    public Lesson(Date date, String title, BigDecimal price, double hours, boolean isClaimed, Date dateClaimed)
    {
        Date = date;
        Title = title;
        Price = price;
        Hours = hours;
        IsClaimed = isClaimed;
        DateClaimed = dateClaimed;
    }
}

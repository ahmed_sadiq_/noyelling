package au.com.saybravo.noyelling.app;

import android.app.Application;

import com.paypal.merchant.sdk.PayPalHereSDK;

import io.fabric.sdk.android.Fabric;
import net.danlew.android.joda.JodaTimeAndroid;

import java.util.ArrayList;
import java.util.List;

import au.com.saybravo.noyelling.instructor.Domain.Instructor;

/**
 * Created by ahmed.sadiq on 13/04/2016.
 */
public class NoYellingApplication extends Application {

    public static final int NumberOfStudentsPerPage = 50;

    public static final boolean IsLive = true;

    public Instructor CurrentInstructor;

    public static List<Integer> AbandonedClaimsList = new ArrayList<Integer>();

    public static boolean AddedClaim = false;


    @Override
    public void onCreate(){
        super.onCreate();
        Fabric.with(this);
        JodaTimeAndroid.init(this);
       // PayPalHereSDK.init(getApplicationContext(), PayPalHereSDK.Sandbox);
    }

}

package au.com.saybravo.noyelling.app.Services;
import android.content.Context;

import au.com.saybravo.noyelling.app.PreferencesManager;

/**
 * Created by ahmed.sadiq on 8/04/2016.
 */
public class JWTokenService {

    private static String Token;

    public static String getToken(Context context)
    {
        if(Token == null)
        {
            return PreferencesManager.getInstance(context).getTokenValue();
        }
        return Token;
    }

    public static void setToken(String token, Context context) {

        PreferencesManager.getInstance(context).setTokenValue(token);
        Token = token;
    }
}

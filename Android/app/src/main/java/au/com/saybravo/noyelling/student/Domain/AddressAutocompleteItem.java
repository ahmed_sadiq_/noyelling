package au.com.saybravo.noyelling.student.Domain;

/**
 * Created by ahmed.sadiq on 30/05/2016.
 */
public class AddressAutocompleteItem {
    public String PlaceId;
    public String Title;

    public AddressAutocompleteItem(String placeId, String title)
    {
        PlaceId = placeId;
        Title = title;
    }
}

package au.com.saybravo.noyelling.payment;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.paypal.merchant.sdk.AuthenticationListener;
import com.paypal.merchant.sdk.CardReaderConnectionListener;
import com.paypal.merchant.sdk.CardReaderListener;
import com.paypal.merchant.sdk.CardReaderManager;
import com.paypal.merchant.sdk.MerchantManager;
import com.paypal.merchant.sdk.PayPalHereSDK;
import com.paypal.merchant.sdk.TransactionController;
import com.paypal.merchant.sdk.TransactionManager;
import com.paypal.merchant.sdk.domain.ChipAndPinStatusUpdateHandler;
import com.paypal.merchant.sdk.domain.DefaultResponseHandler;
import com.paypal.merchant.sdk.domain.DomainFactory;
import com.paypal.merchant.sdk.domain.Invoice;
import com.paypal.merchant.sdk.domain.InvoiceItem;
import com.paypal.merchant.sdk.domain.Merchant;
import com.paypal.merchant.sdk.domain.PPError;
import com.paypal.merchant.sdk.domain.ReceiptDetails;
import com.paypal.merchant.sdk.domain.SDKReceiptScreenOptions;
import com.paypal.merchant.sdk.domain.SDKSignatureScreenOptions;
import com.paypal.merchant.sdk.domain.credentials.OAuthCredentials;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.app.BaseActivity;
import au.com.saybravo.noyelling.app.Domain.PayPalAccessToken;
import au.com.saybravo.noyelling.app.Helpers.StringHelper;
import au.com.saybravo.noyelling.app.Services.JWTRefreshService;
import au.com.saybravo.noyelling.app.Services.ServiceGenerator;
import au.com.saybravo.noyelling.payment.Domain.Package;
import au.com.saybravo.noyelling.payment.Domain.PackageDetail;
import au.com.saybravo.noyelling.payment.Domain.Payment;
import au.com.saybravo.noyelling.payment.Domain.PaymentBody;
import au.com.saybravo.noyelling.payment.Domain.ReconcileBody;
import au.com.saybravo.noyelling.payment.Domain.Transaction;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ahmed.sadiq on 18/05/16.
 */
public class PaymentActivity extends BaseActivity implements AuthenticationListener, CardReaderConnectionListener, TransactionController, View.OnClickListener {

    public enum ConnectionStatus {
        NoBluetooth,
        BluetoothOff,
        NoPairedDevice,
        MultiplePairedDevices,
        DeviceNotAccessible,
        AccessTokenRequestFailed,
        MerchantAuthenticationFailed,

        Ok
    }

    private BluetoothDevice mDevice = null;
    private boolean cardReaderConnected = false;

    // Connect Failed View
    private RelativeLayout failedConnectRelativeLayout;
    private TextView connectionFailedMessageTextView;

    // Connected View
    private RelativeLayout connectedRelativeLayout;
    private TextView totalValueTextView;

    // Payment Error View
    private RelativeLayout paymentErrorRelativeLayout;

    //Payment Success View
    private RelativeLayout paymentSuccessRelativeLayout;
    private TextView paymentSuccessMessageTextView;

    private Button actionButton;
    private Button cancelButton;

    private Payment paymentInfo;
    private boolean hasLocationPermission = false;
    private boolean donePayment = false;

    private static final String[] LOCATION_PERMS = {
            android.Manifest.permission.ACCESS_COARSE_LOCATION,
            android.Manifest.permission.ACCESS_FINE_LOCATION
    };

    private static final int LOCATION_ACCESS_REQUEST  = 1234;
    private static final int BLUETOOTH_ENABLE_REQUEST = 2345;


    ProgressDialog cardReaderInitDialog;

    public PaymentActivity() {
        super(R.layout.activity_payment);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        bindView();

        failedConnectRelativeLayout.setVisibility(View.VISIBLE);
        connectedRelativeLayout.setVisibility(View.GONE);
        paymentErrorRelativeLayout.setVisibility(View.GONE);
        paymentSuccessRelativeLayout.setVisibility(View.GONE);

        paymentInfo = (Payment) getIntent().getSerializableExtra("PaymentInformation");

        showConnectingDialog();

        initializePaypal();
        setupPermissions();
        authenticateMerchant();

        final TransactionController transactionController = this;

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(paymentInfo != null) {
                    cancelButton.setEnabled(false);
                    deletePayment(paymentInfo.getStudent().getId(), paymentInfo.getId());

                }
            }
        });

        actionButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        String connectToDevice = getString(R.string.Payment_Connect_To_Reader);
        String takePayment = getString(R.string.Payment_Take_Payment);
        String tryAgain = getString(R.string.Payment_Try_Again);
        String done = getString(R.string.Payment_Done);

        String buttonText = actionButton.getText().toString();

        if(buttonText.equalsIgnoreCase(connectToDevice)){
            // Connect to device

            showConnectingDialog();
            ConnectionStatus rtnVal;

            if((rtnVal = selectAndConnectToReader()) != ConnectionStatus.Ok) {
                showNotConnectedView(rtnVal);

            }
        }else if(buttonText.equalsIgnoreCase(takePayment)){

            // Take Payment
            if (hasLocationPermission && mDevice != null) {
                takePayment();
            }

        }else if(buttonText.equalsIgnoreCase(tryAgain)){
            retryPayment();

        }else if(buttonText.equalsIgnoreCase(done)){
            // Done
            onBackPressed();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch(requestCode){

            case LOCATION_ACCESS_REQUEST:
                hasLocationPermission = true;
                break;

        }
    }

    @Override
    public void onBackPressed() {

        if(donePayment == true){
            super.onBackPressed();
        }
        //Do nothing disables back button
    }

    private void bindView(){

        // Failed Connect View
        failedConnectRelativeLayout = (RelativeLayout) findViewById(R.id.FailedConnectView);
        connectionFailedMessageTextView = (TextView) findViewById(R.id.ConnectionFailedMessage);

        // Connected View
        connectedRelativeLayout = (RelativeLayout) findViewById(R.id.ConnectedView);
        totalValueTextView = (TextView) findViewById(R.id.TotalValue);

        // Payment Error View
        paymentErrorRelativeLayout = (RelativeLayout) findViewById(R.id.PaymentErrorView);
        paymentSuccessMessageTextView = (TextView) findViewById(R.id.PaymentSuccessMessage);

        //Payment Success View
        paymentSuccessRelativeLayout = (RelativeLayout) findViewById(R.id.PaymentSuccessView);

        actionButton = (Button) findViewById(R.id.ActionButton);
        cancelButton = (Button) findViewById(R.id.CancelButton);

    }

    private void disconnectCardReader(){
        if(mDevice != null) {
            PayPalHereSDK.getCardReaderManager().disconnectFromDevice(mDevice);
            mDevice = null;
        }
    }

    private void createInvoice(){

        if(paymentInfo != null){

            Package _package = paymentInfo.getPackage();
            List<PackageDetail> details = _package.getPackageDetails();

            Invoice invoice = PayPalHereSDK.getTransactionManager().beginPayment(this);
            invoice.clearAllItems();

            invoice.setCashierId("Instructor "+GetCurrentInstructorId()+" :"+paymentInfo.getId());

            for(PackageDetail detail: details) {

                InvoiceItem invoiceItem = DomainFactory.newInvoiceItem(detail.getProduct().getName(), "id:"+detail.getProduct().getId(), new BigDecimal(detail.getSalePrice()));
                invoice.addItem(invoiceItem, new BigDecimal(detail.getQuantity()));
            }

        }
    }

    private void retryPayment(){
        if(mDevice == null) {
            showConnectingDialog();
            ConnectionStatus rtnVal;

            if ((rtnVal = selectAndConnectToReader()) != ConnectionStatus.Ok) {
                showNotConnectedView(rtnVal);

            }
        }else{
            showConnectedView();
        }

    }

    private ConnectionStatus selectAndConnectToReader(){

        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();

        // no bluetooth adapter found
        if(btAdapter == null){
            Toast.makeText(this, getString(R.string.Payment_Error_No_Bluetooth), Toast.LENGTH_LONG).show();
            return ConnectionStatus.NoBluetooth;
        }

        if(!btAdapter.isEnabled()){

            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, BLUETOOTH_ENABLE_REQUEST);
            Toast.makeText(this, getString(R.string.Payment_Error_Bluetooth_Off), Toast.LENGTH_LONG).show();

            return ConnectionStatus.BluetoothOff;
        }


        Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();
        List<BluetoothDevice> payPalDevices = getPayPalDevices(pairedDevices);

        if(pairedDevices.size() == 0 || payPalDevices.size() == 0){
            Toast.makeText(this, getString(R.string.Payment_Error_No_Paired_Device), Toast.LENGTH_LONG).show();
            final Intent intent = new Intent(Intent.ACTION_MAIN, null);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            ComponentName cn = new ComponentName("com.android.settings",
                    "com.android.settings.bluetooth.BluetoothSettings");
            intent.setComponent(cn);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity( intent);

            return ConnectionStatus.NoPairedDevice;
        }

        if(payPalDevices.size() == 1){

            BluetoothDevice payPalDevice = payPalDevices.get(0);

            connectToEMVReader(payPalDevice);

        }else{

            Toast.makeText(getApplicationContext(), getString(R.string.Payment_Error_Multiple_PayPal_Devices), Toast.LENGTH_LONG).show();
            return ConnectionStatus.MultiplePairedDevices;

        }
        return ConnectionStatus.Ok;
    }

    private List<BluetoothDevice> getPayPalDevices(Set<BluetoothDevice> pairedDevices){
        List<BluetoothDevice> payPalDevices = new ArrayList<>();
        for(BluetoothDevice device : pairedDevices){
            if(device.getName().contains("PayPal")){
                payPalDevices.add(device);
            }
        }

        return payPalDevices;
    }


    private void setupPermissions() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!hasPermission(LOCATION_PERMS[1])) {
                ActivityCompat.requestPermissions(this, LOCATION_PERMS,  LOCATION_ACCESS_REQUEST);
            }else{
                hasLocationPermission = true;
            }
        }else{
            hasLocationPermission = true;
        }

    }



    @TargetApi(Build.VERSION_CODES.CUR_DEVELOPMENT)
    private boolean hasPermission(String perm) {
        return(android.content.pm.PackageManager.PERMISSION_GRANTED==checkSelfPermission(perm));
    }

        // Paypal Here SDK functions

    // TODO might have to do init only once
    private void initializePaypal()
    {
        PayPalHereSDK.init(getApplicationContext(), PayPalHereSDK.Live);
        PayPalHereSDK.setServerName(PayPalHereSDK.Live);
        PayPalHereSDK.registerAuthenticationListener(this);
        PayPalHereSDK.getCardReaderManager().registerCardReaderConnectionListener(this);

    }


    private void authenticateMerchant(){

        ServiceGenerator.LoginService loginService = ServiceGenerator.createService(ServiceGenerator.LoginService.class, getApplicationContext());

        Call<PayPalAccessToken> call = loginService.GetPayPalAccessToken();

        JWTRefreshService.enqueueSafe(call, this, new Callback<PayPalAccessToken>() {
            @Override
            public void onResponse(Call<PayPalAccessToken> call, Response<PayPalAccessToken> response) {
                if(response.isSuccessful()) {
                    PayPalAccessToken token = response.body();
                    setCredentials(token.getAccessToken());
                }
            }

            @Override
            public void onFailure(Call<PayPalAccessToken> call, Throwable t) {
                showNotConnectedView(ConnectionStatus.AccessTokenRequestFailed);

            }
        });

    }


    private void setCredentials(String accessToken){

        PayPalHereSDK.setCredentials(new OAuthCredentials(accessToken),
                new DefaultResponseHandler<Merchant, PPError<MerchantManager.MerchantErrors>>() {
                    @Override
                    public void onSuccess(Merchant merchant) {

                        ConnectionStatus rtnVal;
                        if((rtnVal = selectAndConnectToReader()) != ConnectionStatus.Ok){
                            showNotConnectedView(rtnVal);
                        }

                    }

                    @Override
                    public void onError(PPError<MerchantManager.MerchantErrors> merchantErrorsPPError) {
                        Log.e("set Credentials", "Error"+ merchantErrorsPPError.getDetailedMessage());
                        showNotConnectedView(ConnectionStatus.MerchantAuthenticationFailed);


                    }
                });

    }


    private void reconcilePayment(int studentId, int paymentId, String transactionId){

        ServiceGenerator.ProductService productService = ServiceGenerator.createService(ServiceGenerator.ProductService.class, getApplicationContext());

        ReconcileBody reconcileBody = new ReconcileBody();
        Transaction transaction = new Transaction();

        transaction.setPaymentType("PAYPAL");
        transaction.setTransactionId(transactionId);

        reconcileBody.setTransaction(transaction);

        Call<PaymentBody> call = productService.ReconcilePayment(studentId,paymentId, reconcileBody);

        JWTRefreshService.enqueueSafe(call, this, new Callback<PaymentBody>() {
            @Override
            public void onResponse(Call<PaymentBody> call, Response<PaymentBody> response) {

                showPaymentSuccessView(true);

            }

            @Override
            public void onFailure(Call<PaymentBody> call, Throwable t) {
                // we need to do something here payment already taken but can't update noyeling
            }
        });

    }

    private void deletePayment(final int studentId, final int paymentId){

        ServiceGenerator.ClaimService productService = ServiceGenerator.createService(ServiceGenerator.ClaimService.class, getApplicationContext());
        Call<Void> call = productService.DeletePendingPayment(studentId, paymentId);

        JWTRefreshService.enqueueSafe(call, this, new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                disconnectCardReader();
                Toast.makeText(getApplicationContext(), R.string.Payment_Cancel_Message, Toast.LENGTH_LONG).show();
                donePayment = true;
                onBackPressed();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                disconnectCardReader();
            }
        });

    }

    private void showNotConnectedView(ConnectionStatus connectionStatus){

        failedConnectRelativeLayout.setVisibility(View.VISIBLE);
        connectedRelativeLayout.setVisibility(View.GONE);
        paymentSuccessRelativeLayout.setVisibility(View.GONE);
        paymentErrorRelativeLayout.setVisibility(View.GONE);

        String errorMessage = "";

        switch (connectionStatus){

            case NoBluetooth:
                errorMessage = getString(R.string.Payment_Error_No_Bluetooth);
                break;
            case BluetoothOff:
                errorMessage = getString(R.string.Payment_Error_Bluetooth_Off);
                break;
            case NoPairedDevice:
                errorMessage = getString(R.string.Payment_Error_No_Paired_Device);
                break;
            case DeviceNotAccessible:
                errorMessage = getString(R.string.Payment_Error_CardReaderNotFound);
                break;
            case MultiplePairedDevices:
                errorMessage = getString(R.string.Payment_Error_Multiple_PayPal_Devices);
                break;
            case AccessTokenRequestFailed:
                errorMessage = "Error authentication error";
                break;
            case MerchantAuthenticationFailed:
                errorMessage = "Error Merchant authentication error";
                break;

        }

        connectionFailedMessageTextView.setText(errorMessage);
        actionButton.setText(getString(R.string.Payment_Connect_To_Reader));

        if(cardReaderInitDialog != null) {
            cardReaderInitDialog.dismiss();
        }

    }

    private void showConnectedView(){

        failedConnectRelativeLayout.setVisibility(View.GONE);
        connectedRelativeLayout.setVisibility(View.VISIBLE);
        paymentSuccessRelativeLayout.setVisibility(View.GONE);
        paymentErrorRelativeLayout.setVisibility(View.GONE);

        if(paymentInfo != null){
            totalValueTextView.setText("$" + StringHelper.FormatCurrency(paymentInfo.getPackage().getSalePrice()));
        }

        actionButton.setText(getString(R.string.Payment_Take_Payment));

        if(cardReaderInitDialog != null) {
            cardReaderInitDialog.dismiss();
        }
    }

    private void showPaymentSuccessView(boolean doneUpdateServer){

        failedConnectRelativeLayout.setVisibility(View.GONE);
        connectedRelativeLayout.setVisibility(View.GONE);
        paymentSuccessRelativeLayout.setVisibility(View.VISIBLE);
        paymentErrorRelativeLayout.setVisibility(View.GONE);

        cancelButton.setVisibility(View.INVISIBLE);
        if(doneUpdateServer) {

            donePayment = true;
            paymentSuccessMessageTextView.setText(R.string.Payment_Success_Message);

        }else{
            paymentSuccessMessageTextView.setText(R.string.Payment_Success_Pending_Update_Message);
        }
        actionButton.setText(getString(R.string.Payment_Done));

    }

    private void showPaymentErrorView(){

        failedConnectRelativeLayout.setVisibility(View.GONE);
        connectedRelativeLayout.setVisibility(View.GONE);
        paymentSuccessRelativeLayout.setVisibility(View.GONE);
        paymentErrorRelativeLayout.setVisibility(View.VISIBLE);

        actionButton.setText(getString(R.string.Payment_Try_Again));

    }

    private void makeEMVReaderActive(){
        if(PayPalHereSDK.getCardReaderManager().getActiveReaderType().equals(CardReaderListener.ReaderTypes.ChipAndPinReader)){
            return;
        }

        List<CardReaderManager.CardReader> cardReaders =  PayPalHereSDK.getCardReaderManager().getAvailableReaders();
        for(CardReaderManager.CardReader reader: cardReaders){
            if(reader.getReaderType().equals(CardReaderListener.ReaderTypes.ChipAndPinReader)){
                PayPalHereSDK.getCardReaderManager().setActiveReader(reader.getReaderType());
            }
        }
    }

    private void connectToEMVReader(final BluetoothDevice device){


        PayPalHereSDK.getCardReaderManager().connectToDevice(device, new ChipAndPinStatusUpdateHandler<CardReaderManager.ChipAndPinStatusResponse, PPError<CardReaderManager.ChipAndPinConnectionStatus>>() {
            @Override
            public void onInitiated(CardReaderManager.ChipAndPinStatusResponse chipAndPinStatusResponse, PPError<CardReaderManager.ChipAndPinConnectionStatus> chipAndPinConnectionStatusPPError) {
                if(chipAndPinConnectionStatusPPError != null){
                   // Log.d("Error connecting", chipAndPinConnectionStatusPPError.getDetailedMessage());
                }
            }

            @Override
            public void onStatusUpdated(CardReaderManager.ChipAndPinStatusResponse chipAndPinStatusResponse, PPError<CardReaderManager.ChipAndPinConnectionStatus> chipAndPinConnectionStatusPPError) {
                if(chipAndPinConnectionStatusPPError != null){
                    // Log.d("Error connecting", chipAndPinConnectionStatusPPError.getDetailedMessage());
                }
            }

            @Override
            public void onCompleted(CardReaderManager.ChipAndPinStatusResponse chipAndPinStatusResponse, PPError<CardReaderManager.ChipAndPinConnectionStatus> chipAndPinConnectionStatusPPError) {
                if(chipAndPinConnectionStatusPPError != null && chipAndPinConnectionStatusPPError.getErrorCode() == CardReaderManager.ChipAndPinConnectionStatus.ConnectedAndReady){

                    makeEMVReaderActive();
                    mDevice = device;
                    cardReaderConnected = true;
                    createInvoice();
                    showConnectedView();


                }else if(chipAndPinConnectionStatusPPError != null && chipAndPinConnectionStatusPPError.getErrorCode() == CardReaderManager.ChipAndPinConnectionStatus.ErrorSoftwareUpdateRequired){

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(cardReaderInitDialog != null) {

                                cardReaderInitDialog.setMessage(getString(R.string.Payment_Firmware_Update));
                            }
                        }
                    });


                    PayPalHereSDK.getCardReaderManager().initiateSoftwareUpdate(device, new ChipAndPinStatusUpdateHandler<CardReaderManager.ChipAndPinStatusResponse, PPError<CardReaderManager.ChipAndPinSoftwareUpdateStatus>>() {
                        @Override
                        public void onInitiated(CardReaderManager.ChipAndPinStatusResponse chipAndPinStatusResponse, PPError<CardReaderManager.ChipAndPinSoftwareUpdateStatus> chipAndPinSoftwareUpdateStatusPPError) {

                        }

                        @Override
                        public void onStatusUpdated(CardReaderManager.ChipAndPinStatusResponse chipAndPinStatusResponse, PPError<CardReaderManager.ChipAndPinSoftwareUpdateStatus> chipAndPinSoftwareUpdateStatusPPError) {

                        }

                        @Override
                        public void onCompleted(CardReaderManager.ChipAndPinStatusResponse chipAndPinStatusResponse, PPError<CardReaderManager.ChipAndPinSoftwareUpdateStatus> chipAndPinSoftwareUpdateStatusPPError) {
                            makeEMVReaderActive();
                            mDevice = device;
                            cardReaderConnected = true;
                            createInvoice();
                            showConnectedView();
                        }
                    });
                }
            }
        });

    }

    private void takePayment(){
        /**
         * STEP 2: Step 2 is calling processPayment of PayPalHere SDK's Transaction Manager.
         */

            PayPalHereSDK.getTransactionManager().processPaymentWithSDKUI(TransactionManager.PaymentType.CardReader, new DefaultResponseHandler<TransactionManager.PaymentResponse, PPError<TransactionManager.PaymentErrors>>() {
                    @Override
                    public void onSuccess(TransactionManager.PaymentResponse paymentResponse) {

                        showPaymentSuccessView(false);

                        int studentId = paymentInfo.getStudent().getId();
                        int paymentId = paymentInfo.getId();

                        String transactionId = paymentResponse.getTransactionRecord().getTransactionId();

                        reconcilePayment(studentId, paymentId, transactionId);

                        disconnectCardReader();
                    }

                    @Override
                    public void onError(PPError<TransactionManager.PaymentErrors> paymentErrorsPPError) {
                        Log.e("Error", "payment errored "+ paymentErrorsPPError.getErrorCode());
                        showPaymentErrorView();
                    }
                }
        );
    }

    //AuthenticationListener
    @Override
    public void onInvalidToken() {
        Log.d("Error", "onInvalidToken");

    }



    //CardReaderconnectionListener
    @Override
    public void onPaymentReaderConnected(CardReaderListener.ReaderTypes readerTypes, CardReaderListener.ReaderConnectionTypes readerConnectionTypes) {
        cardReaderConnected = true;

    }

    @Override
    public void onPaymentReaderDisconnected(CardReaderListener.ReaderTypes readerTypes) {
        if(cardReaderConnected == false){
            // this is a situation where the reader was not connected due to it being turned off
            showNotConnectedView(ConnectionStatus.DeviceNotAccessible);

        }
        cardReaderConnected = false;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        if(requestCode == BLUETOOTH_ENABLE_REQUEST){
            if(resultCode == RESULT_OK){
                showConnectingDialog();
                ConnectionStatus rtnVal;
                if((rtnVal = selectAndConnectToReader()) != ConnectionStatus.Ok) {
                    showNotConnectedView(rtnVal);
                }
            }
        }

    }

    @Override
    public void onConnectedReaderNeedsSoftwareUpdate(boolean b) {
        Log.d("debug", "needs update "+b);
    }

    @Override
    public void onConnectedReaderSoftwareUpdateComplete() {

    }

    @Override
    public void onMultipleCardReadersConnected(List<CardReaderListener.ReaderTypes> list) {

    }

    @Override
    public void onActiveReaderChanged(CardReaderListener.ReaderTypes readerTypes) {

    }


    // TransactionController

    @Override
    public TransactionControlAction onPreAuthorize(Invoice invoice, String s) {
        return TransactionControlAction.CONTINUE;
    }

    @Override
    public void onPostAuthorize(boolean b) {

    }

    @Override
    public void onPrintRequested(Activity activity, Invoice invoice) {

    }

    @Override
    public Activity getCurrentActivity() {
        return this;
    }

    @Override
    public SDKSignatureScreenOptions getSignatureScreenOpts() {
        return null;
    }

    @Override
    public SDKReceiptScreenOptions getReceiptScreenOptions() {
        return null;
    }

    @Override
    public void onUserPaymentOptionSelected(PaymentOption paymentOption) {

    }

    @Override
    public void onUserRefundOptionSelected(PaymentOption paymentOption) {

    }

    @Override
    public void onTokenExpired(Activity activity, TokenExpirationHandler tokenExpirationHandler) {

    }

    @Override
    public void onContactlessReaderTimeout(Activity activity, ContactlessReaderTimeoutOptionsHandler contactlessReaderTimeoutOptionsHandler) {
        contactlessReaderTimeoutOptionsHandler.onTimeout(ContactlessReaderTimeoutOptions.RETRY_WITH_CONTACTLESS);

    }

    @Override
    public void onReadyToCancelTransaction(CancelTransactionReason cancelTransactionReason) {

    }

    @Override
    public TipPromptOptions shouldPromptForTips() {
        return null;
    }

    @Override
    public Bitmap provideSignatureBitmap() {
        return null;
    }

    @Override
    public void onReaderDisplayUpdated(PresentedReaderDisplay presentedReaderDisplay) {

    }

    /******** UI Components ************/

    private void showConnectingDialog(){

        if(cardReaderInitDialog == null) {
            cardReaderInitDialog = new ProgressDialog(this);
            cardReaderInitDialog.setCanceledOnTouchOutside(false);
        }
        cardReaderInitDialog.setMessage(getString(R.string.Payment_Initialising_Dialog));
        cardReaderInitDialog.show();
    }

}

package au.com.saybravo.noyelling.student.Domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import au.com.saybravo.noyelling.student.Domain.Student;

public class StudentBody {

    @SerializedName("student")
    @Expose
    public Student student;
}

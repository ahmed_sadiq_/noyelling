package au.com.saybravo.noyelling.lessons.Domain;

import au.com.saybravo.noyelling.app.util.AppConstants;

public class LessonListViewItem {

    public ItemType Type;
    public String MainText;
    public String SubText;
    public Integer PaymentId;
    public Integer PackageDetailId;
    public Integer ClaimId;
    public Integer OriginalPosition;
    public String ClaimType;

    public LessonListViewItem(ItemType type, String mainText, String subText, Integer paymentId, Integer packageDetailId, Integer originalPosition)
    {
        Type = type;
        MainText = mainText;
        SubText = subText;
        PaymentId = paymentId;
        PackageDetailId = packageDetailId;
        OriginalPosition = originalPosition;
    }

    public enum ItemType
    {
        Header,
        Checkbox,
        Claimed
    }
}




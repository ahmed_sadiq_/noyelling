package au.com.saybravo.noyelling.lessonPackage;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import au.com.saybravo.noyelling.R;
import au.com.saybravo.noyelling.app.Helpers.StringHelper;
import au.com.saybravo.noyelling.app.NoYellingApplication;
import au.com.saybravo.noyelling.app.ProgressButton.ProgressButton;
import au.com.saybravo.noyelling.app.ProgressButton.ProgressButtonCallback;
import au.com.saybravo.noyelling.app.Services.JWTRefreshService;
import au.com.saybravo.noyelling.app.Services.ServiceGenerator;
import au.com.saybravo.noyelling.lessonPackage.Domain.PackageDetailsBody;
import au.com.saybravo.noyelling.lessonPackage.Domain.DiscountValueBody;
import au.com.saybravo.noyelling.lessonPackage.Domain.LessonPackageItem;
import au.com.saybravo.noyelling.lessonPackage.Domain.ProductItem;
import au.com.saybravo.noyelling.payment.Domain.PaymentBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ahmed.sadiq on 26/05/16.
 */
public class LessonPackageListviewAdapter extends ArrayAdapter<LessonPackageItem> {

    private int layoutResourceId;
    private int totalHeaderLocation  = -1;
    private ProgressButton submitButton = null;
    private Boolean isLoading = null;
    private boolean isGettingDiscountTotal = false;
    private int studentId;

    private Call<DiscountValueBody> currentCall = null;
    private Activity currentActivity;
    private  PackageDetailsBody packageDetailsBody = null;

    public LessonPackageListviewAdapter(Activity context, int resource, int studentId) {
        super(context, resource);
        currentActivity = context;
        this.studentId = studentId;
        layoutResourceId = resource;

    }

    public void setAvailableLessonPackages(List<LessonPackageItem> packageItems) {

        addAll(packageItems);

        addTotalHeaderSection();
        notifyDataSetChanged();
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        try {

            final LessonPackageItem lessonPackageItem = getItem(position);

            final View itemView;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                itemView = inflater.inflate(layoutResourceId, null);

            } else {
                itemView = convertView;
            }

            RelativeLayout itemHeaderRelativeLayout = (RelativeLayout) itemView.findViewById(R.id.ItemHeader);
            RelativeLayout lessonItemRelativeLayout = (RelativeLayout) itemView.findViewById(R.id.LessonItem);
            RelativeLayout totalHeaderRelativeLayout = (RelativeLayout) itemView.findViewById(R.id.TotalHeader);
            RelativeLayout totalItemRelativeLayout = (RelativeLayout) itemView.findViewById(R.id.TotalItem);
            RelativeLayout totalSummaryRelativeLayout = (RelativeLayout) itemView.findViewById(R.id.TotalSummary);
            RelativeLayout submitRelativeLayout = (RelativeLayout) itemView.findViewById(R.id.SubmitSection);

            RelativeLayout spacerRelativeLayout = (RelativeLayout) itemView.findViewById(R.id.Spacer);


            lessonItemRelativeLayout.setVisibility(View.GONE);
            itemHeaderRelativeLayout.setVisibility(View.GONE);
            totalHeaderRelativeLayout.setVisibility(View.GONE);
            totalItemRelativeLayout.setVisibility(View.GONE);
            totalSummaryRelativeLayout.setVisibility(View.GONE);
            submitRelativeLayout.setVisibility(View.GONE);
            spacerRelativeLayout.setVisibility(View.GONE);


            if(lessonPackageItem.IsSubmitButton){
                submitRelativeLayout.setVisibility(View.VISIBLE);

                submitButton = new ProgressButton(itemView, R.id.CreatePackageButton, R.string.LessonPackage_CreateBtnText, R.string.LessonPackage_CreateBtnUploadingText, isLoading != null && isLoading.booleanValue(), new ProgressButtonCallback() {
                    @Override
                    public void callback() {
                        if(isLoading == null || isLoading == false ) {
                            if(!isGettingDiscountTotal) {
                                isLoading = true;
                                createPackage();
                            }
                        }
                    }
                });


                return itemView;
            }


            if (lessonPackageItem.IsTotal) {

                if(lessonPackageItem.IsHeader && !lessonPackageItem.IsSummary){

                    totalHeaderRelativeLayout.setVisibility(View.VISIBLE);
                    spacerRelativeLayout.setVisibility(View.VISIBLE);


                }else if(lessonPackageItem.IsSummary) {

                    totalSummaryRelativeLayout.setVisibility(View.VISIBLE);
                    ((TextView) totalSummaryRelativeLayout.findViewById(R.id.TotalSum)).setText("$"+StringHelper.FormatCurrencyOptinalTwoDecimal(lessonPackageItem.ItemPrice));

                }else {

                    totalItemRelativeLayout.setVisibility(View.VISIBLE);

                    if(lessonPackageItem.ItemCount > 0) {
                        ((TextView) totalItemRelativeLayout.findViewById(R.id.TotalCount)).setText("" + lessonPackageItem.ItemCount);
                        ((TextView) totalItemRelativeLayout.findViewById(R.id.TotalItemName)).setText(" x " + lessonPackageItem.ItemName);
                    }else{
                        ((TextView) totalItemRelativeLayout.findViewById(R.id.TotalCount)).setText("");
                        ((TextView) totalItemRelativeLayout.findViewById(R.id.TotalItemName)).setText(lessonPackageItem.ItemName);
                    }
                    ((TextView) totalItemRelativeLayout.findViewById(R.id.TotalItemValue)).setText("$" + StringHelper.FormatCurrencyOptinalTwoDecimal(lessonPackageItem.ItemPrice));

                }
                return itemView;

            } else {
                if (lessonPackageItem.IsHeader) {

                    itemHeaderRelativeLayout.setVisibility(View.VISIBLE);

                    ((TextView) itemHeaderRelativeLayout.findViewById(R.id.ItemHeaderText)).setText(lessonPackageItem.HeaderName);

                } else {
                    lessonItemRelativeLayout.setVisibility(View.VISIBLE);

                    ((TextView) lessonItemRelativeLayout.findViewById(R.id.ItemName)).setText(lessonPackageItem.ItemName);
                    ((TextView) lessonItemRelativeLayout.findViewById(R.id.ItemValue)).setText("$"+StringHelper.FormatCurrencyOptinalTwoDecimal(lessonPackageItem.ItemPrice));

                    ((TextView) lessonItemRelativeLayout.findViewById(R.id.ItemTotal)).setText("" + lessonPackageItem.ItemCount);

                    changeImage(lessonPackageItem.ItemCount != 0, itemView);

                    final ImageView add = (ImageView) lessonItemRelativeLayout.findViewById(R.id.AddItem);
                    add.setTag(position);

                    add.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(submitButton != null && submitButton.isLoading || isGettingDiscountTotal){
                                return;
                            }
                            add.startAnimation(AnimationUtils.loadAnimation(getContext(),R.anim.scale_image));
                            Integer pos = (Integer)view.getTag();
                            LessonPackageItem clickedItem = getItem(pos);
                            clickedItem.ItemCount++;
                            if (clickedItem.ItemCount == 1) {
                                changeImage(true, itemView);
                            }
                            updateTotatSection();

                        }
                    });

                    final ImageView remove = (ImageView) lessonItemRelativeLayout.findViewById(R.id.RemoveItem);
                    remove.setTag(position);
                    remove.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(submitButton!= null && submitButton.isLoading || isGettingDiscountTotal ){
                                return;
                            }
                            remove.startAnimation(AnimationUtils.loadAnimation(getContext(),R.anim.scale_image));
                            Integer pos = (Integer)view.getTag();
                            LessonPackageItem clickedItem = getItem(pos);
                            if (clickedItem.ItemCount > 0) {
                                clickedItem.ItemCount--;

                                if (clickedItem.ItemCount == 0) {
                                    changeImage(false, itemView);
                                }
                                updateTotatSection();
                            }
                        }
                    });

                }

                return itemView;
            }

        }catch (Exception ex){
            String vla = ex.getMessage();
            if(!NoYellingApplication.IsLive)
            {
                Log.i("Error", ex.getLocalizedMessage());
            }
            return null;
        }

    }

    private void createPackage(){

        if(packageDetailsBody != null && !isGettingDiscountTotal){

            ServiceGenerator.ProductService productService = ServiceGenerator.createService(ServiceGenerator.ProductService.class, currentActivity.getApplicationContext() );
            Call<PaymentBody>call = productService.PostPayment(studentId, packageDetailsBody);

            JWTRefreshService.enqueueSafe(call, currentActivity, new Callback<PaymentBody>() {
               @Override
               public void onResponse(Call<PaymentBody> call, Response<PaymentBody> response) {
                   if(response.isSuccessful()){
                       PaymentBody paymentBody = response.body();
                       if(paymentBody != null) {
                           ((LessonPackageActivity)currentActivity).processPayment(paymentBody);
                       }
                   }
                   isLoading = false;
               }
               @Override
               public void onFailure(Call<PaymentBody> call, Throwable t) {
                    isLoading = false;
               }
           });

        }else{

            isLoading = false;
        }
    }

    private void changeImage(boolean enable, View itemView){
        RelativeLayout lessonItemRelativeLayout = (RelativeLayout) itemView.findViewById(R.id.LessonItem);

        ImageView remove = (ImageView)lessonItemRelativeLayout.findViewById(R.id.RemoveItem);
        if(enable){

            remove.setImageResource(R.drawable.btn_subtract);
        }else {
            remove.setImageResource(R.drawable.btn_subtract_disabled);
        }
    }

    private void addTotalHeaderSection(){

        LessonPackageItem totalHeader = new LessonPackageItem();
        totalHeader.IsHeader = true;
        totalHeader.IsTotal = true;
        add(totalHeader);

        totalHeaderLocation = getPosition(totalHeader);
        updateTotatSection();

    }

    private void updateTotatSection(){



        int total = getCount();
        int added = total - (totalHeaderLocation+1);

        List<ProductItem> discountItems = new ArrayList<ProductItem>();

        BigDecimal totalSummaryPrice = new BigDecimal(0);

        while(added > 0 ){
            LessonPackageItem item = getItem(totalHeaderLocation+1);
            remove(item);
            added--;
        }

        for( int i =0 ; i < getCount(); i++){
            LessonPackageItem item = getItem(i);
            if(!item.IsTotal && !item.IsHeader && item.ItemCount > 0 ){
                LessonPackageItem totalItem = new LessonPackageItem();
                totalItem.ItemCount = item.ItemCount;
                totalItem.IsTotal = true;
                totalItem.ItemName = item.ItemName;
                totalItem.ItemPrice = item.ItemPrice * item.ItemCount;

                ProductItem productItem = new ProductItem();

                productItem.productId = item.Id;
                productItem.quantity = item.ItemCount;

                discountItems.add(productItem);

                totalSummaryPrice = totalSummaryPrice.add( new BigDecimal(totalItem.ItemPrice).setScale(2, BigDecimal.ROUND_HALF_UP)).setScale(2, BigDecimal.ROUND_HALF_UP);

                add(totalItem);
            }
        }

      if(discountItems.size() > 0 ) {

          LessonPackageItem totalSummaryItem = new LessonPackageItem();
          totalSummaryItem.IsTotal = true;
          totalSummaryItem.ItemPrice = 0;
          totalSummaryItem.IsSummary = true;

          add(totalSummaryItem);


          LessonPackageItem submitButton = new LessonPackageItem();
          submitButton.IsSubmitButton = true;

          add(submitButton);

          getDiscount(discountItems, totalSummaryPrice);
      }else {

          notifyDataSetChanged();
      }
    }


    private void getDiscount(final List<ProductItem> discountItems, final BigDecimal totalSummaryPrice){

        isGettingDiscountTotal = true;
        packageDetailsBody = new PackageDetailsBody();

        if(currentCall != null){

            currentCall.cancel();
            currentCall = null;
        }

        packageDetailsBody.setPackageDetails(discountItems);

        LessonPackageItem discountSection = new LessonPackageItem();

        discountSection.IsTotal = true;
        discountSection.ItemName = "Discount";
        discountSection.ItemPrice = 0;
        insert(discountSection, getCount()-2);

        ServiceGenerator.ProductService productService = ServiceGenerator.createService(ServiceGenerator.ProductService.class, currentActivity.getApplicationContext());
        currentCall = productService.PostDiscont(packageDetailsBody);

        JWTRefreshService.enqueueSafe(currentCall, currentActivity, new Callback<DiscountValueBody>() {
            @Override
            public void onResponse(Call<DiscountValueBody> call, Response<DiscountValueBody> response) {

                if(response.isSuccessful()){

                    DiscountValueBody discountValue = response.body();

                    Double discountedValue =  discountValue.getDicount();

                    /*
                    // Code change : Bravo 13/07/2016
                    // Used to hide and then show the discount if the value was zero which caused a screen flicker.
                    if(totalSummaryPrice.subtract(new BigDecimal(discountedValue).setScale(2, BigDecimal.ROUND_HALF_UP)).compareTo(BigDecimal.ZERO ) == 1) {
                        LessonPackageItem discountSection = new LessonPackageItem();
                        discountSection.IsTotal = true;
                        discountSection.ItemName = "Discount";
                        discountSection.ItemPrice = totalSummaryPrice.subtract(new BigDecimal(discountedValue)).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                        insert(discountSection, getCount()-2);
                    }*/

                    LessonPackageItem discountSectionItem = getItem(getCount()-3);
                    if(totalSummaryPrice.subtract(new BigDecimal(discountedValue).setScale(2, BigDecimal.ROUND_HALF_UP)).compareTo(BigDecimal.ZERO ) == 0) {
                        //remove(discountSectionItem);
                    }else {
                        discountSectionItem.ItemPrice = totalSummaryPrice.subtract(new BigDecimal(discountedValue)).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                    }

                    LessonPackageItem totalSummaryItem = getItem(getCount()-2);
                    totalSummaryItem.ItemPrice = discountValue.getDicount();

                    notifyDataSetChanged();

                }

                isGettingDiscountTotal = false;
            }

            @Override
            public void onFailure(Call<DiscountValueBody> call, Throwable t) {
                isGettingDiscountTotal = false;
            }
        });

    }

    public void updateProgressButton(boolean loadingStatus, String text) {
        if(submitButton != null) {
            submitButton.setLoadingStatus(loadingStatus);
            submitButton.setText(text);
        }
    }

}
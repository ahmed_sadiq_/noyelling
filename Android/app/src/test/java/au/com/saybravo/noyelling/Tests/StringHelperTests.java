package au.com.saybravo.noyelling.Tests;

import org.junit.Ignore;
import org.junit.Test;

import java.math.BigDecimal;

import au.com.saybravo.noyelling.app.Helpers.StringHelper;
import au.com.saybravo.noyelling.app.Helpers.ValidationHelper;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by ahmed.sadiq on 5/05/2016.
 */
public class StringHelperTests {

    //region FormatCurrency for Double

    @Test
    public void FormatCurrency_ShouldFormatDouble1()
    {
        double value = 123.12;
        String expectedResult = "123.12";

        String result = StringHelper.FormatCurrency(value);

        assertThat(result, is(expectedResult));
    }

    @Test
    public void FormatCurrency_ShouldFormatDouble2()
    {
        double value = 123.1;
        String expectedResult = "123.10";

        String result = StringHelper.FormatCurrency(value);

        assertThat(result, is(expectedResult));
    }

    @Test
    public void FormatCurrency_ShouldFormatDouble3()
    {
        double value = 123;
        String expectedResult = "123.00";

        String result = StringHelper.FormatCurrency(value);

        assertThat(result, is(expectedResult));
    }

    //endregion

    //region FormatCurrency for BigDecimal

    @Test
    public void FormatCurrency_ShouldFormatBigDecimal1()
    {
        BigDecimal value = new BigDecimal(123.12);
        String expectedResult = "123.12";

        String result = StringHelper.FormatCurrency(value);

        assertThat(result, is(expectedResult));
    }

    @Test
    public void FormatCurrency_ShouldFormatBigDecimal2()
    {
        BigDecimal value = new BigDecimal(123.1);
        String expectedResult = "123.10";

        String result = StringHelper.FormatCurrency(value);

        assertThat(result, is(expectedResult));
    }

    @Test
    public void FormatCurrency_ShouldFormatBigDecimal3()
    {
        BigDecimal value = new BigDecimal(123);
        String expectedResult = "123.00";

        String result = StringHelper.FormatCurrency(value);

        assertThat(result, is(expectedResult));
    }

    //endregion

    //region FormatCurrency for Double

    @Test
    public void FormatOptionalDecimal_ShouldFormatDouble1()
    {
        double value = 123.12;
        String expectedResult = "123.12";

        String result = StringHelper.FormatOptionalDecimal(value);

        assertThat(result, is(expectedResult));
    }

    // TODO Nish - Fix this?
    @Ignore
    @Test
    public void FormatOptionalDecimal_ShouldFormatDouble2()
    {
        double value = 123.1;
        String expectedResult = "123.10";

        String result = StringHelper.FormatOptionalDecimal(value);

        assertThat(result, is(expectedResult));
    }

    @Test
    public void FormatOptionalDecimal_ShouldFormatDouble3()
    {
        double value = 123;
        String expectedResult = "123";

        String result = StringHelper.FormatOptionalDecimal(value);

        assertThat(result, is(expectedResult));
    }

    //endregion
}

package au.com.saybravo.noyelling.Tests;

import org.junit.Test;

import au.com.saybravo.noyelling.app.Helpers.ValidationHelper;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ValidationHelperTests {
    @Test
    public void IsValidMobileNumber_Valid_ShouldReturnTrue() {

        assertThat(ValidationHelper.IsValidMobileNumber("0432115485"), is(true));
    }
}